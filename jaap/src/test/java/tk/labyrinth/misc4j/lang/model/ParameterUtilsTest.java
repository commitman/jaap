package tk.labyrinth.misc4j.lang.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.langmodel.type.util.DeclaredTypeUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.misc4j.test.model.DiamondGenerics;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.TypeMirror;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@ExtendWithJaap
class ParameterUtilsTest {

	@Test
	void testGetChain(ProcessingEnvironment environment) {
		{
//				Assertions.assertEquals(Collections.emptyList(),
//						ParameterUtils.getActualParameters(environment,
//								DeclaredTypeUtils.from(environment, Integer.class),
//								TypeElementUtils.from(environment, Number.class)
//						).map(TypeMirror::toString).collect(Collectors.toList()));
		}
		{
			Assertions.assertEquals(Collections.singletonList("java.lang.Integer"),
					ParameterUtils.getActualParameters(environment,
							DeclaredTypeUtils.from(environment, ArrayList.class, Integer.class),
							TypeElementUtils.get(environment, ArrayList.class)
					).map(TypeMirror::toString).collect(Collectors.toList()));
			// FIXME: This test must yield empty list, need to fix it later.
			// FIXME: We need case for default generic ArrayList to extend this one.
			Assertions.assertEquals(Collections.singletonList("E"),
					ParameterUtils.getActualParameters(environment,
							DeclaredTypeUtils.from(environment, ArrayList.class),
							TypeElementUtils.get(environment, ArrayList.class)
					).map(TypeMirror::toString).collect(Collectors.toList()));
		}
		{
			Assertions.assertEquals(Arrays.asList("java.lang.String", "java.lang.Long"),
					ParameterUtils.getActualParameters(environment,
							DeclaredTypeUtils.from(environment, DiamondGenerics.StringLongMap.class),
							TypeElementUtils.get(environment, Map.class)
					).map(TypeMirror::toString).collect(Collectors.toList()));
			Assertions.assertEquals(Arrays.asList("java.lang.String", "java.lang.Long"),
					ParameterUtils.getActualParameters(environment,
							DeclaredTypeUtils.from(environment, DiamondGenerics.StringLongMap.class),
							TypeElementUtils.get(environment, DiamondGenerics.ABMap.class)
					).map(TypeMirror::toString).collect(Collectors.toList()));
			Assertions.assertEquals(Collections.singletonList("java.lang.String"),
					ParameterUtils.getActualParameters(environment,
							DeclaredTypeUtils.from(environment, DiamondGenerics.StringLongMap.class),
							TypeElementUtils.get(environment, DiamondGenerics.KLongMap.class)
					).map(TypeMirror::toString).collect(Collectors.toList()));
			Assertions.assertEquals(Collections.singletonList("java.lang.Long"),
					ParameterUtils.getActualParameters(environment,
							DeclaredTypeUtils.from(environment, DiamondGenerics.StringLongMap.class),
							TypeElementUtils.get(environment, DiamondGenerics.StringVMap.class)
					).map(TypeMirror::toString).collect(Collectors.toList()));
		}
		{
			Assertions.assertEquals(Arrays.asList("T", "java.lang.Long"),
					ParameterUtils.getActualParameters(environment,
							DeclaredTypeUtils.from(environment, DiamondGenerics.KLongMap.class),
							TypeElementUtils.get(environment, Map.class)
					).map(TypeMirror::toString).collect(Collectors.toList()));
			Assertions.assertEquals(Arrays.asList("java.lang.String", "T"),
					ParameterUtils.getActualParameters(environment,
							DeclaredTypeUtils.from(environment, DiamondGenerics.StringVMap.class),
							TypeElementUtils.get(environment, Map.class)
					).map(TypeMirror::toString).collect(Collectors.toList()));
			Assertions.assertEquals(Arrays.asList("A", "B"),
					ParameterUtils.getActualParameters(environment,
							DeclaredTypeUtils.from(environment, DiamondGenerics.ABMap.class),
							TypeElementUtils.get(environment, Map.class)
					).map(TypeMirror::toString).collect(Collectors.toList()));
		}
	}
}
