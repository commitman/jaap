package tk.labyrinth.misc4j.lang.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

@ExtendWithJaap
class HierarchyUtilsTest {

	@Test
	void testGetChain(ProcessingContext context) {
		ProcessingEnvironment environment = context.getProcessingEnvironment();
		{
			Assertions.assertEquals(Collections.singletonList("java.lang.Integer"),
					HierarchyUtils.getChain(environment,
							TypeElementUtils.get(environment, Integer.class),
							TypeElementUtils.get(environment, Integer.class)
					).map(TypeElement::toString).collect(Collectors.toList()));
			Assertions.assertEquals(Arrays.asList("java.lang.Integer", "java.lang.Number"),
					HierarchyUtils.getChain(environment,
							TypeElementUtils.get(environment, Integer.class),
							TypeElementUtils.get(environment, Number.class)
					).map(TypeElement::toString).collect(Collectors.toList()));
		}
		{
			Assertions.assertEquals(Collections.singletonList("java.util.ArrayList"),
					HierarchyUtils.getChain(environment,
							TypeElementUtils.get(environment, ArrayList.class),
							TypeElementUtils.get(environment, ArrayList.class)
					).map(TypeElement::toString).collect(Collectors.toList()));
			Assertions.assertEquals(Arrays.asList("java.util.ArrayList", "java.util.AbstractList", "java.util.AbstractCollection"),
					HierarchyUtils.getChain(environment,
							TypeElementUtils.get(environment, ArrayList.class),
							TypeElementUtils.get(environment, AbstractCollection.class)
					).map(TypeElement::toString).collect(Collectors.toList()));
			Assertions.assertEquals(Arrays.asList("java.util.ArrayList", "java.util.List", "java.util.Collection", "java.lang.Iterable"),
					HierarchyUtils.getChain(environment,
							TypeElementUtils.get(environment, ArrayList.class),
							TypeElementUtils.get(environment, Iterable.class)
					).map(TypeElement::toString).collect(Collectors.toList()));
		}
//		{
//			// KLongMap and StringVMap are at the same distance.
//			// TODO: Find out whether the order is determined. It may be the order of interface enumeration.
//			Assertions.assertEquals(Arrays.asList(DiamondGenerics.StringLongMap.class, DiamondGenerics.KLongMap.class, DiamondGenerics.RootMap.class),
//					convert(HierarchyUtils.getChain(DiamondGenerics.StringLongMap.class, DiamondGenerics.RootMap.class)));
//			// Short way is expected.
//			Assertions.assertEquals(Arrays.asList(DiamondGenerics.DeepStringLongMap.class, DiamondGenerics.RootMap.class),
//					convert(HierarchyUtils.getChain(DiamondGenerics.DeepStringLongMap.class, DiamondGenerics.RootMap.class)));
//		}
	}
}
