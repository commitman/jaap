package tk.labyrinth.jaap.annotation.merged;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.testing.junit5.JaapExtension;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;

@ExtendWithJaap
class DefaultMergedAnnotationTest {

	@Test
	void testGetAttribute(ProcessingContext processingContext) {
		ElementTemplate mergedAnnotationTestHandle = processingContext.getElementTemplate(
				DefaultMergedAnnotationTest.class);
		{
			Assertions.assertEquals(
					List.of(
							processingContext.getTypeHandle(JaapExtension.class)),
					mergedAnnotationTestHandle
							.getMergedAnnotation(
									ExtendWith.class,
									MergedAnnotationSpecification.metaAnnotation())
							.getAttributeValueAsClassList("value"));
		}
	}

	@Test
	void testGetSignatureString(ProcessingContext processingContext) {
		Assertions.assertEquals(
				"tk.labyrinth.jaap.annotation.merged:DefaultMergedAnnotationTest" +
						"@tk.labyrinth.jaap.testing.junit5.annotation:ExtendWithJaap",
				processingContext
						.getElementTemplate(DefaultMergedAnnotationTest.class)
						.getMergedAnnotation(
								ExtendWithJaap.class,
								MergedAnnotationSpecification.javaCore())
						.getSignatureString());
		Assertions.assertEquals(
				"tk.labyrinth.jaap.annotation.merged:DefaultMergedAnnotationTest" +
						"@java.lang:Override",
				processingContext
						.getElementTemplate(DefaultMergedAnnotationTest.class)
						.getMergedAnnotation(
								Override.class,
								MergedAnnotationSpecification.javaCore())
						.getSignatureString());
		//
		Assertions.assertEquals(
				"tk.labyrinth.jaap.annotation.merged:DefaultMergedAnnotationTest" +
						"#testGetSignatureString(tk.labyrinth.jaap.context:ProcessingContext)" +
						"@org.junit.jupiter.api:Test",
				processingContext
						.getMethodElementTemplateByName(
								DefaultMergedAnnotationTest.class,
								"testGetSignatureString")
						.getMergedAnnotation(
								Test.class,
								MergedAnnotationSpecification.javaCore())
						.getSignatureString());
	}

	@Test
	void testIsPresent(ProcessingContext processingContext) {
		ElementTemplate mergedAnnotationTestHandle = processingContext.getElementTemplate(
				DefaultMergedAnnotationTest.class);
		ElementTemplate extendWithJaapHandle = processingContext.getElementTemplate(ExtendWithJaap.class);
		{
			MergedAnnotationSpecification javaCoreSpecification = MergedAnnotationSpecification.javaCore();
			{
				Assertions.assertTrue(
						mergedAnnotationTestHandle.getMergedAnnotation(
								ExtendWithJaap.class,
								javaCoreSpecification).isPresent());
				Assertions.assertFalse(
						mergedAnnotationTestHandle.getMergedAnnotation(
								ExtendWith.class,
								javaCoreSpecification).isPresent());
			}
			{
				Assertions.assertFalse(
						extendWithJaapHandle.getMergedAnnotation(
								ExtendWithJaap.class,
								javaCoreSpecification).isPresent());
				Assertions.assertTrue(
						extendWithJaapHandle.getMergedAnnotation(
								ExtendWith.class,
								javaCoreSpecification).isPresent());
			}
		}
		{
			MergedAnnotationSpecification metaAnnotationSpecification = MergedAnnotationSpecification.metaAnnotation();
			//
			Assertions.assertTrue(
					mergedAnnotationTestHandle.getMergedAnnotation(
							ExtendWithJaap.class,
							metaAnnotationSpecification).isPresent());
			Assertions.assertTrue(
					mergedAnnotationTestHandle.getMergedAnnotation(
							ExtendWith.class,
							metaAnnotationSpecification).isPresent());
		}
	}
}