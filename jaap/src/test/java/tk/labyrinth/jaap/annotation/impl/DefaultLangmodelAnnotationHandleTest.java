package tk.labyrinth.jaap.annotation.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotation;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.template.element.FieldElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.test.Dummy;
import tk.labyrinth.jaap.test.TestAnnotation;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.lang.annotation.Documented;

@ExtendWithJaap
class DefaultLangmodelAnnotationHandleTest {

	@Test
	void testEquals(ProcessingContext processingContext) {
		TypeElementTemplate dummyHandle = processingContext.getTypeElementTemplate(Dummy.class);
		AnnotationTypeHandle testAnnotationHandle = processingContext.getAnnotationTypeHandle(TestAnnotation.class);
		//
		AnnotationHandle handle0 = dummyHandle.getDirectAnnotation(testAnnotationHandle);
		AnnotationHandle handle1 = dummyHandle.getDirectAnnotation(testAnnotationHandle);
		//
		Assertions.assertNotNull(handle0);
		Assertions.assertNotNull(handle1);
		Assertions.assertNotSame(handle0, handle1);
		Assertions.assertEquals(handle0, handle1);
	}

	@Test
	void testGetMergedAnnotation(ProcessingContext processingContext) {
		TypeElementTemplate dummyHandle = processingContext.getTypeElementTemplate(Dummy.class);
		FieldElementTemplate customHandle = dummyHandle.getField("custom");
		FieldElementTemplate metaHandle = dummyHandle.getField("meta");
		AnnotationTypeHandle testAnnotationHandle = processingContext.getAnnotationTypeHandle(TestAnnotation.class);
		//
		MergedAnnotation classData = dummyHandle.getMergedAnnotation(
				testAnnotationHandle,
				MergedAnnotationSpecification.javaCore());
		MergedAnnotation customData = customHandle.getMergedAnnotation(
				testAnnotationHandle,
				MergedAnnotationSpecification.javaCore());
		MergedAnnotation metaData = metaHandle.getMergedAnnotation(
				testAnnotationHandle,
				MergedAnnotationSpecification.metaAnnotation());
		//
		Assertions.assertEquals("default", classData.getAttribute("string").getValueAsString());
		Assertions.assertEquals("custom", customData.getAttribute("string").getValueAsString());
		Assertions.assertEquals("meta", metaData.getAttribute("string").getValueAsString());
	}

	@Test
	void testGetMergedAnnotationWithRecursion(ProcessingContext processingContext) {
		{
			// Recursion cases:
			// 1. Shallow: @Documented is annotated with @Documented;
			// 2. Deep: @Documented is annotated with @Retention and @Target which are annotated with @Documented.
			// Asserting that this invocation is executed faultless.
			Assertions.assertFalse(
					processingContext.getTypeElementTemplate(Documented.class)
							.getMergedAnnotation(
									Override.class,
									MergedAnnotationSpecification.metaAnnotation())
							.isPresent());
		}
	}

	@Disabled("TODO")
	@Test
	void testGetSignature(ProcessingContext processingContext) {
		// TODO: It was in old ann model.
//		Assertions.assertEquals("tk.labyrinth.jaap.handle.impl.AnnotationHandleImplTest@tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap",
//				processingContext.getTypeElementTemplate(AnnotationHandleImplTest.class).getDirectAnnotation(ExtendWithJaap.class).getSignature());
	}
}