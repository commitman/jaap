package tk.labyrinth.jaap.annotation.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.test.RepeatableAnnotation;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.Nonnull;

@ExtendWithJaap
class DefaultAnnotationTypeHandleTest {

	@Test
	void testEquals(ProcessingContext processingContext) {
		AnnotationTypeHandle annotationTypeHandle0 = processingContext.getAnnotationTypeHandle(Nonnull.class);
		AnnotationTypeHandle annotationTypeHandle1 = processingContext.getAnnotationTypeHandle(Nonnull.class);
		//
		Assertions.assertNotSame(annotationTypeHandle0, annotationTypeHandle1);
		Assertions.assertEquals(annotationTypeHandle0, annotationTypeHandle1);
	}

	@Test
	void testFindRepeatable(ProcessingContext processingContext) {
		AnnotationTypeHandle repeatableHandle = processingContext.getAnnotationTypeHandle(RepeatableAnnotation.class);
		AnnotationTypeHandle repeaterHandle = processingContext.getAnnotationTypeHandle(RepeatableAnnotation.Repeater.class);
		//
		Assertions.assertNull(repeatableHandle.findRepeatable());
		Assertions.assertEquals(repeatableHandle, repeaterHandle.findRepeatable());
	}

	@Test
	void testFindRepeater(ProcessingContext processingContext) {
		AnnotationTypeHandle repeatableHandle = processingContext.getAnnotationTypeHandle(RepeatableAnnotation.class);
		AnnotationTypeHandle repeaterHandle = processingContext.getAnnotationTypeHandle(RepeatableAnnotation.Repeater.class);
		//
		Assertions.assertEquals(repeaterHandle, repeatableHandle.findRepeater());
		Assertions.assertNull(repeaterHandle.findRepeater());
	}

	@Test
	void testIsRepeatable(ProcessingContext processingContext) {
		AnnotationTypeHandle repeatableHandle = processingContext.getAnnotationTypeHandle(RepeatableAnnotation.class);
		AnnotationTypeHandle repeaterHandle = processingContext.getAnnotationTypeHandle(RepeatableAnnotation.Repeater.class);
		//
		Assertions.assertTrue(repeatableHandle.isRepeatable());
		Assertions.assertFalse(repeaterHandle.isRepeatable());
	}

	@Test
	void testIsRepeater(ProcessingContext processingContext) {
		AnnotationTypeHandle repeatableHandle = processingContext.getAnnotationTypeHandle(RepeatableAnnotation.class);
		AnnotationTypeHandle repeaterHandle = processingContext.getAnnotationTypeHandle(RepeatableAnnotation.Repeater.class);
		//
		Assertions.assertFalse(repeatableHandle.isRepeater());
		Assertions.assertTrue(repeaterHandle.isRepeater());
	}
}