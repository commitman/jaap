package tk.labyrinth.jaap.template.element.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;
import tk.labyrinth.jaap.test.RepeatableAnnotation;
import tk.labyrinth.jaap.test.model.ComplexParameters;
import tk.labyrinth.jaap.test.model.MethodParameters;
import tk.labyrinth.jaap.test.model.VarargMethodsAndInheritance;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.List;

@ExtendWithJaap
class MethodElementTemplateImplTest {

	@Test
	void testGetEffectiveSignature(ProcessingContext context) {
		// FIXME: create real test or remove
		MethodElementTemplate arrayMethodElementTemplate = context.getMethodElementTemplateByName(VarargMethodsAndInheritance.Parent.class, "array");
		MethodElementTemplate varargMethodElementTemplate = context.getMethodElementTemplateByName(VarargMethodsAndInheritance.Parent.class, "vararg");
	}

	@Test
	void testGetFormalParameter(ProcessingContext context) {
		{
			MethodElementTemplate template = context.getMethodElementTemplate("java.lang:Object#wait(long,int)");
			//
			Assertions.assertEquals("long timeoutMillis", template.getFormalParameter(0).toString());
			Assertions.assertEquals("int nanos", template.getFormalParameter(1).toString());
		}
	}

	@Test
	void testGetFormalParameters(ProcessingContext context) {
		ContribAssertions.assertEquals(List.of(
				"long timeoutMillis",
				"int nanos"
		), context.getMethodElementTemplate("java.lang:Object#wait(long,int)").getFormalParameters()
				.map(FormalParameterElementTemplate::toString));
	}

	@Test
	void testGetFullSignature(ProcessingContext context) {
		TypeElementTemplate template = context.getTypeElementTemplate(Object.class);
		//
		Assertions.assertEquals(
				"java.lang:Object#getClass()",
				template.getDeclaredMethod("getClass").getFullSignature().toString());
		Assertions.assertEquals(
				"java.lang:Object#hashCode()",
				template.getDeclaredMethod("hashCode").getFullSignature().toString());
		//
		ContribAssertions.assertEquals(
				List.of(
						"java.lang:Object#wait()",
						"java.lang:Object#wait(long)",
						"java.lang:Object#wait(long,int)"),
				template.getDeclaredMethods("wait")
						.map(MethodElementTemplate::getFullSignature)
						.map(MethodFullSignature::toString)
						.sorted());
	}

	@Test
	void testGetMergedAnnotationGetRelevantAnnotations(ProcessingContext processingContext) {
		TypeElementTemplate myInterfaceHandle = processingContext.getTypeElementTemplate(
				RepeatableAnnotation.MyInterface.class);
		AnnotationTypeHandle repeatableAnnotationHandle = processingContext.getAnnotationTypeHandle(
				RepeatableAnnotation.class);
		{
			ContribAssertions.assertEquals(
					List.of(
							"@tk.labyrinth.jaap.test.RepeatableAnnotation(value=0)",
							"@tk.labyrinth.jaap.test.RepeatableAnnotation(value=1)"),
					myInterfaceHandle.getDeclaredMethod("multiple")
							.getMergedAnnotation(
									repeatableAnnotationHandle,
									MergedAnnotationSpecification.javaCore())
							.getRelevantAnnotations().stream()
							.map(AnnotationHandle::toString));
			ContribAssertions.assertEquals(
					List.of(
							"@tk.labyrinth.jaap.test.RepeatableAnnotation(value=2)"),
					myInterfaceHandle.getDeclaredMethod("repeated")
							.getMergedAnnotation(
									repeatableAnnotationHandle,
									MergedAnnotationSpecification.javaCore())
							.getRelevantAnnotations().stream()
							.map(AnnotationHandle::toString));
			ContribAssertions.assertEquals(
					List.of(
							"@tk.labyrinth.jaap.test.RepeatableAnnotation(value=3)"),
					myInterfaceHandle.getDeclaredMethod("single")
							.getMergedAnnotation(
									repeatableAnnotationHandle,
									MergedAnnotationSpecification.javaCore())
							.getRelevantAnnotations().stream()
							.map(AnnotationHandle::toString));
		}
		{
			ContribAssertions.assertEquals(
					List.of(),
					myInterfaceHandle.getDeclaredMethod("nothing")
							.getMergedAnnotation(
									repeatableAnnotationHandle,
									MergedAnnotationSpecification.javaCore())
							.getRelevantAnnotations().stream());
		}
	}

	@Test
	void testGetReturnType(ProcessingContext context) {
		Assertions.assertEquals("java.lang.Class<?>", context.getMethodElementTemplate(
				"java.lang:Object#getClass()").getReturnType().toString());
		Assertions.assertEquals("int", context.getMethodElementTemplate(
				"java.lang:Object#hashCode()").getReturnType().toString());
		Assertions.assertEquals("void", context.getMethodElementTemplate(
				"java.lang:Object#notify()").getReturnType().toString());
	}

	@Test
	void testGetSignature(ProcessingContext context) {
		Assertions.assertEquals(
				"java.lang:Object#wait()",
				context.getMethodElementTemplate("java.lang:Object#wait()").getSignature());
		Assertions.assertEquals(
				"java.lang:Object#wait(long)",
				context.getMethodElementTemplate("java.lang:Object#wait(long)").getSignature());
		Assertions.assertEquals(
				"java.lang:Object#wait(long,int)",
				context.getMethodElementTemplate("java.lang:Object#wait(long,int)").getSignature());
		Assertions.assertEquals(
				"java.util:Optional#of(java.lang:Object)",
				context.getMethodElementTemplateByName("java.util:Optional#of").getSignature());
	}

	@Test
	void testGetSimpleSignature(ProcessingContext context) {
		{
			TypeElementTemplate typeTemplate = context.getTypeElementTemplate(Object.class);
			//
			Assertions.assertEquals(
					"getClass()",
					typeTemplate.getDeclaredMethod("getClass").getSimpleSignature().toString());
			Assertions.assertEquals(
					"hashCode()",
					typeTemplate.getDeclaredMethod("hashCode").getSimpleSignature().toString());
			//
			ContribAssertions.assertEquals(
					List.of(
							"wait()",
							"wait(long)",
							"wait(long,int)"),
					typeTemplate.getDeclaredMethods("wait")
							.map(MethodElementTemplate::getSimpleSignature)
							.map(MethodSimpleSignature::toString)
							.sorted());
		}
		{
			TypeElementTemplate typeTemplate = context.getTypeElementTemplate(MethodParameters.class);
			//
			Assertions.assertEquals(
					"primitive(int)",
					typeTemplate.getDeclaredMethod("primitive").getSimpleSignature().toString());
			Assertions.assertEquals(
					"object(java.lang:Object)",
					typeTemplate.getDeclaredMethod("object").getSimpleSignature().toString());
			Assertions.assertEquals(
					"integerList(java.util:List)",
					typeTemplate.getDeclaredMethod("integerList").getSimpleSignature().toString());
			Assertions.assertEquals(
					"rawtypeList(java.util:List)",
					typeTemplate.getDeclaredMethod("rawtypeList").getSimpleSignature().toString());
			Assertions.assertEquals(
					"simpleVariableType(java.lang:Object)",
					typeTemplate.getDeclaredMethod("simpleVariableType").getSimpleSignature().toString());
			Assertions.assertEquals(
					"string(java.lang:String)",
					typeTemplate.getDeclaredMethod("string").getSimpleSignature().toString());
			Assertions.assertEquals(
					"unboundedWildcardList(java.util:List)",
					typeTemplate.getDeclaredMethod("unboundedWildcardList").getSimpleSignature().toString());
			Assertions.assertEquals(
					"extendsVariableType(java.lang:Number)",
					typeTemplate.getDeclaredMethod("extendsVariableType").getSimpleSignature().toString());
		}
		{
			TypeElementTemplate typeTemplate = context.getTypeElementTemplate(ComplexParameters.class);
			//
			Assertions.assertEquals(
					"comparable(java.lang:Comparable)",
					typeTemplate.getDeclaredMethod("comparable").getSimpleSignature().toString());
			Assertions.assertEquals(
					"map(java.util:Map)",
					typeTemplate.getDeclaredMethod("map").getSimpleSignature().toString());
			Assertions.assertEquals(
					"put(tk.labyrinth.jaap.test.model:ComplexParameters,java.util:List)",
					typeTemplate.getDeclaredMethod("put").getSimpleSignature().toString());
		}
	}

	@Test
	void testGetSimpleSignatureWithArrayAndVarargParameters(ProcessingContext context) {
		MethodElementTemplate arrayMethodElementTemplate = context.getMethodElementTemplateByName(VarargMethodsAndInheritance.Parent.class, "array");
		MethodElementTemplate varargMethodElementTemplate = context.getMethodElementTemplateByName(VarargMethodsAndInheritance.Parent.class, "vararg");
		//
		{
			Assertions.assertEquals(
					"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Parent#array(int[])",
					arrayMethodElementTemplate.toString());
			Assertions.assertEquals(
					"array(int[])",
					arrayMethodElementTemplate.getSimpleSignature().toString());
		}
		{
			Assertions.assertEquals(
					"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Parent#vararg(int[])",
					varargMethodElementTemplate.toString());
			Assertions.assertEquals(
					"vararg(int[])",
					varargMethodElementTemplate.getSimpleSignature().toString());
		}
	}

	@Test
	void testHasMergedAnnotation(ProcessingContext processingContext) {
		TypeElementTemplate myInterfaceHandle = processingContext.getTypeElementTemplate(
				RepeatableAnnotation.MyInterface.class);
		AnnotationTypeHandle repeatableAnnotationHandle = processingContext.getAnnotationTypeHandle(
				RepeatableAnnotation.class);
		{
			Assertions.assertTrue(
					myInterfaceHandle.getDeclaredMethod("multiple").hasMergedAnnotation(
							repeatableAnnotationHandle,
							MergedAnnotationSpecification.javaCore()));
			Assertions.assertTrue(
					myInterfaceHandle.getDeclaredMethod("repeated").hasMergedAnnotation(
							repeatableAnnotationHandle,
							MergedAnnotationSpecification.javaCore()));
			Assertions.assertTrue(
					myInterfaceHandle.getDeclaredMethod("single").hasMergedAnnotation(
							repeatableAnnotationHandle,
							MergedAnnotationSpecification.javaCore()));
		}
		{
			Assertions.assertFalse(
					myInterfaceHandle.getDeclaredMethod("nothing").hasMergedAnnotation(
							repeatableAnnotationHandle,
							MergedAnnotationSpecification.javaCore()));
		}
	}

	@Test
	void testsOfHasTypeParameters(ProcessingContext context) {
		{
			// No TypeParameters
			//
			MethodElementTemplate compareMethod = context.getMethodElementTemplateByName(
					"java.util:Comparator#compare");
			//
			ContribAssertions.assertEquals(
					List.of(),
					compareMethod.getTypeParameters().map(TypeParameterElementTemplate::getSignature));
			Assertions.assertNull(compareMethod.findTypeParameter("T"));
			Assertions.assertNull(compareMethod.findTypeParameter(1));
		}
		{
			// Two TypeParameters
			//
			MethodElementTemplate comparingOneMethod = context.getMethodElementTemplate(
					"java.util:Comparator#comparing(java.util:function.Function)");
			//
			ContribAssertions.assertEquals(
					List.of(
							"java.util:Comparator#comparing(java.util.function:Function)%T",
							"java.util:Comparator#comparing(java.util.function:Function)%U"),
					comparingOneMethod.getTypeParameters().map(TypeParameterElementTemplate::getSignature));
			Assertions.assertEquals("java.util:Comparator#comparing(java.util.function:Function)%T",
					comparingOneMethod.getTypeParameter("T").getSignature());
			Assertions.assertEquals("java.util:Comparator#comparing(java.util.function:Function)%U",
					comparingOneMethod.getTypeParameter(1).getSignature());
		}
	}
}
