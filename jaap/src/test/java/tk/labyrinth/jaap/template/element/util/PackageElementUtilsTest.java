package tk.labyrinth.jaap.template.element.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.ProcessingEnvironment;
import java.util.stream.Stream;

@ExtendWithJaap
@SuppressWarnings("ConstantConditions")
class PackageElementUtilsTest {

	@Test
	void testFindMember(ProcessingEnvironment processingEnvironment) {
		EntitySelectionContext packageSelectionContext = EntitySelectionContext.forPackage();
		EntitySelectionContext typeSelectionContext = EntitySelectionContext.forType();
		{
			Assertions.assertEquals("tk.labyrinth.jaap.template.element.util.PackageElementUtilsTest", PackageElementUtils.findMember(
					processingEnvironment, "tk.labyrinth.jaap.template.element.util.PackageElementUtilsTest",
					typeSelectionContext).toString());
			Assertions.assertNull(PackageElementUtils.findMember(
					processingEnvironment, "tk.labyrinth.jaap.template.element.util.PackageElementUtilsTest",
					packageSelectionContext));
		}
		{
			Assertions.assertEquals("tk.labyrinth.jaap", PackageElementUtils.findMember(
					processingEnvironment, "tk.labyrinth.jaap", packageSelectionContext).toString());
			Assertions.assertNull(PackageElementUtils.findMember(
					processingEnvironment, "tk.labyrinth", packageSelectionContext));
		}
	}

	@Test
	void testFromStream(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals("tk.labyrinth.jaap.template.element.util", PackageElementUtils.resolve(
				processingEnvironment, Stream.of("tk.labyrinth", "jaap.template.element", "util")).toString());
	}

	@Test
	void testFromString(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals("tk.labyrinth.jaap.template.element.util", PackageElementUtils.resolve(
				processingEnvironment, "tk.labyrinth.jaap.template.element.util").toString());
	}
}
