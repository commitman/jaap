package tk.labyrinth.jaap.template.element.synthetic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;

@ExtendWithJaap
class SyntheticFormalParameterElementTemplateTest {

	@Test
	void testGetDeclaration(ProcessingContext context) {
		FormalParameterDeclaration parameter0 = FormalParameterDeclaration.builder()
				.name("dst")
				.type(TypeDescription.of("byte[]"))
				.build();
		FormalParameterDeclaration parameter1 = FormalParameterDeclaration.builder()
				.name("dstBegin")
				.type(TypeDescription.of("int"))
				.build();
		FormalParameterDeclaration parameter2 = FormalParameterDeclaration.builder()
				.name("coder")
				.type(TypeDescription.of("byte"))
				.build();
		//
		SyntheticMethodElementTemplate template = SyntheticMethodElementTemplate.from(
				context,
				CanonicalTypeSignature.of(String.class),
				MethodDeclaration.builder()
						.formalParameters(List.of(parameter0, parameter1, parameter2))
						.name("test")
						.build());
		//
		Assertions.assertEquals(3, template.getFormalParameterCount());
		Assertions.assertEquals(parameter0, template.getFormalParameter(0).getDeclaration());
		Assertions.assertEquals(parameter1, template.getFormalParameter(1).getDeclaration());
		Assertions.assertEquals(parameter2, template.getFormalParameter(2).getDeclaration());
	}

	@Test
	void testGetSignatureString(ProcessingContext context) {
		FormalParameterDeclaration parameter0 = FormalParameterDeclaration.builder()
				.name("dst")
				.type(TypeDescription.of("byte[]"))
				.build();
		FormalParameterDeclaration parameter1 = FormalParameterDeclaration.builder()
				.name("dstBegin")
				.type(TypeDescription.of("int"))
				.build();
		FormalParameterDeclaration parameter2 = FormalParameterDeclaration.builder()
				.name("coder")
				.type(TypeDescription.of("byte"))
				.build();
		FormalParameterDeclaration parameter3 = FormalParameterDeclaration.builder()
				.name("src")
				.type(TypeDescription.of("byte[][]"))
				.build();
		//
		SyntheticMethodElementTemplate template = SyntheticMethodElementTemplate.from(
				context,
				CanonicalTypeSignature.of(String.class),
				MethodDeclaration.builder()
						.formalParameters(List.of(parameter0, parameter1, parameter2, parameter3))
						.name("test")
						.build());
		//
		Assertions.assertEquals(4, template.getFormalParameterCount());
		Assertions.assertEquals(
				"java.lang:String#test(byte[],int,byte,byte[][])#0",
				template.getFormalParameter(0).getSignatureString());
		Assertions.assertEquals(
				"java.lang:String#test(byte[],int,byte,byte[][])#1",
				template.getFormalParameter(1).getSignatureString());
		Assertions.assertEquals(
				"java.lang:String#test(byte[],int,byte,byte[][])#2",
				template.getFormalParameter(2).getSignatureString());
		Assertions.assertEquals(
				"java.lang:String#test(byte[],int,byte,byte[][])#3",
				template.getFormalParameter(3).getSignatureString());
	}
}
