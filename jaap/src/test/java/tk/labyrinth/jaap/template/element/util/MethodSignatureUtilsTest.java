package tk.labyrinth.jaap.template.element.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.Collection;
import java.util.List;

@ExtendWithJaap
class MethodSignatureUtilsTest {

	@Test
	void testCreateFull(ProcessingEnvironment processingEnvironment) {
		{
			Assertions.assertEquals(
					MethodSignatureUtils.createFull(processingEnvironment, "java.lang.Object#wait(long,int)"),
					MethodSignatureUtils.createFull(processingEnvironment, "java.lang.Object#wait(long,int)"));
		}
		{
			TypeElement collection = TypeElementUtils.get(processingEnvironment, Collection.class);
			//
			TypeMirror asType = collection.asType();
			TypeMirror erasure = processingEnvironment.getTypeUtils().erasure(asType);
			//
			Assertions.assertNotEquals(asType, erasure);
			Assertions.assertEquals(
					MethodSignatureUtils.createFull(processingEnvironment, collection, "addAll", List.of(asType)),
					MethodSignatureUtils.createFull(processingEnvironment, collection, "addAll", List.of(erasure)));
		}
	}
}