package tk.labyrinth.jaap.template;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.math.BigDecimal;

@ExtendWithJaap
class ElementTemplateTest {

	@Test
	void testResolveType(ProcessingContext context) {
		{
			// Field
			Assertions.assertEquals(
					context.getTypeHandle(BigDecimal.class),
					context.getFieldElementTemplate(BigDecimal.class, "ZERO").resolveType());
		}
		{
			// Method
			Assertions.assertEquals(
					context.getTypeHandle(int.class),
					context.getMethodElementTemplate(BigDecimal.class, "precision(long,long)").resolveType());
		}
		{
			// Package
			ContribAssertions.assertThrows(
					() -> context.getPackageElementTemplateOf(BigDecimal.class).resolveType(),
					fault -> {
						Assertions.assertEquals(UnsupportedOperationException.class, fault.getClass());
						Assertions.assertEquals(
								"type = tk.labyrinth.jaap.model.element.template.lang.LangPackageElementTemplate & value = java.math",
								fault.getMessage());
					});
			ContribAssertions.assertThrows(
					() -> context.getPackageElementTemplate("tk.labyrinth", true).resolveType(),
					fault -> {
						Assertions.assertEquals(UnsupportedOperationException.class, fault.getClass());
						Assertions.assertEquals(
								"type = tk.labyrinth.jaap.model.element.template.synthetic.SyntheticPackageElementTemplate & value = tk.labyrinth",
								fault.getMessage());
					});
		}
		{
			// Type
			Assertions.assertEquals(
					context.getTypeHandle(BigDecimal.class),
					context.getTypeElementTemplate(BigDecimal.class).resolveType());
		}
	}
}
