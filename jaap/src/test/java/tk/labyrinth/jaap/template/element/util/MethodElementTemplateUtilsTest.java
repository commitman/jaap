package tk.labyrinth.jaap.template.element.util;

import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.test.model.VarargMethodsAndInheritance;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.Set;
import java.util.stream.Stream;

@ExtendWithJaap
class MethodElementTemplateUtilsTest {

	@Test
	void testFilterNonOverriden(ProcessingContext context) {
		TypeElementTemplate childTypeElementTemplate = context.getTypeElementTemplate(VarargMethodsAndInheritance.Child.class);
		//
		ContribAssertions.assertEquals(
				Set.of(
						"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Child#array(int[])",
						"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Child#vararg(int[])"),
				MethodElementTemplateUtils
						.filterNonOverriden(Stream.concat(
								childTypeElementTemplate.getDeclaredMethods(),
								childTypeElementTemplate.getSuperclass().getDeclaredMethods()))
						.map(MethodElementTemplate::toString));
	}
}
