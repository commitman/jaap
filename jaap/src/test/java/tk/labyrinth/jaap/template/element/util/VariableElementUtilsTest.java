package tk.labyrinth.jaap.template.element.util;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import java.math.BigDecimal;

@ExtendWithJaap
class VariableElementUtilsTest {

	@Test
	void testParseFieldFullName() {
		Assertions.assertEquals(Pair.of("java.lang.System", "out"),
				VariableElementUtils.parseFieldFullName("java.lang.System#out"));
	}

	@Test
	void testRequireField(ProcessingEnvironment processingEnvironment) {
		TypeElement typeElement = processingEnvironment.getElementUtils().getTypeElement(String.class.getCanonicalName());
		{
			VariableElement hashField = (VariableElement) typeElement.getEnclosedElements().stream()
					.filter(element -> element.getSimpleName().contentEquals("hash"))
					.findFirst().orElseThrow();
			Assertions.assertDoesNotThrow(() -> VariableElementUtils.requireField(hashField));
		}
		{
			ExecutableElement concatMethod = (ExecutableElement) typeElement.getEnclosedElements().stream()
					.filter(element -> element.getSimpleName().contentEquals("concat"))
					.findFirst().orElseThrow();
			VariableElement strParameter = concatMethod.getParameters().get(0);
			ContribAssertions.assertThrows(() -> VariableElementUtils.requireField(strParameter), fault -> {
				Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
				Assertions.assertEquals("Require field: str", fault.getMessage());
			});
		}
		{
			// TODO: Field in interface
		}
	}

	@Test
	void testResolveField(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals("ZERO", VariableElementUtils.resolveField(processingEnvironment,
				BigDecimal.class, "ZERO").toString());
	}

	@Test
	void testResolveParameter(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals("timeoutMillis", VariableElementUtils.resolveParameter(processingEnvironment,
				Object.class, "wait(long,int)", 0).toString());
		Assertions.assertEquals("nanos", VariableElementUtils.resolveParameter(processingEnvironment,
				Object.class, "wait(long,int)", 1).toString());
	}
}
