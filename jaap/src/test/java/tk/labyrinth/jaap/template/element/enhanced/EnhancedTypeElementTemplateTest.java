package tk.labyrinth.jaap.template.element.enhanced;

import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.template.element.synthetic.SyntheticElementTemplateRegistry;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import javax.annotation.processing.ProcessingEnvironment;

@ExtendWithJaap
class EnhancedTypeElementTemplateTest {

	@Test
	void testProcessingContextGetTypeElementTemplate(ProcessingEnvironment processingEnvironment) {
		ProcessingContext processingContext = EnhancedProcessing.createContext(processingEnvironment,
				new SyntheticElementTemplateRegistry());
		//
		Class<?> type = String.class;
		String typeName = type.getCanonicalName();
		//
		ContribAssertions.assertInstanceOf(EnhancedTypeElementTemplate.class, processingContext.getTypeElementTemplate(type));
		ContribAssertions.assertInstanceOf(EnhancedTypeElementTemplate.class, processingContext.getTypeElementTemplate(typeName));
	}
}
