package tk.labyrinth.jaap.template.element.synthetic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.test.model.declaration.TestDeclarations;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.math.BigDecimal;
import java.util.Map;

@ExtendWithJaap
class SyntheticMethodElementTemplateTest {

	@Test
	void testBigDecimalGreaterThan(ProcessingContext context) {
		// Variant with one parameter.
		//
		SyntheticMethodElementTemplate methodElement = SyntheticMethodElementTemplate.from(
				context, CanonicalTypeSignature.of(BigDecimal.class),
				TestDeclarations.greaterThanBigDecimal());
		//
		Assertions.assertEquals(
				"java.math:BigDecimal#greaterThan(java.math:BigDecimal)",
				methodElement.getFullSignature().toString());
		Assertions.assertEquals(
				"greaterThan(java.math:BigDecimal)",
				methodElement.getSimpleSignature().toString());
		Assertions.assertEquals(
				"java.math:BigDecimal#greaterThan(java.math:BigDecimal)",
				methodElement.toString());
		Assertions.assertEquals(1, methodElement.getFormalParameterCount());
		Assertions.assertEquals(
				"java.math:BigDecimal#greaterThan(java.math:BigDecimal)#0",
				methodElement.getFormalParameter(0).getSignatureString());
		Assertions.assertEquals(
				"java.math:BigDecimal#greaterThan(java.math:BigDecimal)#0",
				methodElement.getFormalParameter(0).toString());
	}

	@Test
	void testEquals(ProcessingContext context) {
		SyntheticMethodElementTemplate methodElement0 = SyntheticMethodElementTemplate.from(
				context, CanonicalTypeSignature.of(BigDecimal.class),
				TestDeclarations.greaterThanBigDecimal());
		SyntheticMethodElementTemplate methodElement1 = SyntheticMethodElementTemplate.from(
				context, CanonicalTypeSignature.of(BigDecimal.class),
				TestDeclarations.greaterThanBigDecimal());
		//
		Assertions.assertNotSame(methodElement0, methodElement1);
		Assertions.assertEquals(methodElement0, methodElement1);
	}

	@Test
	void testMapEntryDoNothing(ProcessingContext context) {
		// Variant with nested type.
		//
		SyntheticMethodElementTemplate methodElement = SyntheticMethodElementTemplate.from(
				context,
				CanonicalTypeSignature.of(Map.Entry.class),
				MethodDeclaration.builder()
						.name("doNothing")
						.build());
		Assertions.assertEquals("java.util:Map.Entry#doNothing()", methodElement.getFullSignature().toString());
		Assertions.assertEquals("doNothing()", methodElement.getSimpleSignature().toString());
		Assertions.assertEquals("java.util:Map.Entry#doNothing()", methodElement.toString());
		Assertions.assertEquals(0, methodElement.getFormalParameterCount());
	}

	@Test
	void testStringToBigDecimal(ProcessingContext context) {
		// Simple variant.
		//
		SyntheticMethodElementTemplate methodElement = SyntheticMethodElementTemplate.from(
				context,
				CanonicalTypeSignature.of(String.class),
				MethodDeclaration.builder()
						.name("toBigDecimal")
						.returnType(TypeDescription.ofNonParameterized(BigDecimal.class))
						.build());
		Assertions.assertEquals("java.lang:String#toBigDecimal()", methodElement.getFullSignature().toString());
		Assertions.assertEquals("toBigDecimal()", methodElement.getSimpleSignature().toString());
		Assertions.assertEquals("java.lang:String#toBigDecimal()", methodElement.toString());
		Assertions.assertEquals(0, methodElement.getFormalParameterCount());
	}
}
