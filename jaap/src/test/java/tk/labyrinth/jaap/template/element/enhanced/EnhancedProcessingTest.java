package tk.labyrinth.jaap.template.element.enhanced;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.synthetic.SyntheticElementTemplateRegistry;
import tk.labyrinth.jaap.test.model.declaration.TestDeclarations;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.ProcessingEnvironment;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@ExtendWithJaap
class EnhancedProcessingTest {

	// TODO: This should support signatures without colons.
	@Test
	void testBigDecimalGreaterThan(ProcessingEnvironment processingEnvironment) {
		SyntheticElementTemplateRegistry syntheticElementTemplateRegistry = new SyntheticElementTemplateRegistry();
		syntheticElementTemplateRegistry.registerMethodDeclaration(
				CanonicalTypeSignature.of(BigDecimal.class),
				TestDeclarations.greaterThanBigDecimal());
		//
		ProcessingContext processingContext = EnhancedProcessing.createContext(
				processingEnvironment,
				syntheticElementTemplateRegistry);
		TypeElementTemplate bigDecimalTemplate = processingContext.getTypeElementTemplate(BigDecimal.class);
		{
			MethodElementTemplate greaterThanTemplate = processingContext.getMethodElementTemplate(
					"java.math:BigDecimal#greaterThan(java.math:BigDecimal)");
			Assertions.assertNotNull(greaterThanTemplate);
			Assertions.assertEquals(bigDecimalTemplate, greaterThanTemplate.getParent());
			Assertions.assertEquals(
					processingContext.getTypeHandle(boolean.class),
					greaterThanTemplate.getReturnType());
		}
		{
			List<MethodElementTemplate> declaredMethods = bigDecimalTemplate.getDeclaredMethods()
					.collect(Collectors.toList());
			MethodElementTemplate greaterThanTemplate = declaredMethods.stream()
					.filter(declaredMethod -> Objects.equals(declaredMethod.getSimpleNameAsString(), "greaterThan"))
					.findAny().orElse(null);
			Assertions.assertNotNull(greaterThanTemplate);
			Assertions.assertEquals(bigDecimalTemplate, greaterThanTemplate.getParent());
			Assertions.assertEquals(
					processingContext.getTypeHandle(boolean.class),
					greaterThanTemplate.getReturnType());
		}
		{
			List<MethodElementTemplate> allMethods = bigDecimalTemplate.getAllMethods().collect(Collectors.toList());
			MethodElementTemplate greaterThanTemplate = allMethods.stream()
					.filter(methodTemplate -> Objects.equals(methodTemplate.getSimpleNameAsString(), "greaterThan"))
					.findAny().orElse(null);
			Assertions.assertNotNull(greaterThanTemplate);
			Assertions.assertEquals(bigDecimalTemplate, greaterThanTemplate.getParent());
			Assertions.assertEquals(
					processingContext.getTypeHandle(boolean.class),
					greaterThanTemplate.getReturnType());
		}
		{
			MethodElementTemplate greaterThanTemplate = bigDecimalTemplate.selectMethodElement(
					"greaterThan",
					processingContext.getTypeHandle(BigDecimal.class));
			Assertions.assertNotNull(greaterThanTemplate);
			Assertions.assertEquals(bigDecimalTemplate, greaterThanTemplate.getParent());
			Assertions.assertEquals(
					processingContext.getTypeHandle(boolean.class),
					greaterThanTemplate.getReturnType());
		}
	}
}
