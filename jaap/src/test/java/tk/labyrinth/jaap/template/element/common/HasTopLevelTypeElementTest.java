package tk.labyrinth.jaap.template.element.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.Map;

@ExtendWithJaap
class HasTopLevelTypeElementTest {

	@Test
	void testFormalParameterElementTemplate(ProcessingContext context) {
		Assertions.assertEquals(
				context.getTypeElementTemplate(Map.class),
				context.getMethodElementTemplateByName(Map.class, "containsKey")
						.getFormalParameter(0).getTopLevelTypeElement());
		Assertions.assertEquals(
				context.getTypeElementTemplate(Map.class),
				context.getMethodElementTemplateByName(Map.Entry.class, "setValue")
						.getFormalParameter(0).getTopLevelTypeElement());
	}

	@Test
	void testMethodElementTemplate(ProcessingContext context) {
		Assertions.assertEquals(
				context.getTypeElementTemplate(Map.class),
				context.getMethodElementTemplateByName(Map.class, "size").getTopLevelTypeElement());
		Assertions.assertEquals(
				context.getTypeElementTemplate(Map.class),
				context.getMethodElementTemplateByName(Map.Entry.class, "getKey").getTopLevelTypeElement());
	}

	@Test
	void testTypeElementTemplate(ProcessingContext context) {
		Assertions.assertEquals(
				context.getTypeElementTemplate(Map.class),
				context.getTypeElementTemplate(Map.class).getTopLevelTypeElement());
		Assertions.assertEquals(
				context.getTypeElementTemplate(Map.class),
				context.getTypeElementTemplate(Map.Entry.class).getTopLevelTypeElement());
	}
}
