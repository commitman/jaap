package tk.labyrinth.jaap.template.element.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.Map;

@ExtendWithJaap
class HasSignatureTest {

	@Test
	void testGetSignatureOfEnclosedTypeElement(ProcessingContext processingContext) {
		Assertions.assertEquals(
				"java.util:Map.Entry",
				processingContext.getTypeElementTemplate(Map.Entry.class).getSignature().toString());
	}

	@Disabled
	@Test
	void testGetSignatureOfMethodTypeParameterElement(ProcessingContext processingContext) {
		// TODO
//		Assertions.assertEquals(
//				"java.util.Map",
//				processingContext.getMethodElementTemplateByName(Optional.class, "map").getSignature());
	}

	@Test
	void testGetSignatureOfTopLevelTypeElement(ProcessingContext processingContext) {
		Assertions.assertEquals(
				"java.util:Map",
				processingContext.getTypeElementTemplate(Map.class).getSignature().toString());
	}

	@Test
	void testGetSignatureOfTypeTypeParameterElement(ProcessingContext processingContext) {
		Assertions.assertEquals(
				"java.util:Map%K",
				processingContext.getTypeElementTemplate(Map.class).getTypeParameter(0).getSignature());
	}
}
