package tk.labyrinth.jaap.template.element.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.testing.model.MemberAmbiguity;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@ExtendWithJaap
class TypeElementTemplateImplTest {

	@Disabled
	@Test
	void testGetAllMethods(ProcessingContext context) {
		// TODO: Make it check it finds methods in supers.
//		Assertions.assertEquals(167, context.getTypeElementTemplate(BigDecimal.class).getAllMethods().count());
//		Assertions.assertEquals(18, context.getTypeElementTemplate(Number.class).getAllMethods().count());
//		Assertions.assertEquals(13, context.getTypeElementTemplate(Comparable.class).getAllMethods().count());
//		Assertions.assertEquals(12, context.getTypeElementTemplate(Object.class).getAllMethods().count());
	}

	@Test
	void testGetAllMethodsAndDeclaredMethods(ProcessingContext context) {
		// FIXME: Replace with 2 proper tests.
		Assertions.assertEquals(context.getTypeElementTemplate(Object.class).getDeclaredMethods().count(),
				context.getTypeElementTemplate(Object.class).getAllMethods().count());
		Assertions.assertTrue(context.getTypeElementTemplate(Comparable.class).getDeclaredMethods().count() <
				context.getTypeElementTemplate(Comparable.class).getAllMethods().count());
		Assertions.assertTrue(context.getTypeElementTemplate(Number.class).getDeclaredMethods().count() <
				context.getTypeElementTemplate(Number.class).getAllMethods().count());
	}

	@Test
	void testGetDeclaredMethods(ProcessingContext context) {
		// FIXME: Number may be different for different versions of Java.
//		Assertions.assertEquals(148, context.getTypeElementTemplate(BigDecimal.class).getDeclaredMethods().count());
//		Assertions.assertEquals(6, context.getTypeElementTemplate(Number.class).getDeclaredMethods().count());
//		Assertions.assertEquals(1, context.getTypeElementTemplate(Comparable.class).getDeclaredMethods().count());
//		Assertions.assertEquals(12, context.getTypeElementTemplate(Object.class).getDeclaredMethods().count());
	}

	@Test
	void testGetTypeChain(ProcessingContext processingContext) {
		ContribAssertions.assertEquals(List.of(
				processingContext.getTypeElementTemplate(Map.class)
		), processingContext.getTypeElementTemplate(Map.class).getTypeChain());
		ContribAssertions.assertEquals(List.of(
				processingContext.getTypeElementTemplate(Map.Entry.class),
				processingContext.getTypeElementTemplate(Map.class)
		), processingContext.getTypeElementTemplate(Map.Entry.class).getTypeChain());
	}

	@Test
	void testSelectMemberWithChain(ProcessingContext processingContext) {
		{
			TypeElementTemplate typeElementTemplate = processingContext.getTypeElementTemplate(MemberAmbiguity.class);
			ElementTemplate memberTemplate = typeElementTemplate.selectMember(EntitySelectorChain.forVariable(Stream.of("Member")));
			Assertions.assertNotNull(memberTemplate);
			Assertions.assertTrue(memberTemplate.isFieldElement());
			Assertions.assertEquals("Member", memberTemplate.getSimpleName().toString());
		}
		{
			TypeElementTemplate typeElementTemplate = processingContext.getTypeElementTemplate(MemberAmbiguity.WithNestedType.class);
			ElementTemplate memberTemplate = typeElementTemplate.selectMember(EntitySelectorChain.forType(Stream.of("Member")));
			Assertions.assertNotNull(memberTemplate);
			Assertions.assertTrue(memberTemplate.isTypeElement());
			Assertions.assertEquals("Member", memberTemplate.getSimpleName().toString());
		}
	}

	@Test
	void testSelectMemberWithSelector(ProcessingContext processingContext) {
		{
			TypeElementTemplate typeElementTemplate = processingContext.getTypeElementTemplate(MemberAmbiguity.class);
			ElementTemplate memberTemplate = typeElementTemplate.selectMember(
					EntitySelector.build(EntitySelectionContext.forVariable(), "Member"));
			Assertions.assertNotNull(memberTemplate);
			Assertions.assertTrue(memberTemplate.isFieldElement());
			Assertions.assertEquals("Member", memberTemplate.getSimpleName().toString());
		}
		{
			TypeElementTemplate typeElementTemplate = processingContext.getTypeElementTemplate(MemberAmbiguity.WithNestedType.class);
			ElementTemplate memberTemplate = typeElementTemplate.selectMember(
					EntitySelector.build(EntitySelectionContext.forType(), "Member"));
			Assertions.assertNotNull(memberTemplate);
			Assertions.assertTrue(memberTemplate.isTypeElement());
			Assertions.assertEquals("Member", memberTemplate.getSimpleName().toString());
		}
	}

	@Test
	void testSelectMethodElementOrFail(ProcessingContext processingContext) {
		{
			// Choosing between java.lang.String#length() and java.lang.CharSequence#length().
			//
			Assertions.assertEquals(
					processingContext.getTypeElementTemplate(String.class),
					processingContext.getTypeElementTemplate(String.class)
							.selectMethodElementOrFail(MethodSimpleSignature.of("length()"))
							.getParent());
		}
	}

	@Test
	void testSelectMethodElementWithMethodSimpleSignature(ProcessingContext processingContext) {
		TypeElementTemplate typeElementTemplate = processingContext.getTypeElementTemplate(String.class);
		{
			MethodElementTemplate methodTemplate = typeElementTemplate.selectMethodElement(
					MethodSimpleSignature.of("valueOf(int)"));
			Assertions.assertNotNull(methodTemplate);
			Assertions.assertEquals("java.lang:String#valueOf(int)", methodTemplate.toString());
		}
		{
			MethodElementTemplate methodTemplate = typeElementTemplate.selectMethodElement(
					MethodSimpleSignature.of("valueOf(java.lang.Object)"));
			Assertions.assertNotNull(methodTemplate);
			Assertions.assertEquals("java.lang:String#valueOf(java.lang:Object)", methodTemplate.toString());
		}
	}

	@Test
	void testSelectMethodElementWithStringAndTypeTemplateArray(ProcessingContext processingContext) {
		TypeElementTemplate typeElementTemplate = processingContext.getTypeElementTemplate(String.class);
		{
			MethodElementTemplate methodTemplate = typeElementTemplate.selectMethodElement(
					"valueOf",
					processingContext.getTypeHandle(int.class));
			Assertions.assertNotNull(methodTemplate);
			Assertions.assertEquals("java.lang:String#valueOf(int)", methodTemplate.toString());
		}
		{
			MethodElementTemplate methodTemplate = typeElementTemplate.selectMethodElement(
					"valueOf",
					processingContext.getTypeHandle(Object.class));
			Assertions.assertNotNull(methodTemplate);
			Assertions.assertEquals("java.lang:String#valueOf(java.lang:Object)", methodTemplate.toString());
		}
	}
}
