package tk.labyrinth.jaap.context;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;
import java.util.stream.Collectors;

@CompilationTarget(sourceNames = {
		"tk.labyrinth.jaap.test.Dummy",
		"tk.labyrinth.jaap.test.package-info",
		"tk.labyrinth.jaap.test.pkg.package-info"})
@ExtendWithJaap
class RoundContextImplTest {

	@Test
	void testGetAllTypeElements(RoundContext roundContext) {
		Assertions.assertEquals(
				List.of(
						"tk.labyrinth.jaap.test:Dummy",
						"tk.labyrinth.jaap.test:Dummy.StaticNested"),
				roundContext.getAllTypeElements()
						.map(TypeElementTemplate::toString)
						.sorted()
						.collect(Collectors.toList()));
	}

	@Test
	void testGetPackageElements(RoundContext roundContext) {
		Assertions.assertEquals(
				List.of(
						"tk.labyrinth.jaap.test",
						"tk.labyrinth.jaap.test.pkg"),
				roundContext.getPackageElements()
						.map(PackageElementTemplate::toString)
						.sorted()
						.collect(Collectors.toList()));
	}

	@Test
	void testGetTopLevelElements(RoundContext roundContext) {
		Assertions.assertEquals(
				List.of(
						"tk.labyrinth.jaap.test",
						"tk.labyrinth.jaap.test.pkg",
						"tk.labyrinth.jaap.test:Dummy"),
				roundContext.getTopLevelElements()
						.map(ElementTemplate::toString)
						.sorted()
						.collect(Collectors.toList()));
	}
}
