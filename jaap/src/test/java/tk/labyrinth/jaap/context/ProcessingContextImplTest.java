package tk.labyrinth.jaap.context;

import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.Map;

@ExtendWithJaap
class ProcessingContextImplTest {

	@SuppressWarnings("ConstantConditions")
	@Test
	void testFindDeclaredTypeHandle(ProcessingContext processingContext) {
		Assertions.assertEquals(
				"java.lang:String",
				processingContext.findDeclaredTypeHandle("java.lang:String").toString());
		Assertions.assertEquals(
				"java.util:Map.Entry",
				processingContext.findDeclaredTypeHandle("java.util:Map.Entry").toString());
		Assertions.assertNull(processingContext.findDeclaredTypeHandle("String"));
	}

	@Test
	void testGetElementTemplateWithString(ProcessingContext context) {
		Assertions.assertEquals(context.getTypeElementTemplate(Map.Entry.class),
				context.getElementTemplate("java.util:Map.Entry"));
		Assertions.assertEquals(context.getTypeElementTemplate(Map.Entry.class).selectMethodElement("getKey"),
				context.getElementTemplate("java.util:Map.Entry#getKey()"));
	}

	@Test
	void testGetMethodElementTemplateWithString(ProcessingContext context) {
		Assertions.assertEquals(
				"java.lang:Object#wait()",
				context.getMethodElementTemplate("java.lang:Object#wait()").toString());
		Assertions.assertEquals(
				"java.lang:Object#wait(long)",
				context.getMethodElementTemplate("java.lang:Object#wait(long)").toString());
		Assertions.assertEquals(
				"java.lang:Object#wait(long,int)",
				context.getMethodElementTemplate("java.lang:Object#wait(long,int)").toString());
		//
		Assertions.assertEquals(
				"java.util:Collection#addAll(java.util:Collection)",
				context.getMethodElementTemplate(
						"java.util:Collection#addAll(java.util:Collection)")
						.toString());
		{
			// Vararg
			Assertions.assertEquals(
					"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Parent#vararg(int[])",
					context.getMethodElementTemplate(
							"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Parent#vararg(int[])")
							.toString());
			ContribAssertions.assertThrows(
					() -> context.getMethodElementTemplate(
							"tk.labyrinth.jaap.test.model:VarargMethodsAndInheritance.Parent#vararg(int...)"),
					fault -> Assertions.assertEquals(
							"java.lang.IllegalArgumentException: No TypeElement resolved: typeFullName = int...",
							fault.toString()));
		}
	}

	@Test
	void testGetName(ProcessingContext context) {
		Assertions.assertEquals("", context.getName("").toString());
		Assertions.assertEquals("foo", context.getName("foo").toString());
		Assertions.assertEquals("foo.bar", context.getName("foo.bar").toString());
		Assertions.assertEquals("foo.Bar", context.getName("foo.Bar").toString());
		//
		ContribAssertions.assertThrows(
				() -> context.getName(null),
				fault -> Assertions.assertEquals(NullPointerException.class, fault.getClass()));
	}

	@Test
	void testGetPackageWhenAbsent(ProcessingContext processingContext) {
		{
			val template = processingContext.getPackageElementTemplate(
					"tk.labyrinth",
					true);
			Assertions.assertEquals("tk.labyrinth", template.getQualifiedName());
			Assertions.assertTrue(template.isSynthetic());
		}
		{
			ContribAssertions.assertThrows(
					() -> processingContext.getPackageElementTemplate("tk.labyrinth"),
					fault -> {
						Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
						Assertions.assertEquals(
								"Not found: packageSignatureString = tk.labyrinth",
								fault.getMessage());
					});
		}
	}

	@Test
	void testGetTypeParameterElementTemplateWithClassAndString(ProcessingContext context) {
		Assertions.assertEquals("java.util:Map%K",
				context.getTypeParameterElementTemplate(Map.class, "K").toString());
		Assertions.assertEquals("java.util:Map.Entry%V",
				context.getTypeParameterElementTemplate(Map.Entry.class, "V").toString());
	}

	@Test
	void testGetTypeParameterElementTemplateWithString(ProcessingContext context) {
		Assertions.assertEquals("java.util:Map%K",
				context.getTypeParameterElementTemplate("java.util:Map%K").toString());
		Assertions.assertEquals("java.util:Map.Entry%V",
				context.getTypeParameterElementTemplate("java.util:Map.Entry%V").toString());
		// TODO: Methods & Constructors
	}
}
