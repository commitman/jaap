package tk.labyrinth.jaap.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import java.util.Map;
import java.util.Optional;

@ExtendWithJaap
class ParameterizableUtilsTest {

	@Test
	void testFindTypeParameter(ProcessingEnvironment processingEnvironment) {
		{
			TypeElement typeElement = TypeElementUtils.get(processingEnvironment, Map.class);
			//
			Assertions.assertEquals(
					typeElement.getTypeParameters().get(0),
					ParameterizableUtils.findTypeParameter(typeElement, "K"));
			Assertions.assertEquals(
					typeElement.getTypeParameters().get(1),
					ParameterizableUtils.findTypeParameter(typeElement, "V"));
		}
		{
			TypeElement typeElement = TypeElementUtils.get(processingEnvironment, Optional.class);
			ExecutableElement executableElement = TypeElementUtils.getDeclaredMethod(
					processingEnvironment,
					typeElement,
					"map(java.util.function.Function)");
			//
			Assertions.assertEquals(
					executableElement.getTypeParameters().get(0),
					ParameterizableUtils.findTypeParameter(executableElement, "U"));
			//
			Assertions.assertNull(ParameterizableUtils.findTypeParameter(executableElement, "T", false));
			Assertions.assertEquals(
					typeElement.getTypeParameters().get(0),
					ParameterizableUtils.findTypeParameter(executableElement, "T", true));
		}
	}
}