package tk.labyrinth.jaap.util;

import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.template.element.util.MethodSignatureUtils;
import tk.labyrinth.jaap.template.element.util.VariableElementUtils;
import tk.labyrinth.jaap.test.model.MethodParameters;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.CollectionAssertions;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import java.io.PrintStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@ExtendWithJaap
class TypeElementUtilsTest {

	@Test
	void testFindDeclaredMethod(ProcessingEnvironment processingEnvironment) {
		{
			MethodFullSignature methodFullSignature =
					MethodSignatureUtils.createFull(processingEnvironment, MethodParameters.class, "object(java.lang.Object)");
			ExecutableElement declaredMethod = TypeElementUtils.findDeclaredMethod(processingEnvironment, methodFullSignature);
			Assertions.assertNotNull(declaredMethod);
		}
		{
			MethodFullSignature methodFullSignature =
					MethodSignatureUtils.createFull(processingEnvironment, MethodParameters.class, "string(java.lang.String)");
			ExecutableElement declaredMethod = TypeElementUtils.findDeclaredMethod(processingEnvironment, methodFullSignature);
			Assertions.assertNotNull(declaredMethod);
		}
	}

	@Test
	void testGetAllFields(ProcessingEnvironment processingEnvironment) {
		ContribAssertions.assertEquals(Set.of(
				VariableElementUtils.resolveField(processingEnvironment, AtomicInteger.class, "serialVersionUID"),
				VariableElementUtils.resolveField(processingEnvironment, Number.class, "serialVersionUID"),
				VariableElementUtils.resolveField(processingEnvironment, AtomicInteger.class, "U"),
				VariableElementUtils.resolveField(processingEnvironment, AtomicInteger.class, "value"),
				VariableElementUtils.resolveField(processingEnvironment, AtomicInteger.class, "VALUE")
		), TypeElementUtils.getAllFields(processingEnvironment, TypeElementUtils.get(processingEnvironment, AtomicInteger.class)));
		//
		{
			// TODO: Static fields in interfaces
			// TODO: Enums
		}
	}

	@Test
	void testGetBinaryName(ProcessingEnvironment processingEnvironment) {
		{
			TypeElement typeElement = TypeElementUtils.get(processingEnvironment, Map.class);
			String binaryName = TypeElementUtils.getBinaryName(typeElement);
			//
			Assertions.assertEquals("java.util.Map", binaryName);
			Assertions.assertEquals(processingEnvironment.getElementUtils().getBinaryName(typeElement).toString(), binaryName);
		}
		{
			TypeElement typeElement = TypeElementUtils.get(processingEnvironment, Map.Entry.class);
			String binaryName = TypeElementUtils.getBinaryName(typeElement);
			//
			Assertions.assertEquals("java.util.Map$Entry", binaryName);
			Assertions.assertEquals(processingEnvironment.getElementUtils().getBinaryName(typeElement).toString(), binaryName);
		}
	}

	@Test
	void testGetDeclaredField(ProcessingEnvironment processingEnvironment) {
		TypeElement typeElement = TypeElementUtils.get(processingEnvironment, BigDecimal.class);
		{
			VariableElement field = TypeElementUtils.getDeclaredField(typeElement, "intVal");
			Assertions.assertEquals(Set.of(Modifier.PRIVATE, Modifier.FINAL), field.getModifiers());
			Assertions.assertEquals("java.math.BigInteger", field.asType().toString());
			Assertions.assertEquals("intVal", field.getSimpleName().toString());
		}
		{
			VariableElement field = TypeElementUtils.getDeclaredField(typeElement, "ZERO");
			Assertions.assertEquals(Set.of(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL), field.getModifiers());
			Assertions.assertEquals("java.math.BigDecimal", field.asType().toString());
			Assertions.assertEquals("ZERO", field.getSimpleName().toString());
		}
	}

	@Test
	void testGetDeclaredMethod(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals(
				"wait(long,int)",
				TypeElementUtils
						.getDeclaredMethod(processingEnvironment, "java.lang.Object#wait(long,int)")
						.toString());
		Assertions.assertEquals(
				"wait(long,int)",
				TypeElementUtils
						.getDeclaredMethod(
								processingEnvironment,
								MethodSignatureUtils.createFull(processingEnvironment, "java.lang.Object#wait(long,int)"))
						.toString());
		//
		ContribAssertions.assertThrows(() -> TypeElementUtils.getDeclaredMethod(
				processingEnvironment, "java.lang.Object#wait(int,int)"), fault -> {
			Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
			Assertions.assertEquals("Require non-empty stream", fault.getMessage());
		});
	}

	@Test
	void testGetDeclaredMethods(ProcessingEnvironment processingEnvironment) {
		TypeElement typeElement = TypeElementUtils.get(processingEnvironment, Iterator.class);
		//
		ContribAssertions.assertEquals(List.of(
				"forEachRemaining(java.util.function.Consumer<? super E>)",
				"hasNext()",
				"next()",
				"remove()"
		), TypeElementUtils.getDeclaredMethods(typeElement)
				.map(ExecutableElement::toString)
				.sorted());
	}

	@Test
	void testGetDeclaredMethodsWithName(ProcessingEnvironment processingEnvironment) {
		TypeElement typeElement = TypeElementUtils.get(processingEnvironment, Object.class);
		//
		ContribAssertions.assertEquals(List.of(
				"wait()",
				"wait(long)",
				"wait(long,int)"
		), TypeElementUtils.getDeclaredMethods(typeElement, "wait")
				.map(ExecutableElement::toString)
				.sorted());
		ContribAssertions.assertEquals(List.of(
		), TypeElementUtils.getDeclaredMethods(typeElement, "doNothing")
				.map(ExecutableElement::toString)
				.sorted());
	}

	@Test
	void testGetSignature(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals(
				"java.util:Map.Entry",
				TypeElementUtils.getSignature(TypeElementUtils.get(processingEnvironment, Map.Entry.class)));
	}

	@Test
	void testGetTypeElements(ProcessingEnvironment processingEnvironment) {
		//
		// Since Java 12 Boolean also implements Constable so we have to assert using contains instead of equals.
		CollectionAssertions.assertContainsAll(
				Set.of(
						TypeElementUtils.get(processingEnvironment, Boolean.class),
						TypeElementUtils.get(processingEnvironment, Comparable.class),
						TypeElementUtils.get(processingEnvironment, Object.class),
						TypeElementUtils.get(processingEnvironment, Serializable.class)),
				TypeElementUtils.getTypeElements(
						processingEnvironment,
						TypeElementUtils.get(processingEnvironment, Boolean.class)));
		//
		ContribAssertions.assertEquals(
				Set.of(
						TypeElementUtils.get(processingEnvironment, AbstractMap.class),
						TypeElementUtils.get(processingEnvironment, Cloneable.class),
						TypeElementUtils.get(processingEnvironment, HashMap.class),
						TypeElementUtils.get(processingEnvironment, Map.class),
						TypeElementUtils.get(processingEnvironment, Object.class),
						TypeElementUtils.get(processingEnvironment, Serializable.class)),
				TypeElementUtils.getTypeElements(
						processingEnvironment,
						TypeElementUtils.get(processingEnvironment, HashMap.class)));
	}

	@Test
	void testGetWithClass(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals("java.util.List", TypeElementUtils.get(processingEnvironment,
				List.class).toString());
		Assertions.assertEquals("java.util.Map.Entry", TypeElementUtils.get(processingEnvironment,
				Map.Entry.class).toString());
		//
		ContribAssertions.assertThrows(() -> TypeElementUtils.get(processingEnvironment, int.class), fault -> {
			Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
			Assertions.assertEquals("No TypeElement resolved: type = int", fault.getMessage());
		});
	}

	@CompilationTarget(sourceResources = "NotCompiledClass.java")
	@Test
	void testGetWithString(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals("java.util.List", TypeElementUtils.get(
				processingEnvironment, "java.util.List").toString());
		Assertions.assertEquals("java.util.Map.Entry", TypeElementUtils.get(
				processingEnvironment, "java.util.Map$Entry").toString());
		Assertions.assertEquals("java.util.Map.Entry", TypeElementUtils.get(
				processingEnvironment, "java.util.Map.Entry").toString());
		//
		Assertions.assertEquals("tk.labyrinth.jaap.test.model.resource.NotCompiledClass", TypeElementUtils.get(
				processingEnvironment, "tk.labyrinth.jaap.test.model.resource.NotCompiledClass").toString());
		//
		ContribAssertions.assertThrows(() -> TypeElementUtils.get(processingEnvironment, "int"), fault -> {
			Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
			Assertions.assertEquals("No TypeElement resolved: typeFullName = int", fault.getMessage());
		});
	}

	@Test
	void testGetWithTypeMirror(ProcessingEnvironment processingEnvironment) {
		Assertions.assertEquals("java.util.List", TypeElementUtils.get(
				processingEnvironment, TypeMirrorUtils.resolve(processingEnvironment, List.class)).toString());
		Assertions.assertEquals("java.util.Map.Entry", TypeElementUtils.get(
				processingEnvironment, TypeMirrorUtils.resolve(processingEnvironment, Map.Entry.class)).toString());
		//
		ContribAssertions.assertThrows(() -> TypeElementUtils.get(processingEnvironment,
				TypeMirrorUtils.resolve(processingEnvironment, int.class)), fault -> {
			Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
			Assertions.assertEquals("No TypeElement resolved: typeMirror = int", fault.getMessage());
		});
	}

	@Test
	void testSelectMember(ProcessingEnvironment processingEnvironment) {
		{
			// System.out.println();
			TypeElement typeElement = TypeElementUtils.get(processingEnvironment, PrintStream.class);
			Assertions.assertEquals(
					ExecutableElementUtils.resolve(
							processingEnvironment,
							"java.io:PrintStream#println()"),
					TypeElementUtils.selectMember(
							processingEnvironment,
							typeElement,
							EntitySelector.forMethod("println", Stream.empty())));
		}
		{
			// System.out.println(12);
			TypeElement typeElement = TypeElementUtils.get(processingEnvironment, PrintStream.class);
			Assertions.assertEquals(
					ExecutableElementUtils.resolve(
							processingEnvironment,
							"java.io:PrintStream#println(int)"),
					TypeElementUtils.selectMember(
							processingEnvironment,
							typeElement,
							EntitySelector.forMethod(
									"println",
									Stream.of(
											TypeDescription.ofNonParameterized(int.class)))));
		}
	}

	@Test
	void testSelectMethodMember(ProcessingEnvironment processingEnvironment) {
		val stringTypeElement = TypeElementUtils.get(processingEnvironment, String.class);
		{
			val signature = MethodSimpleSignature.of("wait()");
			val expected = ExecutableElementUtils.get(processingEnvironment, Object.class, signature);
			val actual = TypeElementUtils.selectMethodMember(processingEnvironment, stringTypeElement, signature);
			Assertions.assertEquals(expected, actual);
		}
		{
			val expectedSignature = MethodSignatureUtils.createSimpleFromParameterful(processingEnvironment, "wait", long.class);
			val expected = ExecutableElementUtils.get(processingEnvironment, Object.class, expectedSignature);
			val actualSignature = MethodSignatureUtils.createSimpleFromParameterful(processingEnvironment, "wait", Long.class);
			val actual = TypeElementUtils.selectMethodMember(processingEnvironment, stringTypeElement, actualSignature);
			Assertions.assertEquals(expected, actual);
		}
	}
}
