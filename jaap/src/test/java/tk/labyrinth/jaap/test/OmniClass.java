package tk.labyrinth.jaap.test;

@OmniAnnotation(
		annotationTypes = {
				Override.class,
				SuppressWarnings.class
		},
		annotations = {
				@SuppressWarnings("one"),
				@SuppressWarnings("two")
		},
		string = "one",
		strings = {
				"two",
				"three"
		},
		types = {
				Integer.class,
				String.class
		}
)
public class OmniClass {

}
