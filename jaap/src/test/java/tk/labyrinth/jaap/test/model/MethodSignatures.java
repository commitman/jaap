package tk.labyrinth.jaap.test.model;

import org.checkerframework.checker.fenum.qual.Fenum;

public class MethodSignatures {

	{
		// long over Object.
		receivesLongOrObject(12);
	}

	public static void receivesInt(int value) {
		// no-op
	}

	public static void receivesLongOrObject(long value) {
		// no-op
	}

	public static void receivesLongOrObject(Object value) {
		// no-op
	}

	public static void receivesObject(Object value) {
		// no-op
	}

	// We're using @Fenum as annotation with @Target(TYPE_USE)
	public static void withLongFormalParameterWithTypeUseAnnotation(@Fenum("") long l) {
		// no-op
	}

	// We're using @Fenum as annotation with @Target(TYPE_USE)
	public static void withObjectFormalParameterWithTypeUseAnnotation(@Fenum("") Object obj) {
		// no-op
	}
}
