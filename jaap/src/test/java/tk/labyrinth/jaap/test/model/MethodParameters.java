package tk.labyrinth.jaap.test.model;

import java.util.List;

public class MethodParameters {

	public static <E extends Number> void extendsVariableType(E e) {
		// no-op
	}

	public static void integerList(List<Integer> list) {
		// no-op
	}

	public static void object(Object object) {
		// no-op
	}

	public static void primitive(int primitive) {
		// no-op
	}

	@SuppressWarnings("rawtypes")
	public static void rawtypeList(List list) {
		// no-op
	}

	public static <E> void simpleVariableType(E e) {
		// no-op
	}

	public static void string(String string) {
		// no-op
	}

	public static void unboundedWildcardList(List<?> list) {
		// no-op
	}
}
