package tk.labyrinth.jaap.test.model.declaration;

import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.declaration.TypeParameterDeclaration;

import java.math.BigDecimal;
import java.util.List;

/**
 * @version 1.0.0
 */
public class TestDeclarations {

	public static MethodDeclaration doNothing() {
		return MethodDeclaration.builder()
				.modifiers(List.of(JavaMethodModifier.PUBLIC))
				.name("doNothing")
				.build();
	}

	public static MethodDeclaration greaterThanBigDecimal() {
		return MethodDeclaration.builder()
				.formalParameters(List.of(
						FormalParameterDeclaration.builder()
								.name("other")
								.type(TypeDescription.ofNonParameterized(BigDecimal.class))
								.build()))
				.modifiers(JavaMethodModifier.PUBLIC)
				.name("greaterThan")
				.returnType(TypeDescription.ofNonParameterized(boolean.class))
				.build();
	}

	public static MethodDeclaration greaterThanInteger() {
		return MethodDeclaration.builder()
				.formalParameters(List.of(
						FormalParameterDeclaration.builder()
								.name("other")
								.type(TypeDescription.ofNonParameterized(Integer.class))
								.build()))
				.modifiers(JavaMethodModifier.PUBLIC)
				.name("greaterThan")
				.returnType(TypeDescription.ofNonParameterized(boolean.class))
				.build();
	}

	public static MethodDeclaration mapInObjects() {
		return MethodDeclaration.builder()
				.formalParameters(List.of(
						FormalParameterDeclaration.builder()
								.name("function")
								.type(TypeDescription.of("java.util.function.Function" + "<" +
										"java.util.Objects#map(java.util.function.Function)%T" +
										"," +
										"java.util.Objects#map(java.util.function.Function)%R" +
										">"))
								.build()))
				.modifiers(JavaMethodModifier.PUBLIC, JavaMethodModifier.STATIC)
				.name("greaterThan")
				.returnType(TypeDescription.ofNonParameterized(boolean.class))
				.typeParameters(List.of(
						TypeParameterDeclaration.builder()
								.name("T")
								.build(),
						TypeParameterDeclaration.builder()
								.name("R")
								.build()))
				.build();
	}
}
