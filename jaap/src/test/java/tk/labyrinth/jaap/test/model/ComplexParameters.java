package tk.labyrinth.jaap.test.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ComplexParameters {

	public static <T extends Comparable<T>> void comparable(T comparable) {
		// no-op
	}

	public static <T> void map(Map<T, ? extends List<T>> map) {
		// no-op
	}

	public static <K extends ComplexParameters, V extends List<String> & Serializable> void put(K key, V value) {
		// no-op
	}
}
