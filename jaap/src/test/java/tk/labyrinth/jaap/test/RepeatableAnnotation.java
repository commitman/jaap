package tk.labyrinth.jaap.test;

import java.lang.annotation.Repeatable;

@Repeatable(RepeatableAnnotation.Repeater.class)
public @interface RepeatableAnnotation {

	int value();

	@interface Repeater {

		RepeatableAnnotation[] value();
	}

	interface MyInterface {

		@RepeatableAnnotation(0)
		@RepeatableAnnotation(1)
		Object multiple();

		Object nothing();

		@RepeatableAnnotation.Repeater(@RepeatableAnnotation(2))
		Object repeated();

		@RepeatableAnnotation(3)
		Object single();
	}
}
