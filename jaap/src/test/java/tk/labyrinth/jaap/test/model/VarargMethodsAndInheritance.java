package tk.labyrinth.jaap.test.model;

public class VarargMethodsAndInheritance {

	static {
		Child child = new Child();
		// noinspection UnnecessaryLocalVariable
		Parent parent = child;
		{
			child.array(1, 2);
			// noinspection RedundantArrayCreation
			child.array(new int[]{1, 2});
			parent.array(new int[]{1, 2});
		}
		{
			child.vararg(new int[]{1, 2});
			parent.vararg(1, 2);
			// noinspection RedundantArrayCreation
			parent.vararg(new int[]{1, 2});
		}
	}

	public static class Child extends Parent {

		@Override
		public void array(int... args) {
			// no-op
		}

		@Override
		@SuppressWarnings("ProblematicVarargsMethodOverride")
		public void vararg(int[] args) {
			// no-op
		}
	}

	public static class Parent {

		@SuppressWarnings("unused")
		public void array(int[] args) {
			// no-op
		}

		public void vararg(int... args) {
			// no-op
		}
	}
}
