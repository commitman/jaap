package tk.labyrinth.jaap.handle.type.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;

@ExtendWithJaap
class ParameterizedTypeHandleImplTest {

	@Test
	void testToRawType(ProcessingContext processingContext) {
		ParameterizedTypeHandle parameterizedTypeHandle = processingContext.getParameterizedTypeHandle(
				GenericContext.empty(),
				List.class);
		//
		Assertions.assertEquals("java.util.List<E>", parameterizedTypeHandle.toString());
		Assertions.assertEquals("java.util.List", parameterizedTypeHandle.toRawType().toString());
	}
}