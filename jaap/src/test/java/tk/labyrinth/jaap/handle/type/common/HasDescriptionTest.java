package tk.labyrinth.jaap.handle.type.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

@ExtendWithJaap
class HasDescriptionTest {

	@Test
	void testGetDescription(ProcessingContext processingContext) {
		// FIXME: Replace TypeTemplate with TypeHandle and signature with description.
//		{
//			// Array
//			//
//			Assertions.assertEquals(
//					"java.lang.Object[]",
//					processingContext.getTypeTemplate(Object[].class).getSignature().toString());
//			Assertions.assertEquals(
//					"int[][]",
//					processingContext.getTypeTemplate(int[][].class).getSignature().toString());
//		}
//		{
//			// Declared
//			//
//			Assertions.assertEquals(
//					"java.util.List[]",
//					processingContext.getTypeTemplate(List[].class).getSignature().toString());
//			Assertions.assertEquals(
//					"java.lang.String[]",
//					processingContext.getTypeTemplate(String[].class).getSignature().toString());
//		}
//		{
//			// Primitive
//			//
//			Assertions.assertEquals(
//					"boolean",
//					processingContext.getTypeTemplate(boolean.class).getSignature().toString());
//			Assertions.assertEquals(
//					"byte",
//					processingContext.getTypeTemplate(byte.class).getSignature().toString());
//			Assertions.assertEquals(
//					"char",
//					processingContext.getTypeTemplate(char.class).getSignature().toString());
//			Assertions.assertEquals(
//					"double",
//					processingContext.getTypeTemplate(double.class).getSignature().toString());
//			Assertions.assertEquals(
//					"float",
//					processingContext.getTypeTemplate(float.class).getSignature().toString());
//			Assertions.assertEquals(
//					"int",
//					processingContext.getTypeTemplate(int.class).getSignature().toString());
//			Assertions.assertEquals(
//					"long",
//					processingContext.getTypeTemplate(long.class).getSignature().toString());
//			Assertions.assertEquals(
//					"short",
//					processingContext.getTypeTemplate(short.class).getSignature().toString());
//		}
		{
			// Variable
			//
			// TODO
		}
		{
			// Void
			//
			Assertions.assertEquals(
					TypeDescription.of("void"),
					processingContext.getTypeHandle(void.class).getDescription());
		}
	}
}