package tk.labyrinth.jaap.model.entity.selection;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.stream.Stream;

class EntitySelectionContextTest {

	@Test
	void testForField() {
		EntitySelectionContext context = EntitySelectionContext.forVariable();
		//
		Assertions.assertFalse(context.canBePackage());
		Assertions.assertFalse(context.canBeType());
		Assertions.assertTrue(context.canBeVariable());
		Assertions.assertFalse(context.canBeMethod());
		Assertions.assertEquals(EntitySelectionContext.forVariableOrType(), context.getParent());
	}

	@Test
	void testForFieldOrType() {
		EntitySelectionContext context = EntitySelectionContext.forVariableOrType();
		//
		Assertions.assertFalse(context.canBePackage());
		Assertions.assertTrue(context.canBeType());
		Assertions.assertTrue(context.canBeVariable());
		Assertions.assertFalse(context.canBeMethod());
		Assertions.assertEquals(EntitySelectionContext.forVariableOrTypeOrPackage(), context.getParent());
	}

	@Test
	void testForMethod() {
		{
			EntitySelectionContext context = EntitySelectionContext.forMethod();
			//
			Assertions.assertFalse(context.canBePackage());
			Assertions.assertFalse(context.canBeType());
			Assertions.assertFalse(context.canBeVariable());
			Assertions.assertTrue(context.canBeMethod());
			Assertions.assertEquals(EntitySelectionContext.forVariableOrType(), context.getParent());
		}
		{
			ContribAssertions.assertThrows(
					() -> EntitySelectionContext.forMethod(Stream.of((TypeDescription) null)),
					fault -> {
						Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
						Assertions.assertEquals("argumentTypes must not contain nulls: [null]", fault.getMessage());
					});
		}
	}

	@Test
	void testForMethodInvocation() {
		{
			EntitySelectionContext context = EntitySelectionContext.forMethodInvocation();
			//
			Assertions.assertFalse(context.canBePackage());
			Assertions.assertFalse(context.canBeType());
			Assertions.assertFalse(context.canBeVariable());
			Assertions.assertFalse(context.canBeMethod());
			Assertions.assertEquals(EntitySelectionContext.forMethod(), context.getParent());
		}
		{
			ContribAssertions.assertThrows(
					() -> EntitySelectionContext.forMethodInvocation(Stream.of((TypeDescription) null)),
					fault -> {
						Assertions.assertEquals(IllegalArgumentException.class, fault.getClass());
						Assertions.assertEquals("argumentTypes must not contain nulls: [null]", fault.getMessage());
					});
		}
	}

	@Test
	void testForMethodInvocationArgument() {
		EntitySelectionContext context = EntitySelectionContext.forMethodInvocationArgument();
		//
		Assertions.assertFalse(context.canBePackage());
		Assertions.assertFalse(context.canBeType());
		Assertions.assertTrue(context.canBeVariable());
		Assertions.assertFalse(context.canBeMethod());
		Assertions.assertEquals(EntitySelectionContext.forVariable(), context.getParent());
	}

	@Test
	void testForMethodInvocationTarget() {
		EntitySelectionContext context = EntitySelectionContext.forMethodInvocationTarget();
		//
		Assertions.assertFalse(context.canBePackage());
		Assertions.assertTrue(context.canBeType());
		Assertions.assertTrue(context.canBeVariable());
		Assertions.assertFalse(context.canBeMethod());
		Assertions.assertEquals(EntitySelectionContext.forVariableOrTypeOrPackage(), context.getParent());
	}

	@Test
	void testForStarOrTypeName() {
		EntitySelectionContext context = EntitySelectionContext.forStarOrTypeName();
		//
		Assertions.assertFalse(context.canBePackage());
		Assertions.assertTrue(context.canBeType());
		Assertions.assertFalse(context.canBeVariable());
		Assertions.assertFalse(context.canBeMethod());
		Assertions.assertEquals(EntitySelectionContext.forTypeNameOrPackage(), context.getParent());
	}

	@Test
	void testForTypeName() {
		EntitySelectionContext context = EntitySelectionContext.forTypeName();
		//
		Assertions.assertFalse(context.canBePackage());
		Assertions.assertTrue(context.canBeType());
		Assertions.assertFalse(context.canBeVariable());
		Assertions.assertFalse(context.canBeMethod());
		Assertions.assertEquals(EntitySelectionContext.forTypeNameOrPackage(), context.getParent());
	}

	@Test
	void testForTypeNameOrPackage() {
		EntitySelectionContext context = EntitySelectionContext.forTypeNameOrPackage();
		//
		Assertions.assertTrue(context.canBePackage());
		Assertions.assertTrue(context.canBeType());
		Assertions.assertFalse(context.canBeVariable());
		Assertions.assertFalse(context.canBeMethod());
		Assertions.assertEquals(EntitySelectionContext.forTypeNameOrPackage(), context.getParent());
	}

	@Test
	void testForVariableInitializer() {
		EntitySelectionContext context = EntitySelectionContext.forVariableInitializer();
		//
		Assertions.assertFalse(context.canBePackage());
		Assertions.assertFalse(context.canBeType());
		Assertions.assertTrue(context.canBeVariable());
		Assertions.assertFalse(context.canBeMethod());
		Assertions.assertEquals(EntitySelectionContext.forVariable(), context.getParent());
	}

	@Test
	void testForVariableType() {
		EntitySelectionContext context = EntitySelectionContext.forVariableType();
		//
		Assertions.assertFalse(context.canBePackage());
		Assertions.assertTrue(context.canBeType());
		Assertions.assertFalse(context.canBeVariable());
		Assertions.assertFalse(context.canBeMethod());
		Assertions.assertEquals(EntitySelectionContext.forTypeName(), context.getParent());
	}
}
