package tk.labyrinth.jaap.model.entity.selection;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

class EntitySelectorChainTest {

	@Test
	void testHead() {
		Assertions.assertEquals(EntitySelector.build(EntitySelectionContext.forVariable(), "a"),
				EntitySelectorChain.forVariable(Stream.of("a")).head());
		//
		Assertions.assertEquals(EntitySelector.build(EntitySelectionContext.forVariableOrType(), "a"),
				EntitySelectorChain.forVariable(Stream.of("a", "b")).head());
		//
		Assertions.assertEquals(EntitySelector.build(EntitySelectionContext.forVariableOrTypeOrPackage(), "a"),
				EntitySelectorChain.forVariable(Stream.of("a", "b", "c")).head());
		//
		Assertions.assertEquals(EntitySelector.build(EntitySelectionContext.forVariableOrTypeOrPackage(), "a"),
				EntitySelectorChain.forVariable(Stream.of("a", "b", "c", "d")).head());
	}

	@Test
	void testPrepend() {
		EntitySelectorChain selectorChain = EntitySelectorChain.forVariable(Stream.of("a", "b", "c"));
		//
		Assertions.assertEquals(EntitySelectorChain.forVariable(Stream.of("z", "a", "b", "c")),
				selectorChain.prepend("z"));
	}
}
