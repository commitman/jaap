package tk.labyrinth.jaap.model.signature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

class CanonicalTypeSignatureTest {

	@Test
	void testComplexCaseBreakdown() {
		CanonicalTypeSignature typeSignature = CanonicalTypeSignature.ofValid("d$ll$r:N$tN$st$dT$p$.N$st$dT$p$[][]");
		Assertions.assertEquals(List.of("d$ll$r"), typeSignature.getPackages());
		Assertions.assertEquals(List.of("N$tN$st$dT$p$", "N$st$dT$p$"), typeSignature.getTypes());
		Assertions.assertEquals(2, typeSignature.getArrayCount());
	}

	@Test
	void testNamesWithDollarsCases() {
		{
			CanonicalTypeSignature typeSignature = CanonicalTypeSignature.ofValid("d$ll$r:N$tN$st$dT$p$");
			Assertions.assertEquals(List.of("d$ll$r"), typeSignature.getPackages());
			Assertions.assertEquals(List.of("N$tN$st$dT$p$"), typeSignature.getTypes());
		}
		{
			CanonicalTypeSignature typeSignature = CanonicalTypeSignature.ofValid("$");
			Assertions.assertEquals(List.of(), typeSignature.getPackages());
			Assertions.assertEquals(List.of("$"), typeSignature.getTypes());
		}
	}

	@Test
	void testOfWithClass() {
		{
			Assertions.assertEquals("java.util:Map", CanonicalTypeSignature.of(Map.class).toString());
			Assertions.assertEquals("java.util:Map.Entry", CanonicalTypeSignature.of(Map.Entry.class).toString());
		}
		{
			Assertions.assertEquals("java.lang:Object[]", CanonicalTypeSignature.of(Object[].class).toString());
			Assertions.assertEquals("int[][]", CanonicalTypeSignature.of(int[][].class).toString());
		}
	}

	@Test
	void testToString() {
		{
			// Name only
			Assertions.assertEquals("type", CanonicalTypeSignature.ofValid("type").toString());
			Assertions.assertEquals("Type", CanonicalTypeSignature.ofValid("Type").toString());
			Assertions.assertEquals("TYPE", CanonicalTypeSignature.ofValid("TYPE").toString());
		}
		{
			// Dots and name
			Assertions.assertEquals("j2a.Type", CanonicalTypeSignature.ofValid("j2a.Type").toString());
			Assertions.assertEquals("j2a.l2g.Type", CanonicalTypeSignature.ofValid("j2a.l2g.Type").toString());
		}
	}
}
