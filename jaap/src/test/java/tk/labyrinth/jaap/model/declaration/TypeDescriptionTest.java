package tk.labyrinth.jaap.model.declaration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class TypeDescriptionTest {

	@Test
	void testGetErasureString() {
		Assertions.assertEquals(
				"int[]",
				TypeDescription.of("int[]").getErasureString());
		Assertions.assertEquals(
				"Map",
				TypeDescription.of("Map<? extends Object,Set<?>>").getErasureString());
	}

	@Test
	void testGetSimpleString() {
		Assertions.assertEquals(
				"T",
				TypeDescription.of("java.util.stream:Stream%T").getSimpleString());
		Assertions.assertEquals(
				"List<T>",
				TypeDescription.of("List<java.util.stream:Stream%T>").getSimpleString());
		Assertions.assertEquals(
				"? extends T",
				TypeDescription.of("? extends java.util.stream:Stream%T").getSimpleString());
	}

	@Test
	void testOf() {
		{
			TypeDescription typeDescription = TypeDescription.of("T");
			//
			Assertions.assertNull(typeDescription.getArrayDepth());
			Assertions.assertEquals("T", typeDescription.getFullName());
			Assertions.assertNull(typeDescription.getLowerBound());
			Assertions.assertNull(typeDescription.getParameters());
			Assertions.assertNull(typeDescription.getUpperBound());
		}
		{
			TypeDescription typeDescription = TypeDescription.of("T extends U & V");
			//
			Assertions.assertNull(typeDescription.getArrayDepth());
			Assertions.assertEquals("T", typeDescription.getFullName());
			Assertions.assertNull(typeDescription.getLowerBound());
			Assertions.assertNull(typeDescription.getParameters());
			Assertions.assertEquals(
					List.of(
							TypeDescription.of("U"),
							TypeDescription.of("V")),
					typeDescription.getUpperBound());
		}
		{
			TypeDescription typeDescription = TypeDescription.of("Map<? extends Object,Set<?>>");
			//
			Assertions.assertNull(typeDescription.getArrayDepth());
			Assertions.assertEquals("Map", typeDescription.getFullName());
			Assertions.assertNull(typeDescription.getLowerBound());
			Assertions.assertEquals(
					List.of(
							TypeDescription.of("? extends Object"),
							TypeDescription.of("Set<?>")),
					typeDescription.getParameters());
			Assertions.assertNull(typeDescription.getUpperBound());
		}
	}
}