package tk.labyrinth.jaap.model.element.template.mixin;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.math.BigDecimal;
import java.util.stream.Stream;

@ExtendWithJaap
class PackageElementTemplateMixinTest {

	@Test
	void testSelectMember(ProcessingContext context) {
		{
			// import java.math.BigDecimal;
			PackageElementTemplate packageElement = context.getPackageElementTemplate("java", true);
			Assertions.assertEquals(context.getTypeElementTemplate(BigDecimal.class),
					packageElement.selectMember(EntitySelectorChain.forStarOrTypeInImport(
							Stream.of("math", "BigDecimal"))));
		}
	}
}
