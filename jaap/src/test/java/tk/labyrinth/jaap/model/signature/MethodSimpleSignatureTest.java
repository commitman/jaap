package tk.labyrinth.jaap.model.signature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class MethodSimpleSignatureTest {

	@Test
	void testOfWithString() {
		{
			MethodSimpleSignature methodSimpleSignature = MethodSimpleSignature.of("length(java.lang.String)");
			Assertions.assertEquals("length", methodSimpleSignature.getName());
			Assertions.assertEquals(1, methodSimpleSignature.getParameters().size());
			Assertions.assertEquals(List.of("java.lang.String"), methodSimpleSignature.getParameters());
		}
		{
			MethodSimpleSignature methodSimpleSignature = MethodSimpleSignature.of("length(java.lang.String,java.lang.Integer)");
			Assertions.assertEquals("length", methodSimpleSignature.getName());
			Assertions.assertEquals(2, methodSimpleSignature.getParameters().size());
			Assertions.assertEquals(List.of("java.lang.String", "java.lang.Integer"), methodSimpleSignature.getParameters());
		}
		{
			MethodSimpleSignature methodSimpleSignature = MethodSimpleSignature.of("length()");
			Assertions.assertEquals("length", methodSimpleSignature.getName());
			Assertions.assertEquals(0, methodSimpleSignature.getParameters().size());
			Assertions.assertEquals(List.of(), methodSimpleSignature.getParameters());
		}
		{
			Assertions.assertThrows(
					IllegalArgumentException.class,
					() -> MethodSimpleSignature.of("length(                 )"),
					"False MethodSimpleSignature: length(                 )");
		}
		{
			Assertions.assertThrows(
					IllegalArgumentException.class,
					() -> MethodSimpleSignature.of("length(java.lang.String,       java.lang.Integer            )"),
					"False MethodSimpleSignature: length(java.lang.String,       java.lang.Integer            )");
		}
	}
}