package tk.labyrinth.jaap.langmodel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.template.element.util.TypeParameterElementUtils;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.processing.ProcessingEnvironment;

@ExtendWithJaap
class AnnprocElementFactoryTest {

	@Test
	void testGetTypeParameterWithString(ProcessingEnvironment processingEnvironment) {
		ElementFactory elementFactory = new AnnprocElementFactory(processingEnvironment);
		//
		Assertions.assertEquals(
				"java.util:Optional%T",
				TypeParameterElementUtils.getSignature(
						processingEnvironment,
						elementFactory.getTypeParameter("java.util:Optional%T")));
		Assertions.assertEquals(
				"java.util:Map%K",
				TypeParameterElementUtils.getSignature(
						processingEnvironment,
						elementFactory.getTypeParameter("java.util:Map%K")));
		{
			// TODO: Support qualified, not only canonical signatures.
//			Assertions.assertEquals(
//					"java.util:Map.Entry%V",
//					TypeParameterElementUtils.getSignature(
//							processingEnvironment,
//							elementFactory.getTypeParameter("java.util:HashMap.Entry%V")));
		}
	}

	@Test
	void testGetTypeWithString(ProcessingEnvironment processingEnvironment) {
		ElementFactory elementFactory = new AnnprocElementFactory(processingEnvironment);
		//
		Assertions.assertEquals(
				"java.util:Map.Entry",
				TypeElementUtils.getSignature(elementFactory.getType("java.util:Map.Entry")));
		{
			// TODO: Currently not supported.
//		Assertions.assertEquals(
//				"java.util:Map.Entry",
//				TypeElementUtils.getSignature(elementFactory.getType("java.util:HashMap.Entry")));
		}
	}
}