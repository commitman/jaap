package tk.labyrinth.jaap.langmodel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.ProcessingEnvironment;

@ExtendWithJaap
class AnnprocTypeMirrorFactoryTest {

	@Test
	void testFindVariable(ProcessingEnvironment environment) {
		AnnprocTypeMirrorFactory typeMirrorFactory = new AnnprocTypeMirrorFactory(
				new AnnprocElementFactory(environment),
				environment);
		//
		Assertions.assertEquals(
				"K",
				typeMirrorFactory.getVariable(TypeDescription.builder()
						.fullName("java.util.Map%K")
						.build()).toString());
		Assertions.assertNull(
				typeMirrorFactory.findVariable(
						TypeDescription.builder()
								.fullName("java.util.Map%T")
								.build()));
	}
}