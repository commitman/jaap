package tk.labyrinth.javax.lang.model.element;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.testing.junit5.JaapExtension;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

@ExtendWithJaap
public class ExecutableElementTest {

	/**
	 * Notable case where return type of {@link Object#toString()} (which is {@link String})
	 * is not equal to {@link TypeMirror} obtained from String's element.<br>
	 * This means that we can not compare {@link TypeMirror TypeMirrors} with <b>equals</b>
	 * even if they were refined with {@link Types#erasure(TypeMirror)}.<br>
	 * {@link Types#isSameType(TypeMirror, TypeMirror)} is the true way of comparing
	 * arbitrary {@link TypeMirror TypeMirrors}.
	 *
	 * @param processingEnvironment provided by {@link JaapExtension}
	 */
	@Test
	void testGetReturnType(ProcessingEnvironment processingEnvironment) {
		TypeMirror stringTypeMirror = processingEnvironment.getElementUtils()
				.getTypeElement(String.class.getCanonicalName()).asType();
		TypeMirror returnTypeMirror = processingEnvironment.getElementUtils()
				.getTypeElement(Object.class.getCanonicalName()).getEnclosedElements().stream()
				.filter(enclosedElement -> enclosedElement.getSimpleName().contentEquals("toString"))
				.map(ExecutableElement.class::cast)
				.findFirst().orElseThrow().getReturnType();
		//
		Assertions.assertEquals("java.lang.String", returnTypeMirror.toString());
		Assertions.assertEquals(stringTypeMirror.toString(), returnTypeMirror.toString());
		//
		Assertions.assertNotSame(stringTypeMirror, returnTypeMirror);
		Assertions.assertNotEquals(stringTypeMirror, returnTypeMirror);
		//
		Assertions.assertTrue(processingEnvironment.getTypeUtils().isSameType(stringTypeMirror, returnTypeMirror));
	}
}
