package tk.labyrinth.javax.lang.model.type;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.Collection;

@ExtendWithJaap
public class TypeMirrorTest {

	@Test
	void testEquals(ProcessingEnvironment processingEnvironment) {
		{
			// Plain
			TypeElement string = TypeElementUtils.get(processingEnvironment, String.class);
			//
			TypeMirror asType = string.asType();
			TypeMirror erasure = processingEnvironment.getTypeUtils().erasure(asType);
			TypeMirror declaredType = processingEnvironment.getTypeUtils().getDeclaredType(string);
			//
			Assertions.assertEquals("java.lang.String", asType.toString());
			Assertions.assertEquals("java.lang.String", erasure.toString());
			Assertions.assertEquals("java.lang.String", declaredType.toString());
			//
			// ( asType == erasure ) != declaredType
			Assertions.assertEquals(asType, erasure);
			Assertions.assertNotEquals(asType, declaredType);
			Assertions.assertNotEquals(erasure, declaredType);
		}
		{
			// Generic
			TypeElement collection = TypeElementUtils.get(processingEnvironment, Collection.class);
			//
			TypeMirror asType = collection.asType();
			TypeMirror erasure = processingEnvironment.getTypeUtils().erasure(asType);
			TypeMirror declaredType = processingEnvironment.getTypeUtils().getDeclaredType(collection);
			TypeMirror declaredType0 = processingEnvironment.getTypeUtils().getDeclaredType(collection, erasure);
			//
			Assertions.assertEquals("java.util.Collection<E>", asType.toString());
			Assertions.assertEquals("java.util.Collection", erasure.toString());
			Assertions.assertEquals("java.util.Collection", declaredType.toString());
			//
			// asType != ( erasure == declaredType )
			Assertions.assertNotEquals(asType, erasure);
			Assertions.assertNotEquals(asType, declaredType);
			Assertions.assertEquals(erasure, declaredType);
		}
	}
}
