package tk.labyrinth.misc4j.lang.struct;

import javax.annotation.Nullable;
import java.util.stream.Stream;

/**
 * @param <E> E
 * @param <D> D
 *
 * @deprecated Moving to typical-java project.
 */
@Deprecated
public interface TypeDescriptor<E, D extends TypeDescriptor<E, D>> {

	@Override
	boolean equals(Object obj);

	TypeDescriptor<E, D> getDefaultDescriptor();

	Stream<TypeDescriptor<E, D>> getDirectSuperinterfaces();

	E getElement();

	default TypeDescriptor<E, D> getParameter(int index) {
		return getParameters().skip(index).findFirst().orElse(null);
	}

	default int getParameterCount() {
		return (int) getParameters().count();
	}

	Stream<TypeDescriptor<E, D>> getParameters();

	TypeDescriptor<E, D> getRawDescriptor();

	@Nullable
	TypeDescriptor<E, D> getSuperclass();

	default boolean hasParameters() {
		return getParameterCount() > 0;
	}

	boolean isSubtype(TypeDescriptor<E, ?> descriptor);

	boolean isVariable();
}
