package tk.labyrinth.misc4j.lang.model;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.stream.Stream;

public class ParameterUtils {

	/**
	 * FIXME: The implementation does not work properly with rawtypes - it returns default mappings for them.<br>
	 * FIXME: See tests for more information.
	 *
	 * @param environment   non-null
	 * @param actualType    non-null
	 * @param declaringType non-null
	 *
	 * @return non-null
	 */
	public static Stream<TypeMirror> getActualParameters(ProcessingEnvironment environment, TypeMirror actualType, TypeElement declaringType) {
		return tk.labyrinth.misc4j.lang.struct.ParameterUtils.getActualParameters(
				new TypeMirrorTypeDescriptor(environment, actualType),
				new TypeMirrorTypeDescriptor(environment, declaringType.asType())
		);
	}
}
