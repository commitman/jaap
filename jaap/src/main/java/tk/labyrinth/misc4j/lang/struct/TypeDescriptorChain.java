package tk.labyrinth.misc4j.lang.struct;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EqualsAndHashCode
@ToString
public class TypeDescriptorChain<E, D extends TypeDescriptor<E, D>> {

	private final List<TypeDescriptor<E, D>> descriptors;

	private TypeDescriptorChain(Stream<? extends TypeDescriptor<E, D>> descriptors) {
		this.descriptors = descriptors.collect(Collectors.toList());
	}

	public TypeDescriptorChain<E, D> append(TypeDescriptor<E, D> descriptor) {
		Objects.requireNonNull(descriptor, "descriptor");
		//
		return new TypeDescriptorChain<>(Stream.concat(descriptors.stream(), Stream.of(descriptor)));
	}

	public Stream<TypeDescriptor<E, D>> getDescriptors() {
		return descriptors.stream();
	}

	public TypeDescriptor<E, D> getLast() {
		return descriptors.get(descriptors.size() - 1);
	}

	public static <E, D extends TypeDescriptor<E, D>> TypeDescriptorChain<E, D> of(TypeDescriptor<E, D> descriptor) {
		Objects.requireNonNull(descriptor, "typeDescriptor");
		//
		return new TypeDescriptorChain<>(Stream.of(descriptor));
	}
}
