package tk.labyrinth.jaap.context;

import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.ReferenceTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.handle.type.impl.AnnprocTypeHandleFactory;
import tk.labyrinth.jaap.langmodel.AnnprocElementFactory;
import tk.labyrinth.jaap.langmodel.AnnprocTypeMirrorFactory;
import tk.labyrinth.jaap.langmodel.ElementFactory;
import tk.labyrinth.jaap.langmodel.TypeMirrorFactory;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.template.element.AnnprocElementTemplateFactoryImpl;
import tk.labyrinth.jaap.template.element.ConstructorElementTemplate;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.FieldElementTemplate;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.AnnotatedConstruct;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.annotation.Annotation;

@SuppressWarnings({"unused", "UnusedReturnValue"})
public interface ProcessingContext {

	@Nullable
	DeclaredTypeHandle findDeclaredTypeHandle(String typeSignatureString);

	@Nullable
	MethodElementTemplate findMethodElementTemplate(MethodFullSignature methodFullSignature);

	@Nullable
	PackageElementTemplate findPackageElementTemplate(String packageSignatureString);

	@Nullable
	TypeElementTemplate findTypeElementTemplate(CanonicalTypeSignature canonicalTypeSignature);

	@Nullable
	TypeHandle findTypeHandle(String typeDescriptionString);

	@Nullable
	TypeHandle findTypeHandle(TypeDescription typeDescription);

	@Nullable
	TypeParameterElementTemplate findTypeParameterElementTemplate(Class<?> type, String typeParameterSimpleName);

	@Nullable
	TypeParameterElementTemplate findTypeParameterElementTemplate(String typeParameterSignature);

	AnnotationHandle getAnnotationHandle(AnnotatedConstruct parent, AnnotationMirror annotationMirror);

	AnnotationTypeHandle getAnnotationTypeHandle(Class<? extends Annotation> annotationType);

	AnnotationTypeHandle getAnnotationTypeHandle(DeclaredType annotationDeclaredType);

	AnnotationTypeHandle getAnnotationTypeHandle(String annotationTypeName);

	AnnotationTypeHandle getAnnotationTypeHandle(TypeMirror annotationTypeMirror);

	ArrayTypeHandle getArrayTypeHandle(GenericContext genericContext, TypeMirror typeMirror);

	ConstructorElementTemplate getConstructorElementTemplate(Element element);

	ConstructorElementTemplate getConstructorElementTemplate(ExecutableElement executableElement);

	DeclaredTypeHandle getDeclaredTypeHandle(GenericContext genericContext, TypeElement typeElement);

	DeclaredTypeHandle getDeclaredTypeHandle(GenericContext genericContext, TypeMirror typeMirror);

	ElementTemplate getElementTemplate(Class<?> type);

	ElementTemplate getElementTemplate(Element element);

	ElementTemplate getElementTemplate(String fullElementSignature);

	FieldElementTemplate getFieldElementTemplate(Class<?> type, String fieldSimpleName);

	FieldElementTemplate getFieldElementTemplate(Element element);

	FieldElementTemplate getFieldElementTemplate(String fieldFullName);

	FieldElementTemplate getFieldElementTemplate(VariableElement variableElement);

	FormalParameterElementTemplate getFormalParameterElementTemplate(VariableElement variableElement);

	GenericTypeHandle getGenericTypeHandle(GenericContext genericContext, DeclaredType declaredType);

	MethodElementTemplate getMethodElementTemplate(Class<?> type, String simpleMethodSignature);

	MethodElementTemplate getMethodElementTemplate(Element element);

	MethodElementTemplate getMethodElementTemplate(ExecutableElement executableElement);

	default MethodElementTemplate getMethodElementTemplate(MethodFullSignature methodFullSignature) {
		MethodElementTemplate result = findMethodElementTemplate(methodFullSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: methodFullSignature = " + methodFullSignature);
		}
		return result;
	}

	MethodElementTemplate getMethodElementTemplate(String methodFullSignature);

	MethodElementTemplate getMethodElementTemplateByName(Class<?> type, String name);

	MethodElementTemplate getMethodElementTemplateByName(String methodFullName);

	/**
	 * Returns {@link Name} built with underlying {@link ProcessingEnvironment}.
	 *
	 * @param name non-null
	 *
	 * @return non-null
	 */
	Name getName(String name);

	PackageElementTemplate getPackageElementTemplate(Element element);

	PackageElementTemplate getPackageElementTemplate(PackageElement packageElement);

	PackageElementTemplate getPackageElementTemplate(String packageSignatureString);

	PackageElementTemplate getPackageElementTemplate(String packageSignatureString, boolean synthesizeIfAbsent);

	PackageElementTemplate getPackageElementTemplateOf(Class<?> childType);

	ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, Class<?> type);

	ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, DeclaredType declaredType);

	ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, TypeMirror typeMirror);

	PrimitiveTypeHandle getPrimitiveTypeHandle(PrimitiveType primitiveType);

	PrimitiveTypeHandle getPrimitiveTypeHandle(TypeMirror typeMirror);

	ProcessingEnvironment getProcessingEnvironment();

	RawTypeHandle getRawTypeHandle(DeclaredType declaredType);

	ReferenceTypeHandle getReferenceTypeHandle(TypeMirror typeMirror);

	TypeElementTemplate getTypeElementTemplate(CanonicalTypeSignature canonicalTypeSignature);

	TypeElementTemplate getTypeElementTemplate(Class<?> type);

	TypeElementTemplate getTypeElementTemplate(Element element);

	TypeElementTemplate getTypeElementTemplate(String typeFullName);

	TypeElementTemplate getTypeElementTemplate(TypeElement typeElement);

	TypeElementTemplate getTypeElementTemplate(TypeMirror typeMirror);

	TypeHandle getTypeHandle(Class<?> type);

	TypeHandle getTypeHandle(GenericContext genericContext, Class<?> type);

	TypeHandle getTypeHandle(GenericContext genericContext, TypeElement typeElement);

	TypeHandle getTypeHandle(GenericContext genericContext, TypeMirror typeMirror);

	default TypeHandle getTypeHandle(String typeDescriptionString) {
		TypeHandle result = findTypeHandle(typeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeDescriptionString = " + typeDescriptionString);
		}
		return result;
	}

	default TypeHandle getTypeHandle(TypeDescription typeDescription) {
		TypeHandle result = findTypeHandle(typeDescription);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeDescription = " + typeDescription);
		}
		return result;
	}

	TypeParameterElementTemplate getTypeParameterElementTemplate(Class<?> type, String typeParameterSimpleName);

	TypeParameterElementTemplate getTypeParameterElementTemplate(String typeParameterSignature);

	TypeParameterElementTemplate getTypeParameterElementTemplate(TypeParameterElement typeParameterElement);

	VariableTypeHandle getVariableTypeHandle(GenericContext genericContext, TypeMirror typeMirror);

	VariableTypeHandle getVariableTypeHandle(GenericContext genericContext, TypeVariable typeVariable);

	static ProcessingContext of(AnnotationProcessingRound round) {
		return of(round.getProcessingEnvironment());
	}

	static ProcessingContext of(ProcessingEnvironment environment) {
		ElementFactory elementFactory = new AnnprocElementFactory(environment);
		TypeMirrorFactory typeMirrorFactory = new AnnprocTypeMirrorFactory(elementFactory, environment);
		//
		AnnprocElementTemplateFactoryImpl elementTemplateFactory = new AnnprocElementTemplateFactoryImpl(
				elementFactory,
				typeMirrorFactory);
		AnnprocTypeHandleFactory typeHandleFactory = new AnnprocTypeHandleFactory(typeMirrorFactory);
		//
		ProcessingContext processingContext = new ProcessingContextImpl(
				elementTemplateFactory,
				environment,
				typeHandleFactory,
				typeMirrorFactory);
		//
		elementTemplateFactory.acceptProcessingContext(processingContext);
		typeHandleFactory.acceptProcessingContext(processingContext);
		//
		return processingContext;
	}
}
