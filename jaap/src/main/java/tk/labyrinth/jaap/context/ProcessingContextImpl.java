package tk.labyrinth.jaap.context;

import lombok.Getter;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.impl.DefaultAnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.impl.DefaultLangmodelAnnotationHandle;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.ReferenceTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandleFactory;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.langmodel.TypeMirrorFactory;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.template.synthetic.SyntheticPackageElementTemplate;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.template.element.ConstructorElementTemplate;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.ElementTemplateFactory;
import tk.labyrinth.jaap.template.element.FieldElementTemplate;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;
import tk.labyrinth.jaap.util.TypeMirrorUtils;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.AnnotatedConstruct;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.lang.annotation.Annotation;
import java.util.Objects;

public class ProcessingContextImpl implements ProcessingContext {

	private final ElementTemplateFactory elementTemplateFactory;

	@Getter
	private final ProcessingEnvironment processingEnvironment;

	private final TypeHandleFactory typeHandleFactory;

	private final TypeMirrorFactory typeMirrorFactory;

	public ProcessingContextImpl(
			ElementTemplateFactory elementTemplateFactory,
			ProcessingEnvironment processingEnvironment,
			TypeHandleFactory typeHandleFactory,
			TypeMirrorFactory typeMirrorFactory) {
		this.elementTemplateFactory = elementTemplateFactory;
		this.processingEnvironment = processingEnvironment;
		this.typeHandleFactory = typeHandleFactory;
		this.typeMirrorFactory = typeMirrorFactory;
	}

	@Nullable
	@Override
	public DeclaredTypeHandle findDeclaredTypeHandle(String typeSignatureString) {
		return typeHandleFactory.findDeclared(typeSignatureString);
	}

	@Nullable
	@Override
	public MethodElementTemplate findMethodElementTemplate(MethodFullSignature methodFullSignature) {
		return elementTemplateFactory.findMethod(methodFullSignature);
	}

	@Nullable
	@Override
	public PackageElementTemplate findPackageElementTemplate(String packageSignatureString) {
		return elementTemplateFactory.findPackage(packageSignatureString);
	}

	@Nullable
	@Override
	public TypeElementTemplate findTypeElementTemplate(CanonicalTypeSignature canonicalTypeSignature) {
		return elementTemplateFactory.findType(canonicalTypeSignature);
	}

	@Nullable
	@Override
	public TypeHandle findTypeHandle(String typeDescriptionString) {
		return typeHandleFactory.find(typeDescriptionString);
	}

	@Nullable
	@Override
	public TypeHandle findTypeHandle(TypeDescription typeDescription) {
		return typeHandleFactory.find(typeDescription);
	}

	@Nullable
	@Override
	public TypeParameterElementTemplate findTypeParameterElementTemplate(Class<?> type, String typeParameterSimpleName) {
		return elementTemplateFactory.findTypeParameter(type, typeParameterSimpleName);
	}

	@Nullable
	@Override
	public TypeParameterElementTemplate findTypeParameterElementTemplate(String typeParameterSignature) {
		return elementTemplateFactory.findTypeParameter(typeParameterSignature);
	}

	@Override
	public AnnotationHandle getAnnotationHandle(AnnotatedConstruct parent, AnnotationMirror annotationMirror) {
		Objects.requireNonNull(parent, "parent");
		Objects.requireNonNull(annotationMirror, "annotationMirror");
		//
		// FIXME: Move to AnnotationHandleFactory.
		return new DefaultLangmodelAnnotationHandle(annotationMirror, parent, this);
	}

	@Override
	public AnnotationTypeHandle getAnnotationTypeHandle(Class<? extends Annotation> annotationType) {
		Objects.requireNonNull(annotationType, "annotationType");
		//
		// FIXME: Move to AnnotationHandleFactory.
		return new DefaultAnnotationTypeHandle(getTypeElementTemplate(annotationType));
	}

	@Override
	public AnnotationTypeHandle getAnnotationTypeHandle(DeclaredType annotationDeclaredType) {
		Objects.requireNonNull(annotationDeclaredType, "annotationDeclaredType");
		//
		// FIXME: Move to AnnotationHandleFactory.
		return new DefaultAnnotationTypeHandle(getTypeElementTemplate(annotationDeclaredType));
	}

	@Override
	public AnnotationTypeHandle getAnnotationTypeHandle(String annotationTypeName) {
		Objects.requireNonNull(annotationTypeName, "annotationTypeName");
		//
		// FIXME: Move to AnnotationHandleFactory.
		return new DefaultAnnotationTypeHandle(getTypeElementTemplate(annotationTypeName));
	}

	@Override
	public AnnotationTypeHandle getAnnotationTypeHandle(TypeMirror annotationTypeMirror) {
		return getAnnotationTypeHandle(TypeMirrorUtils.requireDeclaredType(annotationTypeMirror));
	}

	@Override
	public ArrayTypeHandle getArrayTypeHandle(GenericContext genericContext, TypeMirror typeMirror) {
		return typeHandleFactory.getArray(genericContext, typeMirror);
	}

	@Override
	public ConstructorElementTemplate getConstructorElementTemplate(Element element) {
		return elementTemplateFactory.getConstructor(element);
	}

	@Override
	public ConstructorElementTemplate getConstructorElementTemplate(ExecutableElement executableElement) {
		return elementTemplateFactory.getConstructor(executableElement);
	}

	@Override
	public DeclaredTypeHandle getDeclaredTypeHandle(GenericContext genericContext, TypeElement typeElement) {
		return typeHandleFactory.getDeclared(genericContext, typeElement);
	}

	@Override
	public DeclaredTypeHandle getDeclaredTypeHandle(GenericContext genericContext, TypeMirror typeMirror) {
		return typeHandleFactory.getDeclared(genericContext, typeMirror);
	}

	@Override
	public ElementTemplate getElementTemplate(Class<?> type) {
		return elementTemplateFactory.get(type);
	}

	@Override
	public ElementTemplate getElementTemplate(Element element) {
		return elementTemplateFactory.getElement(element);
	}

	@Override
	public ElementTemplate getElementTemplate(String fullElementSignature) {
		return elementTemplateFactory.getElement(fullElementSignature);
	}

	@Override
	public FieldElementTemplate getFieldElementTemplate(Class<?> type, String fieldSimpleName) {
		return elementTemplateFactory.getField(type, fieldSimpleName);
	}

	@Override
	public FieldElementTemplate getFieldElementTemplate(Element element) {
		return elementTemplateFactory.getField(element);
	}

	@Override
	public FieldElementTemplate getFieldElementTemplate(String fieldFullName) {
		return elementTemplateFactory.getField(fieldFullName);
	}

	@Override
	public FieldElementTemplate getFieldElementTemplate(VariableElement variableElement) {
		return elementTemplateFactory.getField(variableElement);
	}

	@Override
	public FormalParameterElementTemplate getFormalParameterElementTemplate(VariableElement variableElement) {
		return elementTemplateFactory.getFormalParameter(variableElement);
	}

	@Override
	public GenericTypeHandle getGenericTypeHandle(GenericContext genericContext, DeclaredType declaredType) {
		return typeHandleFactory.getGeneric(genericContext, declaredType);
	}

	@Override
	public MethodElementTemplate getMethodElementTemplate(Class<?> type, String simpleMethodSignature) {
		return elementTemplateFactory.getMethod(type, simpleMethodSignature);
	}

	@Override
	public MethodElementTemplate getMethodElementTemplate(Element element) {
		return elementTemplateFactory.getMethod(element);
	}

	@Override
	public MethodElementTemplate getMethodElementTemplate(ExecutableElement executableElement) {
		return elementTemplateFactory.getMethod(executableElement);
	}

	@Override
	public MethodElementTemplate getMethodElementTemplate(String methodFullSignature) {
		return elementTemplateFactory.getMethod(methodFullSignature);
	}

	@Override
	public MethodElementTemplate getMethodElementTemplateByName(Class<?> type, String name) {
		TypeElementTemplate typeElementTemplate = getTypeElementTemplate(type);
		return typeElementTemplate.getDeclaredMethod(name);
	}

	@Override
	public MethodElementTemplate getMethodElementTemplateByName(String methodFullName) {
		// FIXME: Move to factory, rework to be smarter.
		String[] split = methodFullName.split("#");
		TypeElementTemplate typeElementTemplate = getTypeElementTemplate(split[0]);
		return typeElementTemplate.getDeclaredMethod(split[1]);
	}

	@Override
	public Name getName(String name) {
		return processingEnvironment.getElementUtils().getName(name);
	}

	@Override
	public PackageElementTemplate getPackageElementTemplate(Element element) {
		return elementTemplateFactory.getPackage(element);
	}

	@Override
	public PackageElementTemplate getPackageElementTemplate(PackageElement packageElement) {
		return elementTemplateFactory.getPackage(packageElement);
	}

	@Override
	public PackageElementTemplate getPackageElementTemplate(String packageSignatureString) {
		return getPackageElementTemplate(packageSignatureString, false);
	}

	@Override
	public PackageElementTemplate getPackageElementTemplate(String packageSignatureString, boolean synthesizeIfAbsent) {
		PackageElementTemplate result;
		{
			PackageElementTemplate found = findPackageElementTemplate(packageSignatureString);
			if (found != null) {
				result = found;
			} else {
				if (synthesizeIfAbsent) {
					// TODO: This one probably should belong to separate implementation, supporting synthetics.
					result = new SyntheticPackageElementTemplate(this, packageSignatureString);
				} else {
					throw new IllegalArgumentException("Not found: packageSignatureString = " + packageSignatureString);
				}
			}
		}
		return result;
	}

	@Override
	public PackageElementTemplate getPackageElementTemplateOf(Class<?> childType) {
		return elementTemplateFactory.getPackageOf(childType);
	}

	@Override
	public ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, Class<?> type) {
		return elementTemplateFactory.getParameterizedTypeHandle(genericContext, type);
	}

	@Override
	public ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, DeclaredType declaredType) {
		return elementTemplateFactory.getParameterizedTypeHandle(genericContext, declaredType);
	}

	@Override
	public ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, TypeMirror typeMirror) {
		return elementTemplateFactory.getParameterizedTypeHandle(genericContext, typeMirror);
	}

	@Override
	public PrimitiveTypeHandle getPrimitiveTypeHandle(PrimitiveType primitiveType) {
		return typeHandleFactory.getPrimitive(primitiveType);
	}

	@Override
	public PrimitiveTypeHandle getPrimitiveTypeHandle(TypeMirror typeMirror) {
		return typeHandleFactory.getPrimitive(typeMirror);
	}

	@Override
	public RawTypeHandle getRawTypeHandle(DeclaredType declaredType) {
		return typeHandleFactory.getRaw(declaredType);
	}

	@Override
	public ReferenceTypeHandle getReferenceTypeHandle(TypeMirror typeMirror) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeElementTemplate getTypeElementTemplate(CanonicalTypeSignature canonicalTypeSignature) {
		return elementTemplateFactory.getType(canonicalTypeSignature);
	}

	@Override
	public TypeElementTemplate getTypeElementTemplate(Class<?> type) {
		return elementTemplateFactory.getType(type);
	}

	@Override
	public TypeElementTemplate getTypeElementTemplate(Element element) {
		return elementTemplateFactory.getType(element);
	}

	@Override
	public TypeElementTemplate getTypeElementTemplate(String typeFullName) {
		return elementTemplateFactory.getType(typeFullName);
	}

	@Override
	public TypeElementTemplate getTypeElementTemplate(TypeElement typeElement) {
		return elementTemplateFactory.getType(typeElement);
	}

	@Override
	public TypeElementTemplate getTypeElementTemplate(TypeMirror typeMirror) {
		return elementTemplateFactory.getType(typeMirror);
	}

	@Override
	public TypeHandle getTypeHandle(Class<?> type) {
		return getTypeHandle(GenericContext.empty(), type);
	}

	@Override
	public TypeHandle getTypeHandle(GenericContext genericContext, Class<?> type) {
		return typeHandleFactory.get(type);
	}

	@Override
	public TypeHandle getTypeHandle(GenericContext genericContext, TypeElement typeElement) {
		return typeHandleFactory.get(genericContext, typeElement);
	}

	@Override
	public TypeHandle getTypeHandle(GenericContext genericContext, TypeMirror typeMirror) {
		return typeHandleFactory.get(genericContext, typeMirror);
	}

	@Override
	public TypeHandle getTypeHandle(String typeDescriptionString) {
		return getTypeHandle(TypeDescription.of(typeDescriptionString));
	}

	@Override
	public TypeParameterElementTemplate getTypeParameterElementTemplate(Class<?> type, String typeParameterSimpleName) {
		return elementTemplateFactory.getTypeParameter(type, typeParameterSimpleName);
	}

	@Override
	public TypeParameterElementTemplate getTypeParameterElementTemplate(String typeParameterSignature) {
		return elementTemplateFactory.getTypeParameter(typeParameterSignature);
	}

	@Override
	public TypeParameterElementTemplate getTypeParameterElementTemplate(TypeParameterElement typeParameterElement) {
		return elementTemplateFactory.getTypeParameter(typeParameterElement);
	}

	@Override
	public VariableTypeHandle getVariableTypeHandle(GenericContext genericContext, TypeMirror typeMirror) {
		return typeHandleFactory.getVariable(genericContext, typeMirror);
	}

	@Override
	public VariableTypeHandle getVariableTypeHandle(GenericContext genericContext, TypeVariable typeVariable) {
		return typeHandleFactory.getVariable(genericContext, typeVariable);
	}
}
