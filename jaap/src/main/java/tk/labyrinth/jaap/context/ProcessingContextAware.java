package tk.labyrinth.jaap.context;

public interface ProcessingContextAware {

	void acceptProcessingContext(ProcessingContext processingContext);
}
