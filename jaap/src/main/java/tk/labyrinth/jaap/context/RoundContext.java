package tk.labyrinth.jaap.context;

import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import java.util.stream.Stream;

public interface RoundContext extends HasProcessingContext {

	Stream<DeclaredTypeHandle> getAllDeclaredTypes();

	Stream<TypeElementTemplate> getAllTypeElements();

	Stream<PackageElementTemplate> getPackageElements();

	AnnotationProcessingRound getRound();

	Stream<DeclaredTypeHandle> getTopLevelDeclaredTypes();

	/**
	 * Consists of {@link PackageElementTemplate PackageElementTemplates}
	 * and {@link TypeElementTemplate TypeElementTemplates}.
	 *
	 * @return non-null
	 */
	Stream<ElementTemplate> getTopLevelElements();

	Stream<TypeElementTemplate> getTopLevelTypeElements();

	static RoundContext of(AnnotationProcessingRound round) {
		return of(round, ProcessingContext.of(round.getProcessingEnvironment()));
	}

	static RoundContext of(AnnotationProcessingRound round, ProcessingContext processingContext) {
		return new RoundContextImpl(processingContext, round);
	}
}
