package tk.labyrinth.jaap.trees.util;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodInvocationTreeUtils {

	// TODO: Cover with tests.
	public static String buildQualifiedName(MethodInvocationTree methodInvocationTree) {
		return getSimpleNames(methodInvocationTree).collect(Collectors.joining("."));
	}

	// TODO: Cover with tests.
	public static String getSimpleName(MethodInvocationTree methodInvocationTree) {
		String result;
		{
			ExpressionTree methodSelect = methodInvocationTree.getMethodSelect();
			if (methodSelect instanceof IdentifierTree) {
				result = IdentifierTreeUtils.getSimpleName((IdentifierTree) methodSelect);
			} else if (methodSelect instanceof MemberSelectTree) {
				result = ((MemberSelectTree) methodSelect).getIdentifier().toString();
			} else {
				throw new NotImplementedException(ExceptionUtils.render(methodInvocationTree));
			}
		}
		return result;
	}

	// TODO: Cover with tests.
	public static Stream<String> getSimpleNames(MethodInvocationTree methodInvocationTree) {
		Stream<String> result;
		{
			ExpressionTree methodSelect = methodInvocationTree.getMethodSelect();
			if (methodSelect instanceof IdentifierTree) {
				result = IdentifierTreeUtils.getSimpleNames((IdentifierTree) methodSelect);
			} else if (methodSelect instanceof MemberSelectTree) {
				result = MemberSelectTreeUtils.getSimpleNames((MemberSelectTree) methodSelect);
			} else {
				throw new NotImplementedException(ExceptionUtils.render(methodInvocationTree));
			}
		}
		return result;
	}
}
