package tk.labyrinth.jaap.trees.util;

import com.sun.source.tree.IdentifierTree;

import java.util.stream.Stream;

public class IdentifierTreeUtils {

	// TODO: Cover with tests.
	public static String getSimpleName(IdentifierTree identifierTree) {
		return identifierTree.getName().toString();
	}

	// TODO: Cover with tests.
	public static Stream<String> getSimpleNames(IdentifierTree identifierTree) {
		return Stream.of(getSimpleName(identifierTree));
	}
}
