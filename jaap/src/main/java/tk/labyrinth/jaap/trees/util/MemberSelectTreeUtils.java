package tk.labyrinth.jaap.trees.util;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.misc4j2.java.lang.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Encountered in:<br>
 * - Import Declaration;<br>
 * - MethodInvocation Arguments;<br>
 * - MethodInvocation MethodSelections;<br>
 * - Package Declaration;<br>
 * - Variable Initializers;<br>
 * - Variable Types;<br>
 */
public class MemberSelectTreeUtils {

	// TODO: Cover with tests.
	public static String buildQualifiedName(MemberSelectTree memberSelectTree) {
		return getSimpleNames(memberSelectTree).collect(Collectors.joining("."));
	}

	/**
	 * Splits provided {@link MemberSelectTree} into self-sustained {@link ExpressionTree} and sequence of simple names.
	 *
	 * @param memberSelectTree non-null
	 *
	 * @return non-null with nullable left and non-empty right
	 */
	// TODO: Cover with tests.
	public static Pair<ExpressionTree, List<String>> deconstruct(MemberSelectTree memberSelectTree) {
		ExpressionTree resultExpressionTree;
		List<String> resultSimpleNames = new ArrayList<>();
		{
			ExpressionTree reducedTree = ObjectUtils.<ExpressionTree>reduce(
					memberSelectTree,
					MemberSelectTree.class::isInstance,
					expressionTree -> {
						MemberSelectTree tree = (MemberSelectTree) expressionTree;
						resultSimpleNames.add(0, tree.getIdentifier().toString());
						return tree.getExpression();
					});
			//
			// MemberSelect is depleted at this point.
			if (reducedTree instanceof IdentifierTree) {
				// Identifier.
				resultSimpleNames.add(0, ((IdentifierTree) reducedTree).getName().toString());
				resultExpressionTree = null;
			} else {
				// Any except Identifier or MemberSelect.
				resultExpressionTree = reducedTree;
			}
		}
		return Pair.of(resultExpressionTree, resultSimpleNames);
	}

	// TODO: Cover with tests.
	public static Stream<String> getSimpleNames(MemberSelectTree memberSelectTree) {
		Stream<String> previousSimpleNames;
		{
			ExpressionTree expression = memberSelectTree.getExpression();
			if (expression instanceof IdentifierTree) {
				previousSimpleNames = IdentifierTreeUtils.getSimpleNames((IdentifierTree) expression);
			} else if (expression instanceof MemberSelectTree) {
				previousSimpleNames = getSimpleNames((MemberSelectTree) expression);
			} else if (expression instanceof MethodInvocationTree) {
				previousSimpleNames = Stream.empty();
			} else {
				throw new NotImplementedException(ExceptionUtils.render(memberSelectTree));
			}
		}
		return StreamUtils.concat(previousSimpleNames, memberSelectTree.getIdentifier().toString());
	}
}
