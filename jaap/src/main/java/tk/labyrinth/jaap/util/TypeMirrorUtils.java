package tk.labyrinth.jaap.util;

import tk.labyrinth.jaap.langmodel.type.tool.ParameterResolvingTypeVisitor;
import tk.labyrinth.jaap.langmodel.type.util.ArrayTypeUtils;
import tk.labyrinth.jaap.langmodel.type.util.DeclaredTypeUtils;
import tk.labyrinth.jaap.langmodel.type.util.IntersectionTypeUtils;
import tk.labyrinth.jaap.langmodel.type.util.TypeVariableUtils;
import tk.labyrinth.jaap.langmodel.type.util.WildcardTypeUtils;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Parameterizable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.IntersectionType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.ReferenceType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.WildcardType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TypeMirrorUtils {

	/**
	 * Primitives and void.
	 */
	private static final Map<String, Function<ProcessingEnvironment, TypeMirror>> keywordTypeMirrorResolvers;

	static {
		Map<String, Function<ProcessingEnvironment, TypeMirror>> map = Stream.of(TypeKind.values())
				.filter(TypeKind::isPrimitive)
				.collect(Collectors.toMap(
						typeKind -> typeKind.toString().toLowerCase(),
						typeKind -> processingEnvironment -> processingEnvironment.getTypeUtils()
								.getPrimitiveType(typeKind)));
		map.put(
				TypeKind.VOID.toString().toLowerCase(),
				processingEnvironment -> processingEnvironment.getTypeUtils().getNoType(TypeKind.VOID));
		keywordTypeMirrorResolvers = Map.copyOf(map);
	}

	public static int compareConsideringPrimitives(TypeMirror first, TypeMirror second) {
		return evaluateType(first) - evaluateType(second);
	}

	public static TypeMirror erasure(ProcessingEnvironment processingEnvironment, Element element) {
		TypeMirror result;
		if (element instanceof ExecutableElement) {
			result = erasure(processingEnvironment, (ExecutableElement) element);
		} else if (element instanceof TypeElement) {
			result = erasure(processingEnvironment, (TypeElement) element);
		} else if (element instanceof VariableElement) {
			result = erasure(processingEnvironment, (VariableElement) element);
		} else {
			throw new IllegalArgumentException(ExceptionUtils.render(element));
		}
		return result;
	}

	public static TypeMirror erasure(ProcessingEnvironment processingEnvironment, ExecutableElement executableElement) {
		// FIXME: It will probably provide false type for constructors.
		//
		// We convert returnType to String and then resolve it to obtain canonical version of this TypeMirror.
		// See tk.labyrinth.javax.lang.model.element.ExecutableElementTest#testGetReturnType() for issue details.
		return resolve(processingEnvironment, processingEnvironment.getTypeUtils()
				.erasure(executableElement.getReturnType()).toString());
	}

	public static TypeMirror erasure(ProcessingEnvironment processingEnvironment, TypeElement typeElement) {
		return processingEnvironment.getTypeUtils().erasure(typeElement.asType());
	}

	public static TypeMirror erasure(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		return processingEnvironment.getTypeUtils().erasure(typeMirror);
	}

	public static TypeMirror erasure(ProcessingEnvironment processingEnvironment, VariableElement variableElement) {
		return processingEnvironment.getTypeUtils().erasure(variableElement.asType());
	}

	public static List<TypeMirror> erasures(ProcessingEnvironment processingEnvironment, List<TypeMirror> typeMirrors) {
		return typeMirrors.stream()
				.map(typeMirror -> erasure(processingEnvironment, typeMirror))
				.collect(Collectors.toList());
	}

	public static int evaluateType(TypeMirror value) {
		int result;
		switch (value.getKind()) {
			case BOOLEAN:
				result = 0;
				break;
			case BYTE:
				result = 1;
				break;
			case CHAR:
				result = 3;
				break;
			case DOUBLE:
				result = 9;
				break;
			case INT:
				result = 4;
				break;
			case FLOAT:
				result = 5;
				break;
			case LONG:
				result = 8;
				break;
			case SHORT:
				result = 2;
				break;
			case DECLARED:
				result = 16;
				break;
			default:
				throw new UnsupportedOperationException(ExceptionUtils.render(value));
		}
		return result;
	}

	@Nullable
	public static TypeMirror find(ProcessingEnvironment processingEnvironment, Class<?> type) {
		// TODO: Add nullability.
		return resolve(processingEnvironment, type);
	}

	@Nullable
	public static TypeMirror find(ProcessingEnvironment processingEnvironment, Type type) {
		TypeMirror result;
		if (type instanceof Class) {
			result = find(processingEnvironment, (Class<?>) type);
		} else if (type instanceof ParameterizedType) {
			// TODO: Preserve parameters.
			result = find(processingEnvironment, (Class<?>) ((ParameterizedType) type).getRawType());
		} else if (type instanceof TypeVariable) {
			TypeVariable<?> typeVariable = (TypeVariable<?>) type;
			//
			Element element = ElementUtils.find(processingEnvironment, typeVariable.getGenericDeclaration());
			if (element != null) {
				TypeParameterElement typeParameter = ElementUtils.findTypeParameter(element, typeVariable.getName());
				if (typeParameter != null) {
					result = typeParameter.asType();
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		} else {
			throw new NotImplementedException(ExceptionUtils.render(type));
		}
		return result;
	}

	/**
	 * @param processingEnvironment non-null
	 * @param typeFullName          non-null
	 *
	 * @return non-null for primitive or void, null otherwise
	 */
	@Nullable
	public static TypeMirror findKeywordType(ProcessingEnvironment processingEnvironment, String typeFullName) {
		return keywordTypeMirrorResolvers.getOrDefault(typeFullName, innerProcessingEnvironment -> null)
				.apply(processingEnvironment);
	}

	public static TypeMirror fromDeclaration(
			ProcessingEnvironment processingEnvironment,
			Parameterizable context,
			TypeDescription typeDescription) {
		TypeMirror result;
		if (typeDescription.getArrayDepth() != null) {
			// Array
			//
			throw new NotImplementedException();
		} else if (typeDescription.getParameters() != null) {
			// Parameterized
			//
			result = processingEnvironment.getTypeUtils().getDeclaredType(
					TypeElementUtils.get(processingEnvironment, typeDescription.getFullName()),
					typeDescription.getParameters().stream()
							.map(parameter -> fromDeclaration(processingEnvironment, context, parameter))
							.toArray(TypeMirror[]::new));
		} else if (typeDescription.getLowerBound() != null || typeDescription.getUpperBound() != null) {
			// Variable or Wildcard
			//
			throw new NotImplementedException();
		} else {
			// Plain or Primitive or Raw or Variable or Wildcard
			//
			String typeFullName = typeDescription.getFullName();
			//
			if (typeFullName.contains(".")) {
				// Plain or Raw
				//
				TypeElement typeElement = TypeElementUtils.get(processingEnvironment, typeFullName);
				if (typeElement.getTypeParameters().isEmpty()) {
					// Plain
					//
					result = typeElement.asType();
				} else {
					// Raw
					//
					result = erasure(processingEnvironment, typeElement);
				}
			} else {
				// Primitive or Variable or Wildcard
				//
				if (Objects.equals(typeFullName, "?")) {
					// Wildcard
					//
					result = processingEnvironment.getTypeUtils().getWildcardType(null, null);
				} else {
					// Primitive or Variable
					//
					TypeMirror keywordType = findKeywordType(processingEnvironment, typeFullName);
					if (keywordType != null) {
						// Primitive
						//
						result = keywordType;
					} else {
						// Variable
						//
						TypeParameterElement typeParameterElement = ParameterizableUtils
								.findTypeParameter(context, typeFullName, true);
						if (typeParameterElement != null) {
							result = typeParameterElement.asType();
						} else {
							throw new IllegalArgumentException("Unresolvable: " + typeDescription);
						}
					}
				}
			}
		}
		return result;
	}

	public static TypeMirror get(ProcessingEnvironment processingEnvironment, Class<?> type) {
		TypeMirror result = find(processingEnvironment, type);
		if (result == null) {
			throw new IllegalArgumentException("No TypeMirror resolved: type = " + type);
		}
		return result;
	}

	public static TypeMirror get(ProcessingEnvironment processingEnvironment, Type type) {
		TypeMirror result = find(processingEnvironment, type);
		if (result == null) {
			throw new IllegalArgumentException("No TypeMirror resolved: type = " + type);
		}
		return result;
	}

	/**
	 * Returns a Stream of distinct TypeMirrors which are either same as or superclass of provided TypeMirror.<br>
	 * This method returns Classes only.<br>
	 *
	 * @param environment non-null
	 * @param mirror      non-null
	 *
	 * @return non-empty, contains at least provided TypeMirror
	 *
	 * @throws IllegalArgumentException in case provided typeMirror is Interface
	 * @see #getTypeHierarchy(ProcessingEnvironment, TypeMirror)
	 */
	public static Stream<TypeMirror> getClassHierarchy(ProcessingEnvironment environment, TypeMirror mirror) {
		if (environment.getTypeUtils().asElement(mirror).getKind() == ElementKind.INTERFACE) {
			throw new IllegalArgumentException("Class or Enum required: " + mirror);
		}
		List<TypeMirror> result = new ArrayList<>();
		{
			TypeMirror targetTypeMirror = mirror;
			while (targetTypeMirror != null) {
				result.add(targetTypeMirror);
				targetTypeMirror = environment.getTypeUtils().directSupertypes(targetTypeMirror).stream()
						.findFirst().orElse(null);
			}
		}
		return result.stream();
	}

	// FIXME: Do erasure inside.
	public static String getSignatureContributingString(TypeMirror erasedTypeMirror) {
		String result;
		if (TypeMirrorUtils.isDeclaredType(erasedTypeMirror)) {
			// FIXME: Use ElementUtils#getSignature when we will have method without ProcEnv.
			result = TypeElementUtils.getSignature(ElementUtils.requireType(
					TypeMirrorUtils.requireDeclaredType(erasedTypeMirror).asElement()));
		} else if (TypeMirrorUtils.isPrimitiveType(erasedTypeMirror)) {
			// Erasure of primitives does not remove annotations, so we need to remove them here.
			result = erasedTypeMirror.getKind().toString().toLowerCase();
		} else {
			// TODO: Arrays?
			result = erasedTypeMirror.toString();
		}
		return result;
	}

	/**
	 * Returns a Stream of distinct TypeMirrors which are either same as or supertype of provided TypeMirror.<br>
	 * This method returns both Classes and Interfaces.<br>
	 *
	 * @param environment non-null
	 * @param mirror      non-null
	 *
	 * @return non-empty, contains at least provided TypeMirror
	 *
	 * @see #getClassHierarchy(ProcessingEnvironment, TypeMirror)
	 */
	public static Stream<TypeMirror> getTypeHierarchy(ProcessingEnvironment environment, TypeMirror mirror) {
		List<TypeMirror> result = new ArrayList<>();
		result.add(mirror);
		int index = 0;
		while (result.size() > index) {
			environment.getTypeUtils().directSupertypes(result.get(index)).forEach(directSupertype -> {
				if (result.stream().noneMatch(resultTypeMirror ->
						environment.getTypeUtils().isSameType(resultTypeMirror, directSupertype))) {
					result.add(directSupertype);
				}
			});
			index++;
		}
		return result.stream();
	}

	public static boolean isArrayType(TypeMirror typeMirror) {
		return typeMirror instanceof ArrayType;
	}

	public static boolean isAssignable(ProcessingEnvironment processingEnvironment, TypeMirror first, TypeMirror second) {
		return processingEnvironment.getTypeUtils().isAssignable(first, second);
	}

	public static boolean isDeclaredType(TypeMirror typeMirror) {
		return typeMirror instanceof DeclaredType;
	}

	public static boolean isErasure(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		return typeMirror == erasure(processingEnvironment, typeMirror);
	}

	public static boolean isInterface(ProcessingEnvironment environment, TypeMirror typeMirror) {
		return environment.getTypeUtils().asElement(typeMirror).getKind() == ElementKind.INTERFACE;
	}

	public static boolean isIntersectionType(TypeMirror typeMirror) {
		return typeMirror instanceof IntersectionType;
	}

	public static boolean isNoType(TypeMirror typeMirror) {
		return typeMirror instanceof NoType;
	}

	public static boolean isPrimitiveType(TypeMirror typeMirror) {
		return typeMirror instanceof PrimitiveType;
	}

	public static boolean isReferenceType(TypeMirror typeMirror) {
		return typeMirror instanceof ReferenceType;
	}

	public static boolean isTypeVariable(TypeMirror typeMirror) {
		// FIXME: Solve import conflict with java.lang.reflect.TypeVariable.
		return typeMirror instanceof javax.lang.model.type.TypeVariable;
	}

	public static boolean isWildcardType(TypeMirror typeMirror) {
		return typeMirror instanceof WildcardType;
	}

	public static ArrayType requireArrayType(TypeMirror typeMirror) {
		if (!isArrayType(typeMirror)) {
			throw new IllegalArgumentException("Require ArrayType: " + typeMirror);
		}
		return (ArrayType) typeMirror;
	}

	public static DeclaredType requireDeclaredType(TypeMirror typeMirror) {
		if (!isDeclaredType(typeMirror)) {
			throw new IllegalArgumentException("Require DeclaredType: " + typeMirror);
		}
		return (DeclaredType) typeMirror;
	}

	public static IntersectionType requireIntersectionType(TypeMirror typeMirror) {
		if (!isIntersectionType(typeMirror)) {
			throw new IllegalArgumentException("Require IntersectionType: " + typeMirror);
		}
		return (IntersectionType) typeMirror;
	}

	public static NoType requireNoType(TypeMirror value) {
		if (!isNoType(value)) {
			throw new IllegalArgumentException("Require noType: " + value);
		}
		return (NoType) value;
	}

	public static TypeMirror requireNonPrimitive(TypeMirror argument) {
		if (argument == null || argument.getKind().isPrimitive()) {
			throw new IllegalArgumentException("Require non-primitive: " + argument);
		}
		return argument;
	}

	public static PrimitiveType requirePrimitive(TypeMirror typeMirror) {
		if (!isPrimitiveType(typeMirror)) {
			throw new IllegalArgumentException("Require PrimitiveType: " + typeMirror);
		}
		return (PrimitiveType) typeMirror;
	}

	/**
	 * Tests whether first is a subtype of second.
	 *
	 * @param environment non-null
	 * @param first       non-null
	 * @param second      non-null
	 *
	 * @return first
	 */
	public static TypeMirror requireSubtype(ProcessingEnvironment environment, TypeMirror first, TypeMirror second) {
		if (first == null || second == null || !environment.getTypeUtils().isSubtype(first, second)) {
			throw new IllegalArgumentException("Require subtype: first = " + first + ", second = " + second);
		}
		return first;
	}

	public static javax.lang.model.type.TypeVariable requireTypeVariable(TypeMirror typeMirror) {
		// FIXME: Solve qualified name conflict with java.lang.reflect.TypeVariable.
		if (!isTypeVariable(typeMirror)) {
			throw new IllegalArgumentException("Require TypeVariable: " + typeMirror);
		}
		return (javax.lang.model.type.TypeVariable) typeMirror;
	}

	public static TypeMirror resolve(
			ProcessingEnvironment processingEnvironment,
			Map<TypeParameterElement, TypeMirror> typeParameterMappings,
			TypeMirror typeMirror) {
		return new ParameterResolvingTypeVisitor().visit(
				typeMirror,
				new ParameterResolvingTypeVisitor.Context(
						processingEnvironment,
						typeParameterMappings));
	}

	public static TypeMirror resolve(ProcessingEnvironment processingEnvironment, Class<?> type) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(type, "type");
		//
		return resolve(processingEnvironment, type.getCanonicalName(), false);
	}

	@Deprecated
	public static TypeMirror resolve(ProcessingEnvironment processingEnvironment, String typeFullName) {
		return resolve(processingEnvironment, typeFullName, true);
	}

	@Deprecated
	public static TypeMirror resolve(ProcessingEnvironment processingEnvironment, String typeFullName, boolean erase) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(typeFullName, "typeFullName");
		//
		TypeMirror result;
		if (typeFullName.endsWith("[]")) {
			result = processingEnvironment.getTypeUtils().getArrayType(resolve(processingEnvironment,
					typeFullName.substring(0, typeFullName.length() - 2)));
		} else {
			switch (typeFullName) {
				case "boolean":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.BOOLEAN);
					break;
				case "byte":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.BYTE);
					break;
				case "char":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.CHAR);
					break;
				case "double":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.DOUBLE);
					break;
				case "float":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.FLOAT);
					break;
				case "int":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.INT);
					break;
				case "long":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.LONG);
					break;
				case "short":
					result = processingEnvironment.getTypeUtils().getPrimitiveType(TypeKind.SHORT);
					break;
				case "void":
					result = processingEnvironment.getTypeUtils().getNoType(TypeKind.VOID);
					break;
				default:
					TypeElement typeElement = TypeElementUtils.get(processingEnvironment, typeFullName);
					result = erase
							? erasure(processingEnvironment, typeElement)
							: typeElement.asType();
			}
		}
		return result;
	}

	public static TypeMirror resolveUpperBound(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		TypeMirror result;
		switch (typeMirror.getKind()) {
			case ARRAY:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DECLARED:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				result = typeMirror;
				break;
			case NULL:
				result = get(processingEnvironment, Object.class);
				break;
			case NONE:
			case PACKAGE:
			case VOID:
				throw new IllegalArgumentException(ExceptionUtils.render(typeMirror));
			default:
				throw new NotImplementedException(ExceptionUtils.render(typeMirror));
		}
		return result;
	}

	public static TypeDescription toDescription(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		if (isIntersectionType(typeMirror)) {
			throw new IllegalArgumentException("IntersectionType can not be rendered as single TypeDescription: " + typeMirror);
		}
		return toDescriptionConsideringIntersections(processingEnvironment, typeMirror).get(0);
	}

	public static List<TypeDescription> toDescriptionConsideringIntersections(
			ProcessingEnvironment processingEnvironment,
			TypeMirror typeMirror) {
		List<TypeDescription> result;
		if (typeMirror instanceof ArrayType) {
			result = List.of(ArrayTypeUtils.toDescription(processingEnvironment, (ArrayType) typeMirror));
		} else if (typeMirror instanceof DeclaredType) {
			result = List.of(DeclaredTypeUtils.toDescription(processingEnvironment, (DeclaredType) typeMirror));
		} else if (typeMirror instanceof IntersectionType) {
			result = IntersectionTypeUtils.toDescriptions(processingEnvironment, (IntersectionType) typeMirror);
		} else if (typeMirror instanceof NoType) {
			result = List.of(TypeDescription.ofVoid());
		} else if (typeMirror instanceof PrimitiveType) {
			result = List.of(PrimitiveTypeUtils.toDescription((PrimitiveType) typeMirror));
		} else if (typeMirror instanceof javax.lang.model.type.TypeVariable) {
			result = List.of(TypeVariableUtils.toDescription(processingEnvironment, (javax.lang.model.type.TypeVariable) typeMirror));
		} else if (typeMirror instanceof WildcardType) {
			result = List.of(WildcardTypeUtils.toDescription(processingEnvironment, (WildcardType) typeMirror));
		} else {
			throw new NotImplementedException();
		}
		return result;
	}
}
