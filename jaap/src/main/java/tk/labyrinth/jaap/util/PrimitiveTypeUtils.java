package tk.labyrinth.jaap.util;

import tk.labyrinth.jaap.model.declaration.TypeDescription;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import java.util.Objects;

public class PrimitiveTypeUtils {

	public static PrimitiveType resolve(ProcessingEnvironment processingEnvironment, TypeKind typeKind) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(typeKind, "typeKind");
		//
		PrimitiveType result = processingEnvironment.getTypeUtils().getPrimitiveType(typeKind);
		if (result == null) {
			throw new IllegalArgumentException("Not a primitive: " + typeKind);
		}
		return result;
	}

	public static TypeDescription toDescription(PrimitiveType primitiveType) {
		return TypeDescription.builder()
				.fullName(primitiveType.getKind().name().toLowerCase())
				.build();
	}
}
