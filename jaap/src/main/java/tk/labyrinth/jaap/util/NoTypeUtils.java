package tk.labyrinth.jaap.util;

import javax.lang.model.type.NoType;
import javax.lang.model.type.TypeKind;

public class NoTypeUtils {

	public static NoType requireVoid(NoType value) {
		if (value == null || value.getKind() != TypeKind.VOID) {
			throw new IllegalArgumentException("Require void: " + value);
		}
		return value;
	}
}
