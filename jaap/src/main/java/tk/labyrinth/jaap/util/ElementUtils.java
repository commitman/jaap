package tk.labyrinth.jaap.util;

import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.template.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.template.element.util.VariableElementUtils;
import tk.labyrinth.misc4j2.java.lang.EnumUtils;
import tk.labyrinth.misc4j2.java.lang.ObjectUtils;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import java.lang.reflect.Executable;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Member;
import java.util.List;
import java.util.Objects;

public class ElementUtils {

	@Nullable
	public static Element find(ProcessingEnvironment processingEnvironment, GenericDeclaration genericDeclaration) {
		Element result;
		if (genericDeclaration instanceof Class) {
			result = TypeElementUtils.find(processingEnvironment, (Class<?>) genericDeclaration);
		} else if (genericDeclaration instanceof Executable) {
			result = ExecutableElementUtils.find(processingEnvironment, (Executable) genericDeclaration);
		} else {
			throw new NotImplementedException(ExceptionUtils.render(genericDeclaration));
		}
		return result;
	}

	@Nullable
	public static Element find(ProcessingEnvironment processingEnvironment, Member member) {
		throw new NotImplementedException(ExceptionUtils.render(member));
	}

	@Nullable
	public static TypeParameterElement findTypeParameter(Element element, String name) {
		TypeParameterElement result;
		if (element instanceof ExecutableElement) {
			result = ExecutableElementUtils.findTypeParameter((ExecutableElement) element, name);
		} else if (element instanceof TypeElement) {
			result = TypeElementUtils.findTypeParameter((TypeElement) element, name);
		} else {
			throw new NotImplementedException(ExceptionUtils.render(element));
		}
		return result;
	}

	@Deprecated
	public static String getSignature(ProcessingEnvironment processingEnvironment, Element element) {
		String result;
		if (element instanceof ExecutableElement) {
			result = ExecutableElementUtils.getSignatureString(processingEnvironment, (ExecutableElement) element);
		} else if (element instanceof TypeElement) {
			result = TypeElementUtils.getSignature((TypeElement) element);
		} else {
			throw new NotImplementedException(ExceptionUtils.render(element));
		}
		return result;
	}

	public static boolean isConstructor(Element value) {
		return value != null && value.getKind() == ElementKind.CONSTRUCTOR;
	}

	public static boolean isExecutable(Element value) {
		return value instanceof ExecutableElement;
	}

	public static boolean isField(Element value) {
		return value != null && value.getKind() == ElementKind.FIELD;
	}

	public static boolean isInitializer(Element value) {
		return value != null && EnumUtils.in(value.getKind(), ElementKind.INSTANCE_INIT, ElementKind.STATIC_INIT);
	}

	public static boolean isMethod(Element value) {
		return value != null && value.getKind() == ElementKind.METHOD;
	}

	public static boolean isPackage(Element value) {
		return value instanceof PackageElement;
	}

	public static boolean isType(Element value) {
		return value instanceof TypeElement;
	}

	public static boolean isTypeParameter(Element value) {
		return value instanceof TypeParameterElement;
	}

	public static boolean isVariable(Element value) {
		return value instanceof VariableElement;
	}

	@Nullable
	public static Element navigate(ProcessingEnvironment processingEnvironment, Element element, EntitySelectorChain selectorChain) {
		return ObjectUtils.reduce(
				Pair.of(element, selectorChain),
				pair -> pair.getLeft() != null && pair.getRight() != null,
				pair -> {
					Pair<EntitySelector, EntitySelectorChain> headAndTail = pair.getRight().split();
					return Pair.of(selectMember(processingEnvironment, pair.getLeft(), headAndTail.getLeft()), headAndTail.getRight());
				}).getLeft();
	}

	public static ExecutableElement requireConstructor(Element value) {
		if (!isConstructor(value)) {
			// TODO: Render canonicalName.
			throw new IllegalArgumentException("Require constructor: " + value);
		}
		return (ExecutableElement) value;
	}

	public static ExecutableElement requireExecutable(Element element) {
		if (!isExecutable(element)) {
			// TODO: Render canonicalName.
			throw new IllegalArgumentException("Require executable: " + element);
		}
		return (ExecutableElement) element;
	}

	public static VariableElement requireField(Element value) {
		if (!isField(value)) {
			throw new IllegalArgumentException("Require field: " + value);
		}
		return (VariableElement) value;
	}

	public static ExecutableElement requireInitializer(Element value) {
		if (!isInitializer(value)) {
			// TODO: Render canonicalName.
			throw new IllegalArgumentException("Require initializer: " + value);
		}
		return (ExecutableElement) value;
	}

	public static ExecutableElement requireMethod(Element value) {
		if (!isMethod(value)) {
			// TODO: Render canonicalName.
			throw new IllegalArgumentException("Require method: " + value);
		}
		return (ExecutableElement) value;
	}

	public static PackageElement requirePackage(Element value) {
		if (!isPackage(value)) {
			throw new IllegalArgumentException("Require package: " + value);
		}
		return (PackageElement) value;
	}

	public static TypeElement requireType(Element value) {
		if (!isType(value)) {
			throw new IllegalArgumentException("Require type: " + value);
		}
		return (TypeElement) value;
	}

	public static TypeParameterElement requireTypeParameter(Element value) {
		if (!isTypeParameter(value)) {
			throw new IllegalArgumentException("Require TypeParameterElement: " + value);
		}
		return (TypeParameterElement) value;
	}

	public static VariableElement requireVariable(Element value) {
		if (!isVariable(value)) {
			throw new IllegalArgumentException("Require variable: " + value);
		}
		return (VariableElement) value;
	}

	@Nullable
	public static Element selectMember(ProcessingEnvironment processingEnvironment, Element element, EntitySelector selector) {
		Objects.requireNonNull(element, "element");
		//
		Element result;
		if (element instanceof ExecutableElement) {
			result = ExecutableElementUtils.selectMember(processingEnvironment, (ExecutableElement) element, selector);
		} else if (element instanceof TypeElement) {
			result = TypeElementUtils.selectMember(processingEnvironment, (TypeElement) element, selector);
		} else if (element instanceof PackageElement) {
			throw new NotImplementedException(ExceptionUtils.renderList(List.of(element, selector)));
		} else if (element instanceof VariableElement) {
			result = VariableElementUtils.selectMember(processingEnvironment, (VariableElement) element, selector);
		} else {
			throw new UnsupportedOperationException(ExceptionUtils.render(List.of(element, selector)));
		}
		return result;
	}
}
