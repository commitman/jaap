package tk.labyrinth.jaap.util;

import tk.labyrinth.jaap.model.signature.SignatureSeparators;

/**
 * {@link Class#forName} uses {@link Class#getName()};<br>
 * javax.lang.model uses {@link Class#getCanonicalName()}.<br>
 */
public class TypeNameUtils {

	public static boolean isBinary(String binaryName) {
		return binaryName.contains("$");
	}

	public static boolean isSignature(String binaryName) {
		return binaryName.contains(SignatureSeparators.TOP_LEVEL_TYPE);
	}

	public static String toQualified(String typeName) {
		return typeName
				// Binary.
				.replace('$', '.')
				// Signature.
				.replace(':', '.');
	}
}
