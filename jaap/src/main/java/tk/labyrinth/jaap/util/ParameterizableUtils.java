package tk.labyrinth.jaap.util;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Parameterizable;
import javax.lang.model.element.TypeParameterElement;

public class ParameterizableUtils {

	@Nullable
	public static TypeParameterElement findTypeParameter(Parameterizable parameterizable, String name) {
		return findTypeParameter(parameterizable, name, false);
	}

	/**
	 * @param parameterizable non-null
	 * @param name            fit
	 * @param deep            whether to look up in enclosing elements
	 *
	 * @return nullable
	 */
	@Nullable
	@SuppressWarnings("unchecked")
	public static <P extends Element & Parameterizable> TypeParameterElement findTypeParameter(
			P parameterizable,
			String name,
			boolean deep) {
		TypeParameterElement result;
		{
			TypeParameterElement shallowResult = parameterizable.getTypeParameters().stream()
					.filter(typeParameter -> typeParameter.getSimpleName().contentEquals(name))
					.findFirst().orElse(null);
			if (shallowResult == null && deep && !parameterizable.getModifiers().contains(Modifier.STATIC)) {
				Element enclosingElement = parameterizable.getEnclosingElement();
				if (enclosingElement instanceof Parameterizable) {
					result = findTypeParameter((P) enclosingElement, name, true);
				} else {
					result = null;
				}
			} else {
				result = shallowResult;
			}
		}
		return result;
	}
}
