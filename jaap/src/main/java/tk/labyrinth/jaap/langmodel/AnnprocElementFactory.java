package tk.labyrinth.jaap.langmodel;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.template.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.template.element.util.MethodSignatureUtils;
import tk.labyrinth.jaap.template.element.util.PackageElementUtils;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.TypeMirror;
import java.util.Objects;

@RequiredArgsConstructor
public class AnnprocElementFactory implements ElementFactory {

	private final ProcessingEnvironment processingEnvironment;

	@Nullable
	@Override
	public Element findElement(ElementSignature elementSignature) {
		Objects.requireNonNull(elementSignature, "elementSignature");
		//
		Element result;
		if (elementSignature.matchesMethod()) {
			// TODO: Transform into MethodSignature.
			result = findMethod(elementSignature.toString());
		} else if (elementSignature.matchesType()) {
			// TODO: Transform into TypeSignature.
			result = findType(elementSignature.toString());
		} else {
			throw new NotImplementedException(elementSignature.toString());
		}
		return result;
	}

	@Nullable
	@Override
	public ExecutableElement findMethod(Class<?> type, String methodSimpleSignatureString) {
		return findMethod(MethodSignatureUtils.createFull(processingEnvironment, type, methodSimpleSignatureString));
	}

	@Nullable
	@Override
	public ExecutableElement findMethod(MethodFullSignature methodFullSignature) {
		Objects.requireNonNull(methodFullSignature, "methodFullSignature");
		//
		// FIXME: Make it not fail on non-existing methods.
		return ExecutableElementUtils.findMethod(
				processingEnvironment,
				MethodSignatureUtils.createFull(processingEnvironment, methodFullSignature));
	}

	@Nullable
	@Override
	public PackageElement findPackage(String packageSignatureString) {
		return PackageElementUtils.find(processingEnvironment, packageSignatureString);
	}

	@Nullable
	@Override
	public TypeElement findType(CanonicalTypeSignature canonicalTypeSignature) {
		return TypeElementUtils.find(processingEnvironment, canonicalTypeSignature.toString());
	}

	@Nullable
	@Override
	public TypeParameterElement findTypeParameter(String fullTypeParameterElementSignature) {
		Objects.requireNonNull(fullTypeParameterElementSignature, "fullTypeParameterElementSignature");
		//
		TypeParameterElement result;
		{
			Pair<String, String> split = SignatureSeparators.split(
					fullTypeParameterElementSignature,
					SignatureSeparators.TYPE_PARAMETER);
			//
			if (split.getRight() != null) {
				String typeParameterName = split.getRight();
				TypeElement parent = findType(split.getLeft());
				//
				if (parent != null) {
					result = parent.getTypeParameters().stream()
							.filter(typeParameter -> typeParameter.getSimpleName().contentEquals(typeParameterName))
							.findFirst()
							.orElse(null);
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Override
	public PackageElement getPackage(Element element) {
		return ElementUtils.requirePackage(element);
	}

	@Override
	public PackageElement getPackageOf(Class<?> childType) {
		return PackageElementUtils.resolve(processingEnvironment, childType);
	}

	@Override
	public TypeElement getType(Class<?> type) {
		return TypeElementUtils.get(processingEnvironment, type);
	}

	@Override
	public TypeElement getType(TypeMirror typeMirror) {
		return TypeElementUtils.get(processingEnvironment, typeMirror);
	}
}
