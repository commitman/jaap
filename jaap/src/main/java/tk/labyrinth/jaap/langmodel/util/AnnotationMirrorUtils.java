package tk.labyrinth.jaap.langmodel.util;

import javax.annotation.Nullable;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.type.TypeMirror;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AnnotationMirrorUtils {

	@SuppressWarnings("unchecked")
	private static Object unwrapValue(Object value) {
		Object result;
		if (value instanceof List) {
			result = ((List<AnnotationValue>) value).stream()
					.map(AnnotationValue::getValue)
					.collect(Collectors.toUnmodifiableList());
		} else {
			result = value;
		}
		return result;
	}

	@Nullable
	public static Object findValue(AnnotationMirror annotationMirror, String name) {
		return annotationMirror.getElementValues().entrySet().stream()
				.filter(entry -> entry.getKey().getSimpleName().contentEquals(name))
				.findFirst()
				.map(Map.Entry::getValue)
				.map(AnnotationValue::getValue)
				.map(AnnotationMirrorUtils::unwrapValue)
				.orElse(null);
	}

	@Nullable
	@SuppressWarnings("unchecked")
	public static List<AnnotationMirror> findValueAsAnnotationMirrorList(AnnotationMirror annotationMirror, String name) {
		return (List<AnnotationMirror>) findValueList(annotationMirror, name);
	}

	public static String findValueAsString(AnnotationMirror annotationMirror, String name) {
		return (String) findValue(annotationMirror, name);
	}

	@Nullable
	public static List<?> findValueList(AnnotationMirror annotationMirror, String name) {
		return (List<?>) findValue(annotationMirror, name);
	}

	public static Object getValue(AnnotationMirror annotationMirror, String name) {
		Object result = findValue(annotationMirror, name);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"annotationMirror = " + annotationMirror + ", " +
					"name = " + name);
		}
		return result;
	}

	public static List<AnnotationMirror> getValueAsAnnotationMirrorList(AnnotationMirror annotationMirror, String name) {
		List<AnnotationMirror> result = findValueAsAnnotationMirrorList(annotationMirror, name);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"annotationMirror = " + annotationMirror + ", " +
					"name = " + name);
		}
		return result;
	}

	public static String getValueAsString(AnnotationMirror annotationMirror, String name) {
		String result = (String) findValue(annotationMirror, name);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"annotationMirror = " + annotationMirror + ", " +
					"name = " + name);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<String> getValueAsStringList(AnnotationMirror annotationMirror, String name) {
		return (List<String>) findValueList(annotationMirror, name);
	}

	public static TypeMirror getValueAsTypeMirror(AnnotationMirror annotationMirror, String name) {
		return (TypeMirror) findValue(annotationMirror, name);
	}

	@SuppressWarnings("unchecked")
	public static List<TypeMirror> getValueAsTypeMirrorList(AnnotationMirror annotationMirror, String name) {
		return (List<TypeMirror>) findValueList(annotationMirror, name);
	}
}
