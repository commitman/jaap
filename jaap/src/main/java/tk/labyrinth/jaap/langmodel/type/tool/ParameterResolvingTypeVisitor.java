package tk.labyrinth.jaap.langmodel.type.tool;

import lombok.Value;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ErrorType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.IntersectionType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.NullType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.UnionType;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.AbstractTypeVisitor9;
import java.util.Map;

public class ParameterResolvingTypeVisitor extends AbstractTypeVisitor9<TypeMirror, ParameterResolvingTypeVisitor.Context> {

	@Override
	public TypeMirror visitArray(ArrayType type, Context context) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeMirror visitDeclared(DeclaredType type, Context context) {
		return context.getProcessingEnvironment().getTypeUtils().getDeclaredType(
				(TypeElement) type.asElement(),
				type.getTypeArguments().stream()
						.map(typeArgument -> typeArgument.accept(this, context))
						.toArray(TypeMirror[]::new));
	}

	@Override
	public TypeMirror visitError(ErrorType type, Context context) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeMirror visitExecutable(ExecutableType type, Context context) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeMirror visitIntersection(IntersectionType type, Context context) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeMirror visitNoType(NoType type, Context context) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeMirror visitNull(NullType type, Context context) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeMirror visitPrimitive(PrimitiveType type, Context context) {
		return type;
	}

	@Override
	@SuppressWarnings("SuspiciousMethodCalls")
	public TypeMirror visitTypeVariable(TypeVariable type, Context context) {
		TypeMirror typeMirror = context.typeParameterMappings.get(type.asElement());
		return typeMirror != null
				? typeMirror.accept(this, context)
				: type;
	}

	@Override
	public TypeMirror visitUnion(UnionType type, Context context) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeMirror visitWildcard(WildcardType type, Context context) {
		return context.getProcessingEnvironment().getTypeUtils().getWildcardType(
				type.getExtendsBound() != null
						? type.getExtendsBound().accept(this, context)
						: null,
				type.getSuperBound() != null
						? type.getSuperBound().accept(this, context)
						: null);
	}

	@Value
	public static class Context {

		ProcessingEnvironment processingEnvironment;

		Map<TypeParameterElement, TypeMirror> typeParameterMappings;
	}
}
