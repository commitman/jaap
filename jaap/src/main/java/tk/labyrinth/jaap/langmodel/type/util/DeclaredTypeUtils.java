package tk.labyrinth.jaap.langmodel.type.util;

import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.jaap.util.TypeMirrorUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DeclaredTypeUtils {

	public static DeclaredType from(ProcessingEnvironment environment, Class<?> type, Class<?>... parameters) {
		return from(environment, type, Stream.of(parameters));
	}

	public static DeclaredType from(ProcessingEnvironment environment, Class<?> type, Stream<Class<?>> parameters) {
		return from(environment, TypeElementUtils.get(environment, type), parameters.map(parameter ->
				TypeElementUtils.get(environment, parameter)).map(TypeElement::asType));
	}

	public static DeclaredType from(ProcessingEnvironment environment, TypeElement element, Stream<TypeMirror> parameters) {
		return from(environment, element, parameters.toArray(TypeMirror[]::new));
	}

	public static DeclaredType from(ProcessingEnvironment environment, TypeElement element, TypeMirror... parameters) {
		return environment.getTypeUtils().getDeclaredType(element, parameters);
	}

	public static DeclaredType from(TypeMirror typeMirror) {
		return TypeMirrorUtils.requireDeclaredType(typeMirror);
	}

	public static boolean isAnnotation(DeclaredType declaredType) {
		return declaredType != null && declaredType.asElement().getKind() == ElementKind.ANNOTATION_TYPE;
	}

	public static boolean isGeneric(DeclaredType declaredType) {
		return declaredType != null &&
				ElementUtils.requireType(declaredType.asElement()).getTypeParameters().size() > 0;
	}

	public static boolean isParameterized(DeclaredType declaredType) {
		return declaredType != null && declaredType.getTypeArguments().size() > 0;
	}

	public static boolean isPlain(DeclaredType declaredType) {
		return declaredType != null &&
				ElementUtils.requireType(declaredType.asElement()).getTypeParameters().size() == 0;
	}

	public static boolean isRaw(DeclaredType declaredType) {
		return declaredType != null && !isPlain(declaredType) && declaredType.getTypeArguments().size() == 0;
	}

	public static DeclaredType requireParameterized(DeclaredType declaredType) {
		if (!isParameterized(declaredType)) {
			throw new IllegalArgumentException("Require parameterized: " + declaredType);
		}
		return declaredType;
	}

	public static DeclaredType requireRaw(DeclaredType declaredType) {
		if (!isRaw(declaredType)) {
			throw new IllegalArgumentException("Require raw: " + declaredType);
		}
		return declaredType;
	}

	public static TypeDescription toDescription(ProcessingEnvironment processingEnvironment, DeclaredType declaredType) {
		return TypeDescription.builder()
				// FIXME: Use ElementUtils#getSignature.
				.fullName(TypeElementUtils.getSignature(ElementUtils.requireType(declaredType.asElement())))
				.parameters(!declaredType.getTypeArguments().isEmpty()
						? declaredType.getTypeArguments().stream()
						.map(typeArgument -> TypeMirrorUtils.toDescription(processingEnvironment, typeArgument))
						.collect(Collectors.toList())
						: null)
				.build();
	}

	public static DeclaredType toRaw(ProcessingEnvironment processingEnvironment, DeclaredType declaredType) {
		return TypeMirrorUtils.requireDeclaredType(processingEnvironment.getTypeUtils().erasure(declaredType));
	}
}
