package tk.labyrinth.jaap.langmodel.type.util;

import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.util.TypeMirrorUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.IntersectionType;
import java.util.List;
import java.util.stream.Collectors;

public class IntersectionTypeUtils {

	public static List<TypeDescription> toDescriptions(
			ProcessingEnvironment processingEnvironment,
			IntersectionType intersectionType) {
		return intersectionType.getBounds().stream()
				.map(bound -> TypeMirrorUtils.toDescription(processingEnvironment, bound))
				.collect(Collectors.toList());
	}
}
