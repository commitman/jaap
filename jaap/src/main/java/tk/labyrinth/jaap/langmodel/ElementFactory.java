package tk.labyrinth.jaap.langmodel;

import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.TypeMirror;

// FIXME: Rename to LangmodelElementFactory.
public interface ElementFactory {

	@Nullable
	Element findElement(ElementSignature elementSignature);

	@Nullable
	default Element findElement(String elementSignatureString) {
		return findElement(ElementSignature.of(elementSignatureString));
	}

	@Nullable
	ExecutableElement findMethod(Class<?> type, String methodSimpleSignatureString);

	@Nullable
	ExecutableElement findMethod(MethodFullSignature methodFullSignature);

	@Nullable
	default ExecutableElement findMethod(String methodFullSignature) {
		return findMethod(MethodFullSignature.of(methodFullSignature));
	}

	@Nullable
	PackageElement findPackage(String packageSignatureString);

	@Nullable
	TypeElement findType(CanonicalTypeSignature canonicalTypeSignature);

	@Nullable
	default TypeElement findType(String typeSignatureString) {
		return findType(CanonicalTypeSignature.ofValid(typeSignatureString));
	}

	@Nullable
	TypeParameterElement findTypeParameter(String fullTypeParameterElementSignature);

	PackageElement getPackage(Element element);

	PackageElement getPackageOf(Class<?> childType);

	TypeElement getType(Class<?> type);

	default TypeElement getType(String typeSignatureString) {
		TypeElement result = findType(typeSignatureString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeSignatureString = " + typeSignatureString);
		}
		return result;
	}

	TypeElement getType(TypeMirror typeMirror);

	default TypeParameterElement getTypeParameter(String fullTypeParameterElementSignature) {
		TypeParameterElement result = findTypeParameter(fullTypeParameterElementSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fullTypeParameterElementSignature = " + fullTypeParameterElementSignature);
		}
		return result;
	}
}
