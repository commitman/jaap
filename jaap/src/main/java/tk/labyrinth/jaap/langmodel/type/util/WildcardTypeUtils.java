package tk.labyrinth.jaap.langmodel.type.util;

import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.util.TypeMirrorUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.WildcardType;

public class WildcardTypeUtils {

	public static TypeDescription toDescription(ProcessingEnvironment processingEnvironment, WildcardType wildcardType) {
		return TypeDescription.builder()
				.fullName("?")
				.lowerBound(wildcardType.getSuperBound() != null
						? TypeMirrorUtils.toDescription(processingEnvironment, wildcardType.getSuperBound())
						: null)
				.upperBound(wildcardType.getExtendsBound() != null
						? TypeMirrorUtils.toDescriptionConsideringIntersections(
						processingEnvironment,
						wildcardType.getExtendsBound())
						: null)
				.build();
	}
}
