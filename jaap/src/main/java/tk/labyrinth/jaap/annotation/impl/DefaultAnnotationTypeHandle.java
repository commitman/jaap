package tk.labyrinth.jaap.annotation.impl;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeElementHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.annotation.Nullable;
import java.lang.annotation.Repeatable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@EqualsAndHashCode
@RequiredArgsConstructor
public class DefaultAnnotationTypeHandle implements AnnotationTypeHandle {

	private final TypeElementTemplate typeElementHandle;

	@Nullable
	@Override
	public AnnotationTypeElementHandle findElementDescription(String elementName) {
		AnnotationTypeElementHandle result;
		{
			MethodElementTemplate methodElementHandle = typeElementHandle.findMethodByName(elementName);
			if (methodElementHandle != null) {
				result = new DefaultAnnotationTypeElementHandle(methodElementHandle);
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public AnnotationTypeHandle findRepeatable() {
		AnnotationTypeHandle result;
		{
			AnnotationTypeElementHandle valueElementHandle = findElementDescription("value");
			if (valueElementHandle != null && valueElementHandle.isAnnotationListElement()) {
				AnnotationTypeHandle valueAnnotationType = valueElementHandle.asAnnotationListElement().getType();
				result = Objects.equals(valueAnnotationType.findRepeater(), this) ? valueAnnotationType : null;
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public AnnotationTypeHandle findRepeater() {
		AnnotationTypeHandle result;
		{
			AnnotationHandle annotationHandle = typeElementHandle.findDirectAnnotation(Repeatable.class);
			result = annotationHandle != null
					? annotationHandle.getValueAsClass("value").asAnnotationType()
					: null;
		}
		return result;
	}

	@Override
	public AnnotationTypeElementHandle getElementDescription(String elementName) {
		AnnotationTypeElementHandle result = findElementDescription(elementName);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"this = " + this + ", " +
					"elementName = " + elementName);
		}
		return result;
	}

	@Override
	public List<AnnotationTypeElementHandle> getElementDescriptions() {
		return typeElementHandle.getDeclaredMethods()
				.map(DefaultAnnotationTypeElementHandle::new)
				.collect(Collectors.toList());
	}

	@Override
	public TypeElementTemplate toElement() {
		return typeElementHandle;
	}

	@Override
	public String toString() {
		return "@" + typeElementHandle;
	}
}
