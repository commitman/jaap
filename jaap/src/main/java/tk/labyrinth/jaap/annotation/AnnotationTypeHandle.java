package tk.labyrinth.jaap.annotation;

import tk.labyrinth.jaap.handle.element.common.ConvertibleToTypeElementTemplate;

import javax.annotation.Nullable;
import javax.lang.model.element.TypeElement;
import java.util.List;

/**
 * Represents type of Java annotation.<br>
 * <br>
 * Backed by:<br>
 * - Reflection: {@link Class}<br>
 * - Langmodel: {@link TypeElement}<br>
 */
public interface AnnotationTypeHandle extends
		ConvertibleToTypeElementTemplate {

	@Nullable
	AnnotationTypeElementHandle findElementDescription(String elementName);

	@Nullable
	AnnotationTypeHandle findRepeatable();

	@Nullable
	AnnotationTypeHandle findRepeater();

	AnnotationTypeElementHandle getElementDescription(String elementName);

	List<AnnotationTypeElementHandle> getElementDescriptions();

	default boolean isRepeatable() {
		return findRepeater() != null;
	}

	default boolean isRepeater() {
		return findRepeatable() != null;
	}
}
