package tk.labyrinth.jaap.annotation.merged.relation;

import lombok.Value;

@Value
public class UnweightedAnnotationRelation {

	boolean certain;

	// TODO: Move to weighted.
	AnnotationRelationResolver creator;

	String description;

	MergedAnnotationNode from;

	MergedAnnotationNode to;
}
