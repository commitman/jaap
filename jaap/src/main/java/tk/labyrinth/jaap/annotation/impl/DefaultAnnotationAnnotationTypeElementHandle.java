package tk.labyrinth.jaap.annotation.impl;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.annotation.AnnotationAnnotationTypeElementHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeElementHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;

@EqualsAndHashCode
@RequiredArgsConstructor
public class DefaultAnnotationAnnotationTypeElementHandle implements AnnotationAnnotationTypeElementHandle {

	private final MethodElementTemplate methodElementHandle;

	@Override
	public AnnotationTypeElementHandle asAnnotation() {
		return new DefaultAnnotationTypeElementHandle(methodElementHandle);
	}

	@Override
	public AnnotationTypeHandle getType() {
		return methodElementHandle.getReturnType().asAnnotationType();
	}
}
