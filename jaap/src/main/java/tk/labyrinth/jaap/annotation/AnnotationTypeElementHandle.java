package tk.labyrinth.jaap.annotation;

import javax.annotation.Nullable;
import javax.lang.model.element.ExecutableElement;
import java.lang.reflect.Method;

/**
 * https://docs.oracle.com/javase/specs/jls/se8/html/jls-9.html#jls-9.6.1<br>
 * <br>
 * Backed by:<br>
 * - Reflection: {@link Method}<br>
 * - Langmodel: {@link ExecutableElement}<br>
 */
public interface AnnotationTypeElementHandle {

	AnnotationAnnotationTypeElementHandle asAnnotationListElement();

	@Nullable
	String findDefaultValueAsString();

	String getDefaultValueAsString();

	String getName();

	boolean isAnnotationListElement();
}
