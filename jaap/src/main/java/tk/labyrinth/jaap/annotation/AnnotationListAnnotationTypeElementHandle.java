package tk.labyrinth.jaap.annotation;

/**
 * https://docs.oracle.com/javase/specs/jls/se8/html/jls-9.html#jls-9.6.1
 */
public interface AnnotationListAnnotationTypeElementHandle {

	AnnotationTypeElementHandle asAnnotation();

	AnnotationTypeHandle getType();
}
