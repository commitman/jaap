package tk.labyrinth.jaap.annotation.merged.relation;

import lombok.Value;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.template.element.ElementTemplate;

@Value
public class MergedAnnotationNode {

	AnnotationHandle annotationHandle;

	ElementTemplate elementHandle;

	public static MergedAnnotationNode of(AnnotationHandle annotationHandle) {
		return new MergedAnnotationNode(annotationHandle, null);
	}

	public static MergedAnnotationNode of(ElementTemplate elementHandle) {
		return new MergedAnnotationNode(null, elementHandle);
	}
}
