package tk.labyrinth.jaap.annotation;

import tk.labyrinth.jaap.handle.type.TypeHandle;

/**
 * https://docs.oracle.com/javase/specs/jls/se8/html/jls-9.html#jls-9.6.1
 */
public interface SimpleAnnotationTypeElementHandle<T> {

	AnnotationElementHandle asAnnotation();

	// FIXME: We can separate into String & Primitive, then there will be just PrimitiveType.
	TypeHandle getType();
}
