package tk.labyrinth.jaap.annotation;

import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;

import javax.annotation.Nullable;
import javax.lang.model.element.AnnotationMirror;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Objects;

/**
 * Represents annotation on Java construct such as elements and types.<br>
 * <br>
 * Backed by:<br>
 * - Reflection: A extends {@link Annotation}<br>
 * - Langmodel: {@link AnnotationMirror}<br>
 */
public interface AnnotationHandle {

	@Nullable
	AnnotationElementHandle findAttribute(String attributeName);

	@Nullable
	Object findValue(String attributeName);

	@Nullable
	String findValueAsString(String attributeName);

	default AnnotationElementHandle getAttribute(String attributeName) {
		AnnotationElementHandle result = findAttribute(attributeName);
		if (result == null) {
			throw new NotImplementedException("Not found: " +
					"attributeName = " + attributeName + ", " +
					"this = " + this);
		}
		return result;
	}

	String getSignatureString();

	AnnotationTypeHandle getType();

	Object getValue(String attributeName);

	List<AnnotationHandle> getValueAsAnnotationList(String attributeName);

	TypeHandle getValueAsClass(String attributeName);

	List<TypeHandle> getValueAsClassList(String attributeName);

	String getValueAsString(String attributeName);

	List<String> getValueAsStringList(String attributeName);

	default boolean isOfType(AnnotationTypeHandle annotationTypeHandle) {
		Objects.requireNonNull(annotationTypeHandle, "annotationTypeHandle");
		//
		return Objects.equals(getType(), annotationTypeHandle);
	}
}
