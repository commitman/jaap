package tk.labyrinth.jaap.annotation;

import tk.labyrinth.jaap.handle.type.TypeHandle;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ExecutableElement;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;

/**
 * https://docs.oracle.com/javase/specs/jls/se8/html/jls-9.html#jls-9.6.1<br>
 * <br>
 * Equivalent to entry of {@link AnnotationMirror#getElementValues()}.
 * <br>
 * Backed by:<br>
 * - Reflection: {@link Method} and A extends {@link Annotation}<br>
 * - Langmodel: {@link ExecutableElement} and {@link AnnotationValue}<br>
 */
public interface AnnotationElementHandle {

	ClassAnnotationElementHandle<TypeHandle> asClassAnnotation();

	ClassAnnotationElementHandle<List<TypeHandle>> asClassListAnnotation();

	AnnotationTypeElementHandle getDescription();

	String getName();

	AnnotationHandle getOwner();

	AnnotationHandle getValueAsAnnotation();

	TypeHandle getValueAsClass();

	List<TypeHandle> getValueAsClassList();

	String getValueAsString();

	List<String> getValueAsStringList();

	boolean isClassAnnotation();

	boolean isClassListAnnotation();

	boolean isList();

	

	boolean isPresent();
}
