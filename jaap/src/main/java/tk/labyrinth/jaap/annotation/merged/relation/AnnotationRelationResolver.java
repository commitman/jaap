package tk.labyrinth.jaap.annotation.merged.relation;

import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;

import java.util.List;

public interface AnnotationRelationResolver {

	List<UnweightedAnnotationRelation> resolve(MergedAnnotationNode node, AnnotationTypeHandle annotationTypeHandle);
}
