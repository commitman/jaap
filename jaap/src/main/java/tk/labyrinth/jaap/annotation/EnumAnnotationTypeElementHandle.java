package tk.labyrinth.jaap.annotation;

import tk.labyrinth.jaap.template.element.TypeElementTemplate;

/**
 * https://docs.oracle.com/javase/specs/jls/se8/html/jls-9.html#jls-9.6.1
 */
public interface EnumAnnotationTypeElementHandle {

	AnnotationElementHandle asAnnotation();

	TypeElementTemplate getType();
}
