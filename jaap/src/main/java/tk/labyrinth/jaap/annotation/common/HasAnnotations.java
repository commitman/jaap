package tk.labyrinth.jaap.annotation.common;

import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotation;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationContext;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.annotation.Nullable;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public interface HasAnnotations {

	@Nullable
	default AnnotationHandle findDirectAnnotation(AnnotationTypeHandle annotationTypeHandle) {
		return getDirectAnnotations(annotationTypeHandle).stream()
				.collect(CollectorUtils.findOnly(true));
	}

	@Nullable
	AnnotationHandle findDirectAnnotation(Class<? extends Annotation> annotationType);

	@Deprecated
	@Nullable
	MergedAnnotation findMergedAnnotation(
			AnnotationTypeHandle annotationTypeHandle,
			MergedAnnotationSpecification mergedAnnotationSpecification);

	@Deprecated
	@Nullable
	MergedAnnotation findMergedAnnotation(
			Class<? extends Annotation> annotationType,
			MergedAnnotationSpecification mergedAnnotationSpecification);

	default AnnotationHandle getDirectAnnotation(AnnotationTypeHandle annotationTypeHandle) {
		AnnotationHandle result = findDirectAnnotation(annotationTypeHandle);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"annotationTypeHandle = " + annotationTypeHandle + ", " +
					"this = " + this);
		}
		return result;
	}

	default AnnotationHandle getDirectAnnotation(Class<? extends Annotation> annotationType) {
		AnnotationHandle result = findDirectAnnotation(annotationType);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"annotationType = " + annotationType + ", " +
					"this = " + this);
		}
		return result;
	}

	default List<AnnotationHandle> getDirectAnnotations(AnnotationTypeHandle annotationTypeHandle) {
		return getDirectAnnotations().stream()
				.filter(annotationHandle -> Objects.equals(annotationHandle.getType(), annotationTypeHandle))
				.collect(Collectors.toList());
	}

	List<AnnotationHandle> getDirectAnnotations();

	default MergedAnnotation getMergedAnnotation(
			AnnotationTypeHandle annotationTypeHandle,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		MergedAnnotation result = findMergedAnnotation(annotationTypeHandle, mergedAnnotationSpecification);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"annotationTypeHandle = " + annotationTypeHandle + ", " +
					"this = " + this);
		}
		return result;
	}

	default MergedAnnotation getMergedAnnotation(
			Class<? extends Annotation> annotationType,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		MergedAnnotation result = findMergedAnnotation(annotationType, mergedAnnotationSpecification);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"annotationType = " + annotationType + ", " +
					"this = " + this);
		}
		return result;
	}

	MergedAnnotation getMergedAnnotation(
			String annotationTypeName,
			MergedAnnotationSpecification mergedAnnotationSpecification);

	MergedAnnotationContext getMergedAnnotationContext(
			AnnotationTypeHandle annotationTypeHandle,
			MergedAnnotationSpecification mergedAnnotationSpecification);

	MergedAnnotationContext getMergedAnnotationContext(
			Class<? extends Annotation> annotationType,
			MergedAnnotationSpecification mergedAnnotationSpecification);

	/**
	 * Shorthand for {@link #getMergedAnnotation(AnnotationTypeHandle, MergedAnnotationSpecification)
	 * getMergedAnnotation()}.{@link MergedAnnotation#isPresent() isPresent()}.
	 *
	 * @param annotationTypeHandle          non-null
	 * @param mergedAnnotationSpecification non-null
	 *
	 * @return non-null
	 */
	default boolean hasMergedAnnotation(
			AnnotationTypeHandle annotationTypeHandle,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotation(annotationTypeHandle, mergedAnnotationSpecification).isPresent();
	}

	/**
	 * Shorthand for {@link #getMergedAnnotation(Class, MergedAnnotationSpecification)
	 * getMergedAnnotation()}.{@link MergedAnnotation#isPresent() isPresent()}.
	 *
	 * @param annotationType                non-null
	 * @param mergedAnnotationSpecification non-null
	 *
	 * @return non-null
	 */
	default boolean hasMergedAnnotation(
			Class<? extends Annotation> annotationType,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotation(annotationType, mergedAnnotationSpecification).isPresent();
	}

	/**
	 * Shorthand for {@link #getMergedAnnotation(String, MergedAnnotationSpecification)
	 * getMergedAnnotation()}.{@link MergedAnnotation#isPresent() isPresent()}.
	 *
	 * @param annotationTypeName            non-null
	 * @param mergedAnnotationSpecification non-null
	 *
	 * @return non-null
	 */
	default boolean hasMergedAnnotation(
			String annotationTypeName,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotation(annotationTypeName, mergedAnnotationSpecification).isPresent();
	}
}
