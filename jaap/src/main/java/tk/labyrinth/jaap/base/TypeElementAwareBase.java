package tk.labyrinth.jaap.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import tk.labyrinth.jaap.context.ProcessingContext;

import javax.lang.model.element.TypeElement;
import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.0
 */
@EqualsAndHashCode
@Getter
public abstract class TypeElementAwareBase extends ProcessingContextAwareBase {

	private final TypeElement typeElement;

	public TypeElementAwareBase(ProcessingContext processingContext, TypeElement typeElement) {
		super(processingContext);
		this.typeElement = Objects.requireNonNull(typeElement, "typeElement");
	}
}
