package tk.labyrinth.jaap.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import tk.labyrinth.jaap.context.ProcessingContext;

import javax.lang.model.element.VariableElement;
import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.0
 */
@EqualsAndHashCode
@Getter
public abstract class VariableElementAwareBase extends ProcessingContextAwareBase {

	private final VariableElement variableElement;

	public VariableElementAwareBase(ProcessingContext processingContext, VariableElement variableElement) {
		super(processingContext);
		this.variableElement = Objects.requireNonNull(variableElement, "variableElement");
	}
}
