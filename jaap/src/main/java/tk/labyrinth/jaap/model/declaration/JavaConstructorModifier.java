package tk.labyrinth.jaap.model.declaration;

public enum JavaConstructorModifier implements ConstructorModifier {
	PRIVATE,
	PROTECTED,
	PUBLIC
}
