package tk.labyrinth.jaap.model.element.template.synthetic;

import lombok.EqualsAndHashCode;
import tk.labyrinth.jaap.base.ProcessingContextAwareBase;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.element.template.mixin.PackageElementTemplateMixin;
import tk.labyrinth.jaap.template.element.ElementTemplate;

import javax.annotation.Nullable;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import java.util.Objects;

@EqualsAndHashCode
public class SyntheticPackageElementTemplate extends ProcessingContextAwareBase implements PackageElementTemplateMixin {

	private final String packageQualifiedName;

	public SyntheticPackageElementTemplate(ProcessingContext processingContext, String packageQualifiedName) {
		super(processingContext);
		this.packageQualifiedName = Objects.requireNonNull(packageQualifiedName, "packageQualifiedName");
	}

	@Override
	public PackageElement getPackageElement() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	@Nullable
	@Override
	public ElementTemplate getParent() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String getQualifiedName() {
		return packageQualifiedName;
	}

	@Override
	public String getSignatureString() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Name getSimpleName() {
		// TODO: Support names without dots.
		return getProcessingContext().getName(packageQualifiedName.substring(
				packageQualifiedName.lastIndexOf(".")));
	}

	@Override
	public boolean isSynthetic() {
		return true;
	}

	@Override
	public String toString() {
		return packageQualifiedName;
	}
}
