package tk.labyrinth.jaap.model.entity.mixin;

import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.template.element.ElementTemplate;

import javax.annotation.Nullable;

public interface HasSelectableMembers {

	@Nullable
	ElementTemplate selectMember(EntitySelector selector);

	@Nullable
	default ElementTemplate selectMember(EntitySelectorChain selectorChain) {
		ElementTemplate result;
		{
			Pair<EntitySelector, EntitySelectorChain> headAndTail = selectorChain.split();
			EntitySelector head = headAndTail.getKey();
			EntitySelectorChain tail = headAndTail.getValue();
			//
			ElementTemplate child = selectMember(head);
			if (child != null) {
				if (tail != null) {
					result = child.selectMember(tail);
				} else {
					result = child;
				}
			} else {
				result = null;
			}
		}
		return result;
	}
}
