package tk.labyrinth.jaap.model.signature;

import lombok.Getter;
import lombok.Value;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.misc4j.exception.UnreachableStateException;
import tk.labyrinth.jaap.misc4j.java.lang.ClassUtils;
import tk.labyrinth.misc4j2.java.util.function.FunctionUtils;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * package
 * package.package
 * package:type
 * package@annotation
 * <br>
 * type
 * type.type
 * type#field
 * type#method()
 * type@annotation
 * <br>
 * #field
 * #field@annotation
 * <br>
 * #method()
 * #method()#0 - 0th parameter
 * #method()#parameter
 * #method()@annotation
 * <br>
 * #0
 * #0@annotation
 * <br>
 * \@annotation
 * \@annotation#0 - 0th annotation if multiple present
 * \@annotation#attribute
 * \@annotation#attribute#0 - 0th annotation in this attribute
 */
@Value
public class ElementSignature {

	public static final Pattern ANNOTATION_REMAINDER_PATTERN = Pattern.compile("^" +
			"(?<mainSegment>.+)(?<remainingSegment>[@#].+)" +
			"$");

	public static final Pattern PACKAGES_OR_TYPES_PATTERN = Pattern.compile("^" +
			"(?<packagesOrTypesSegment>[\\w$.]+)(?<remainingSegment>[#@].+)" +
			"$");

	public static final Pattern PACKAGES_PATTERN = Pattern.compile("^" +
			"(?<packagesSegment>[\\w.]+):(?<remainingSegment>.+)" +
			"$");

	String content;

	@Getter
	@Nullable
	ElementSignature parent;

	String type;

	private Stream<ElementSignature> doBreakDown() {
		return Stream.concat(
				parent != null ? parent.doBreakDown() : Stream.empty(),
				Stream.of(this));
	}

	private String getSeparator() {
		String result;
		switch (type) {
			case "annotation":
				result = "@";
				break;
			case "field":
			case "formalParameter":
			case "method":
				result = "#";
				break;
			case "package":
			case "packageOrType":
				result = ".";
				break;
			case "type":
				result = parent != null && Objects.equals(parent.getType(), "package") ? ":" : ".";
				break;
			default:
				throw new NotImplementedException(type);
		}
		return result;
	}

	public List<ElementSignature> breakDown() {
		return doBreakDown().collect(Collectors.toList());
	}

	public ElementSignature getParentOrFail() {
		return Objects.requireNonNull(parent, "parent");
	}

	public boolean matchesAnnotation() {
		return Objects.equals(type, "annotation");
	}

	public boolean matchesMethod() {
		return Objects.equals(type, "method");
	}

	public boolean matchesType() {
		return Set.of("packageOrType", "type").contains(type);
	}

	public MethodFullSignature toMethodFullSignature() {
		return MethodFullSignature.of(toString());
	}

	public String toSegmentString() {
		return parent != null ?
				getSeparator() + content
				: content;
	}

	@Override
	public String toString() {
		return parent != null
				? parent + toSegmentString()
				: toSegmentString();
	}

	public String toTechnicalString() {
		return parent != null
				? parent.toTechnicalString() + "/" + getType() + ":" + content
				: getType() + ":" + content;
	}

	private static ElementSignature of(ElementSignature parent, String elementSignatureString) {
		ElementSignature result;
		{
			// Looking for packages.
			//
			Matcher packagesMatcher = PACKAGES_PATTERN.matcher(elementSignatureString);
			if (packagesMatcher.matches()) {
				// Has packages -> looking for types in substring.
				//
				result = of(
						ofPackagesOrTypes(
								parent,
								packagesMatcher.group("packagesSegment"),
								"package"),
						packagesMatcher.group("remainingSegment"));
			} else {
				// No packages -> looking for types.
				//
				Matcher typesMatcher = PACKAGES_OR_TYPES_PATTERN.matcher(elementSignatureString);
				if (typesMatcher.matches()) {
					result = of(
							ofPackagesOrTypes(
									parent,
									typesMatcher.group("packagesOrTypesSegment"),
									parent != null ? "type" : "packageOrType"),
							typesMatcher.group("remainingSegment"));
				} else {
					if (parent != null) {
						if (elementSignatureString.startsWith("@")) {
							// Annotation.
							//
							result = ofAnnotation(parent, elementSignatureString.substring(1));
						} else if (elementSignatureString.startsWith("#")) {
							// Non-type member.
							//
							result = ofNonTypeMember(
									parent,
									elementSignatureString.substring(1));
						} else {
							result = ofPackagesOrTypes(parent, elementSignatureString, "type");
						}
					} else {
						result = ofPackagesOrTypes(null, elementSignatureString, "packageOrType");
					}
				}
			}
		}
		return result;
	}

	private static ElementSignature ofAnnotation(ElementSignature parent, String signatureString) {
		ElementSignature result;
		{
			String type = "annotation";
			//
			val splitResult = split(signatureString, '#', '@');
			Character separator = splitResult.getMiddle();
			//
			ElementSignature currentSegment = new ElementSignature(splitResult.getLeft(), parent, type);
			//
			if (separator != null) {
				if (separator == '#') {
					// TODO:
					//  #0 - ordinal in case of repeatable
					//  #name - attribute
					throw new NotImplementedException();
				} else {
					throw new IllegalArgumentException(parent + " " + signatureString);
				}
			} else {
				result = currentSegment;
			}
		}
		return result;
	}

	private static ElementSignature ofExecutable(ElementSignature parent, String signatureString) {
		ElementSignature result;
		{
			// FIXME: Support constructors.
			String type = "method";
			//
			val splitResult = split(signatureString, '#', '@');
			Character separator = splitResult.getMiddle();
			//
			ElementSignature currentSegment = new ElementSignature(splitResult.getLeft(), parent, type);
			//
			if (separator != null) {
				if (separator == '#') {
					result = ofFormalParameter(currentSegment, splitResult.getRight());
				} else if (separator == '@') {
					result = ofAnnotation(currentSegment, splitResult.getRight());
				} else {
					throw new UnreachableStateException();
				}
			} else {
				result = currentSegment;
			}
		}
		return result;
	}

	private static ElementSignature ofFormalParameter(ElementSignature parent, String signatureString) {
		ElementSignature result;
		{
			String type = "formalParameter";
			//
			val splitResult = split(signatureString, '#', '@');
			Character separator = splitResult.getMiddle();
			//
			ElementSignature currentSegment = new ElementSignature(splitResult.getLeft(), parent, type);
			//
			if (separator != null) {
				if (separator == '@') {
					result = ofAnnotation(currentSegment, splitResult.getRight());
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				result = currentSegment;
			}
		}
		return result;
	}

	private static ElementSignature ofNonTypeMember(ElementSignature parent, String nonTypeMemberSignatureString) {
		ElementSignature result;
		if (nonTypeMemberSignatureString.contains("(")) {
			result = ofExecutable(parent, nonTypeMemberSignatureString);
		} else {
			result = new ElementSignature(nonTypeMemberSignatureString, parent, "field");
		}
		return result;
	}

	private static ElementSignature ofPackagesOrTypes(
			ElementSignature parent,
			String elementSignatureString,
			String type) {
		return Stream.of(elementSignatureString.split("\\."))
				.reduce(
						parent,
						(previous, segment) -> new ElementSignature(segment, previous, type),
						FunctionUtils::throwUnreachableStateException);
	}

	private static Triple<String, Character, String> split(String signatureString, char... separators) {
		Triple<String, Character, String> result;
		{
			int index = StringUtils.indexOfAny(signatureString, separators);
			if (index != -1) {
				char separator = signatureString.charAt(index);
				result = Triple.of(
						signatureString.substring(0, index),
						separator,
						signatureString.substring(index + 1));
			} else {
				result = Triple.of(signatureString, null, null);
			}
		}
		return result;
	}

	public static ElementSignature of(Class<?> type) {
		return of(ClassUtils.getSignature(type));
	}

	public static ElementSignature of(String elementSignatureString) {
		return of(null, elementSignatureString);
	}
}
