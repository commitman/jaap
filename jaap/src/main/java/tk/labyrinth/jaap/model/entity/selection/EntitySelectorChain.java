package tk.labyrinth.jaap.model.entity.selection;

import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.trees.util.MemberSelectTreeUtils;
import tk.labyrinth.jaap.trees.util.MethodInvocationTreeUtils;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class EntitySelectorChain {

	EntitySelectionContext context;

	List<String> simpleNames;

	public boolean canBeMethod() {
		return context.canBeMethod();
	}

	public boolean canBePackage() {
		return context.canBePackage();
	}

	public boolean canBeType() {
		return context.canBeType();
	}

	public boolean canBeVariable() {
		return context.canBeVariable();
	}

	public EntitySelectionContext getContext(int index) {
		return IntStream.range(0, simpleNames.size() - index - 1)
				.mapToObj(internalIndex -> (EntitySelectionContext) null)
				.reduce(context, (first, second) -> first.getParent());
	}

	public int getLength() {
		return simpleNames.size();
	}

	public String getLongName() {
		return String.join(".", simpleNames);
	}

	public EntitySelector head() {
		return EntitySelector.build(getContext(0), simpleNames.get(0));
	}

	public EntitySelectorChain prepend(String simpleName) {
		// FIXME: Replace flatten with concat of E and Collection<E>.
		return new EntitySelectorChain(context, ListUtils.flatten(List.of(simpleName), simpleNames));
	}

	public Pair<EntitySelector, EntitySelectorChain> split() {
		return Pair.of(head(), simpleNames.size() > 1 ? new EntitySelectorChain(context, simpleNames.subList(1, simpleNames.size())) : null);
	}

	private static Stream<String> extractSimpleNames(MemberSelectTree memberSelectTree) {
		return MemberSelectTreeUtils.getSimpleNames(memberSelectTree);
	}

	private static Stream<String> extractSimpleNames(MethodInvocationTree methodInvocationTree) {
		return MethodInvocationTreeUtils.getSimpleNames(methodInvocationTree);
	}

	public static EntitySelectorChain build(EntitySelectionContext selectionContext, MemberSelectTree memberSelectTree) {
		return build(selectionContext, extractSimpleNames(memberSelectTree));
	}

	public static EntitySelectorChain build(EntitySelectionContext selectionContext, MethodInvocationTree methodInvocationTree) {
		return build(selectionContext, extractSimpleNames(methodInvocationTree));
	}

	public static EntitySelectorChain build(EntitySelectionContext selectionContext, Stream<String> simpleNames) {
		return new EntitySelectorChain(selectionContext, simpleNames.collect(Collectors.toList()));
	}

	public static EntitySelectorChain forMethod(MemberSelectTree memberSelectTree, Stream<TypeDescription> argumentTypes) {
		return build(EntitySelectionContext.forMethod(argumentTypes), extractSimpleNames(memberSelectTree));
	}

	public static EntitySelectorChain forMethod(MethodInvocationTree methodInvocationTree, Stream<TypeDescription> argumentTypes) {
		return build(EntitySelectionContext.forMethod(argumentTypes), extractSimpleNames(methodInvocationTree));
	}

	public static EntitySelectorChain forMethod(Stream<String> simpleNames, Stream<TypeDescription> argumentTypes) {
		return build(EntitySelectionContext.forMethod(argumentTypes), simpleNames);
	}

	public static EntitySelectorChain forMethod(Stream<String> simpleNames, TypeDescription... argumentTypes) {
		return forMethod(simpleNames, Stream.of(argumentTypes));
	}

	public static EntitySelectorChain forStarOrTypeInImport(Stream<String> simpleNames) {
		return build(EntitySelectionContext.forStarOrTypeName(), simpleNames);
	}

	public static EntitySelectorChain forType(Stream<String> simpleNames) {
		return build(EntitySelectionContext.forType(), simpleNames);
	}

	public static EntitySelectorChain forVariable(Stream<String> simpleNames) {
		return build(EntitySelectionContext.forVariable(), simpleNames);
	}

	public static EntitySelectorChain forVariableType(MemberSelectTree memberSelectTree) {
		return forVariableType(extractSimpleNames(memberSelectTree));
	}

	public static EntitySelectorChain forVariableType(Stream<String> simpleNames) {
		return build(EntitySelectionContext.forVariableType(), simpleNames);
	}
}
