package tk.labyrinth.jaap.model.declaration;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;

import java.util.List;

@Builder(toBuilder = true)
@Value
public class MethodDeclaration {
	//
	// TODO: Throws

	@Builder.Default
	List<FormalParameterDeclaration> formalParameters = List.of();

	@Builder.Default
	List<MethodModifier> modifiers = List.of();

	String name;

	@Builder.Default
	TypeDescription returnType = TypeDescription.ofVoid();

	@Builder.Default
	List<TypeParameterDeclaration> typeParameters = List.of();

	public MethodSimpleSignature getSignature() {
		return MethodSimpleSignature.of(
				name,
				formalParameters.stream()
						.map(FormalParameterDeclaration::getType)
						// FIXME: For signature we need non-parameterized variant.
						.map(TypeDescription::getErasureString));
	}

	@SuppressWarnings("unused")
	public static class MethodDeclarationBuilder {

		public MethodDeclarationBuilder modifiers(List<MethodModifier> modifiers) {
			this.modifiers$value = modifiers;
			this.modifiers$set = true;
			return this;
		}

		public MethodDeclarationBuilder modifiers(MethodModifier... modifiers) {
			return modifiers(modifiers != null ? List.of(modifiers) : null);
		}
	}
}
