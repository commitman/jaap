package tk.labyrinth.jaap.model.element.template.lang;

import tk.labyrinth.jaap.base.PackageElementAwareBase;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.element.template.mixin.PackageElementTemplateMixin;
import tk.labyrinth.jaap.template.element.ElementTemplate;

import javax.annotation.Nullable;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;

public class LangPackageElementTemplate extends PackageElementAwareBase implements PackageElementTemplateMixin {

	public LangPackageElementTemplate(ProcessingContext processingContext, PackageElement packageElement) {
		super(processingContext, packageElement);
	}

	@Nullable
	@Override
	public ElementTemplate getParent() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String getQualifiedName() {
		return getPackageElement().getQualifiedName().toString();
	}

	@Override
	public String getSignatureString() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Name getSimpleName() {
		return getPackageElement().getSimpleName();
	}

	@Override
	public boolean isSynthetic() {
		return false;
	}

	@Override
	public String toString() {
		return getPackageElement().toString();
	}
}
