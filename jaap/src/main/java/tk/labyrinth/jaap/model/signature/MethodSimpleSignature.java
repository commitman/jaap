package tk.labyrinth.jaap.model.signature;

import lombok.Value;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Value
public class MethodSimpleSignature {

	public static final Pattern PATTERN = Pattern.compile("^(?<name>\\w+)(?<braces>\\((?<parameters>[\\w.:$\\[\\],]*)\\))$");

	String name;

	// TODO: Replace String with TypeSignature.
	List<String> parameters;

	private MethodSimpleSignature(String name, List<String> parameters) {
		this.name = Objects.requireNonNull(name, "name");
		// FIXME: Ensure no null elements passed.
		this.parameters = Objects.requireNonNull(parameters, "parameters");
	}

	public MethodFullSignature toFull(String typeFullName) {
		return new MethodFullSignature(this, typeFullName);
	}

	@Override
	public String toString() {
		return name + "(" + String.join(",", parameters) + ")";
	}

	public static MethodSimpleSignature of(String methodSimpleSignature) {
		Matcher matcher = PATTERN.matcher(methodSimpleSignature);
		if (!matcher.matches()) {
			throw new IllegalArgumentException("False MethodSimpleSignature: " + methodSimpleSignature);
		}
		List<String> parameters;
		{
			String parametersGroup = matcher.group("parameters");
			if (!parametersGroup.isEmpty()) {
				parameters = List.of(parametersGroup.split(","));
			} else {
				parameters = List.of();
			}
		}
		return of(matcher.group("name"), parameters);
	}

	public static MethodSimpleSignature of(String simpleName, List<String> parameters) {
		return new MethodSimpleSignature(simpleName, parameters);
	}

	public static MethodSimpleSignature of(String simpleName, Stream<String> parameters) {
		return of(simpleName, ListUtils.fromNullable(parameters));
	}
}
