package tk.labyrinth.jaap.model.element.template.mixin;

import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.model.element.template.synthetic.SyntheticPackageElementTemplate;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;
import tk.labyrinth.jaap.template.element.util.PackageElementUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;

public interface PackageElementTemplateMixin extends PackageElementTemplate {

	@Override
	default TypeHandle resolveType() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	@Nullable
	@Override
	default ElementTemplate selectMember(EntitySelector selector) {
		ElementTemplate result;
		if (selector.canBeType() || selector.canBePackage()) {
			String memberFullName = getQualifiedName() + "." + selector.getSimpleName();
			//
			Element element = PackageElementUtils.findMember(getProcessingContext().getProcessingEnvironment(), memberFullName, selector.getContext());
			if (element != null) {
				result = getProcessingContext().getElementTemplate(element);
			} else {
				result = new SyntheticPackageElementTemplate(getProcessingContext(), memberFullName);
			}
		} else {
			result = null;
		}
		return result;
	}
}
