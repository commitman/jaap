package tk.labyrinth.jaap.model.signature;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;
import tk.labyrinth.jaap.misc4j.java.lang.ClassUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

/**
 * FIXME: Describe differences from canonical name for local and anonymous classes and arrays.
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class CanonicalTypeSignature {

	// TODO: Check the cases of ..Type and p..Type
	public static final Pattern PATTERN = Pattern.compile("^" +
			"((?<packagesSegment>[\\w$.]+):)?" +
			"(?<typesSegment>[\\w$.]+)" +
			"(?<arraysSegment>(\\[])+)?" +
			"$");

	int arrayCount;

	List<String> packages;

	List<String> types;

	public ElementSignature toElementSignature() {
		return ElementSignature.of(toString());
	}

	public String toQualifiedName() {
		return toString().replace(SignatureSeparators.TOP_LEVEL_TYPE, SignatureSeparators.PACKAGE_OR_TYPE);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		//
		builder.append(StringUtils.join(packages, SignatureSeparators.PACKAGE_OR_TYPE));
		//
		if (!packages.isEmpty()) {
			builder.append(SignatureSeparators.TOP_LEVEL_TYPE);
		}
		//
		builder.append(StringUtils.join(types, SignatureSeparators.PACKAGE_OR_TYPE));
		//
		IntStream.range(0, arrayCount).forEach(index -> builder.append("[]"));
		//
		return builder.toString();
	}

	public static CanonicalTypeSignature of(Class<?> type) {
		return ofValid(ClassUtils.getSignature(type));
	}

	public static CanonicalTypeSignature ofValid(String typeSignature) {
		Matcher matcher = PATTERN.matcher(typeSignature);
		//
		if (!matcher.matches()) {
			throw new IllegalArgumentException("False CanonicalTypeSignature: " + typeSignature);
		}
		//
		String arraysSegment = matcher.group("arraysSegment");
		String packagesSegment = matcher.group("packagesSegment");
		String typesSegment = matcher.group("typesSegment");
		//
		List<String> packages = packagesSegment != null
				? List.of(StringUtils.splitPreserveAllTokens(
				packagesSegment,
				SignatureSeparators.PACKAGE_OR_TYPE))
				: List.of();
		List<String> types = List.of(StringUtils.splitPreserveAllTokens(
				typesSegment,
				SignatureSeparators.PACKAGE_OR_TYPE));
		int arrayCount = arraysSegment != null
				? arraysSegment.length() / 2
				: 0;
		//
		if (packages.stream().anyMatch(String::isEmpty) || types.stream().anyMatch(String::isEmpty)) {
			throw new IllegalArgumentException("False CanonicalTypeSignature: " + typeSignature);
		}
		//
		return new CanonicalTypeSignature(arrayCount, packages, types);
	}
}
