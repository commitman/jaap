package tk.labyrinth.jaap.model.declaration;

import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;
import tk.labyrinth.jaap.misc4j.java.lang.ClassUtils;
import tk.labyrinth.jaap.model.common.JavaConstants;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * https://docs.oracle.com/javase/specs/jls/se15/html/jls-4.html#jls-4.1
 */
@Builder(toBuilder = true)
@Value
public class TypeDescription {
	// TODO: Ensure consistent field combinations

	public static final Pattern PATTERN = Pattern.compile("^" +
			"(?<mainSegment>.+?)" +
			"(\\sextends\\s(?<extendsSegment>.+?))?" +
			"(\\ssuper\\s(?<superSegment>.+?))?" +
			"(<(?<chevronSegment>.*)>)?" +
			"(?<bracketsSegment>(\\[])+)?" +
			"$");

	@Nullable
	Integer arrayDepth;

	String fullName;

	@Nullable
	TypeDescription lowerBound;

	@Nullable
	List<TypeDescription> parameters;

	@Nullable
	List<TypeDescription> upperBound;

	private String toStringRepresentation(boolean excludeParameters, boolean simplifyVariableTypes) {
		StringBuilder stringBuilder = new StringBuilder();
		{
			String fullNameString;
			if (simplifyVariableTypes) {
				int variableSeparatorIndex = fullName.indexOf(SignatureSeparators.TYPE_PARAMETER);
				if (variableSeparatorIndex != -1) {
					fullNameString = fullName.substring(variableSeparatorIndex + 1);
				} else {
					fullNameString = fullName;
				}
			} else {
				fullNameString = fullName;
			}
			stringBuilder.append(fullNameString);
		}
		if (lowerBound != null) {
			stringBuilder
					.append(" super ")
					.append(lowerBound.toStringRepresentation(excludeParameters, simplifyVariableTypes));
		}
		if (!excludeParameters && parameters != null) {
			stringBuilder
					.append("<")
					.append(parameters.stream()
							.map(parameter -> parameter.toStringRepresentation(
									excludeParameters,
									simplifyVariableTypes))
							.collect(Collectors.joining(",")))
					.append(">");
		}
		if (upperBound != null) {
			stringBuilder
					.append(" extends ")
					.append(upperBound.stream()
							.map(parameter -> parameter.toStringRepresentation(
									excludeParameters,
									simplifyVariableTypes))
							.collect(Collectors.joining("&")));
		}
		if (arrayDepth != null) {
			stringBuilder.append(StringUtils.repeat("[]", arrayDepth));
		}
		return stringBuilder.toString();
	}

	// FIXME: This method does not work well for VariableType representation
	//  as we need extra information (bounds of TypeParameter) to properly resolve erasure.
	public String getErasureString() {
		return toStringRepresentation(true, false);
	}

	public String getSimpleString() {
		return toStringRepresentation(false, true);
	}

	public String getVariableName() {
		return fullName.substring(fullName.indexOf(SignatureSeparators.TYPE_PARAMETER) + 1);
	}

	public String getVariableParent() {
		return fullName.substring(0, fullName.indexOf(SignatureSeparators.TYPE_PARAMETER));
	}

	public boolean isArray() {
		return arrayDepth != null;
	}

	public boolean isKeyword() {
		return JavaConstants.KEYWORDS.contains(fullName);
	}

	public boolean isParameterized() {
		return parameters != null;
	}

	public boolean isVariable() {
		return fullName.contains(SignatureSeparators.TYPE_PARAMETER);
	}

	public boolean isWildcard() {
		return Objects.equals(fullName, "?");
	}

	@Override
	public String toString() {
		return toStringRepresentation(false, false);
	}

	@Nullable
	private static Integer resolveBracketsSegment(@Nullable String bracketsSegment) {
		return bracketsSegment != null ? bracketsSegment.length() / 2 : null;
	}

	@Nullable
	static List<TypeDescription> resolveList(@Nullable String value, String delimiter) {
		List<TypeDescription> result;
		if (value != null) {
			List<String> items = new ArrayList<>();
			//
			StringTokenizer stringTokenizer = new StringTokenizer(value, delimiter + "<>", true);
			//
			StringBuilder currentItem = new StringBuilder();
			int depth = 0;
			while (stringTokenizer.hasMoreTokens()) {
				String token = stringTokenizer.nextToken();
				switch (token) {
					case "<":
						currentItem.append("<");
						depth++;
						break;
					case ">":
						currentItem.append(">");
						depth--;
						break;
					default:
						if (depth == 0 && Objects.equals(token, delimiter)) {
							items.add(currentItem.toString().trim());
							currentItem = new StringBuilder();
						} else {
							currentItem.append(token);
						}
				}
			}
			items.add(currentItem.toString().trim());
			//
			result = items.stream()
					.map(TypeDescription::of)
					.collect(Collectors.toList());
		} else {
			result = null;
		}
		return result;
	}

	@Nullable
	static TypeDescription resolveString(@Nullable String value) {
		return value != null ? of(value) : null;
	}

	public static TypeDescription of(String typeDescriptionString) {
		TypeDescription result;
		{
			Matcher matcher = PATTERN.matcher(typeDescriptionString);
			if (matcher.matches()) {
				//
				// x
				String mainSegment = matcher.group("mainSegment");
				//
				// x extends y&z
				String extendsSegment = matcher.group("extendsSegment");
				//
				// x super y
				String superSegment = matcher.group("superSegment");
				//
				// x<y,z>
				String chevronSegment = matcher.group("chevronSegment");
				//
				// x[][]
				String bracketsSegment = matcher.group("bracketsSegment");
				//
				{
					int complexity = 0;
					if (extendsSegment != null) {
						complexity += 1;
					}
					if (superSegment != null) {
						complexity += 1;
					}
					if (chevronSegment != null || bracketsSegment != null) {
						complexity += 1;
					}
					if (complexity > 1) {
						throw new IllegalArgumentException("Overcomplicated: " + typeDescriptionString);
					}
				}
				//
				result = TypeDescription.builder()
						.arrayDepth(resolveBracketsSegment(bracketsSegment))
						.fullName(mainSegment)
						.lowerBound(resolveString(superSegment))
						.parameters(resolveList(chevronSegment, ","))
						.upperBound(resolveList(extendsSegment, "&"))
						.build();
			} else {
				throw new IllegalArgumentException("Does not match pattern: " + typeDescriptionString);
			}
		}
		return result;
	}

	public static TypeDescription ofNonParameterized(Class<?> type) {
		return of(ClassUtils.getSignature(type));
	}

	public static TypeDescription ofVoid() {
		return of("void");
	}
}
