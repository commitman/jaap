package tk.labyrinth.jaap.model.signature;

import lombok.Value;

import java.util.List;

@Value
public class MethodFullSignature {

	MethodSimpleSignature simpleSignature;

	// TODO: TypeFullSignature instead of String
	String typeFullSignature;

	// FIXME: Wrong method, we can't guarantee it to be canonical.
	public CanonicalTypeSignature getCanonicalTypeSignature() {
		return CanonicalTypeSignature.ofValid(typeFullSignature);
	}

	public String getName() {
		return simpleSignature.getName();
	}

	public List<String> getParameters() {
		return simpleSignature.getParameters();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (typeFullSignature != null) {
			builder.append(typeFullSignature).append('#');
		}
		{
			builder.append(simpleSignature.toString());
		}
		return builder.toString();
	}

	public static MethodFullSignature of(String methodFullSignatureString) {
		MethodFullSignature result;
		{
			ElementSignature elementSignature = ElementSignature.of(methodFullSignatureString);
			if (elementSignature.matchesMethod()) {
				result = new MethodFullSignature(
						// FIXME: We want to report argument of this method if simple signature creation fails.
						MethodSimpleSignature.of(elementSignature.getContent()),
						elementSignature.getParentOrFail().toString());
				MethodSimpleSignature.of(elementSignature.getContent());
			} else {
				throw new IllegalArgumentException("False MethodFullSignature: " + methodFullSignatureString);
			}
		}
		return result;
	}
}
