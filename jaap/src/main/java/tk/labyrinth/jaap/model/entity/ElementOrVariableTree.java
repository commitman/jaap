package tk.labyrinth.jaap.model.entity;

import com.sun.source.tree.VariableTree;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;

import javax.lang.model.element.Element;
import java.util.Objects;

public interface ElementOrVariableTree {

	Element asElement();

	VariableTree asVariableTree();

	boolean isElement();

	boolean isVariableTree();

	static ElementOrVariableTree ofElement(Element element) {
		return new ElementImpl(Objects.requireNonNull(element, "element"));
	}

	static ElementOrVariableTree ofVariableTree(VariableTree variableTree) {
		return new VariableTreeImpl(Objects.requireNonNull(variableTree, "variableTree"));
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	@Value
	class ElementImpl implements ElementOrVariableTree {

		Element element;

		@Override
		public Element asElement() {
			return element;
		}

		@Override
		public VariableTree asVariableTree() {
			throw new UnsupportedOperationException(ExceptionUtils.render(this));
		}

		@Override
		public boolean isElement() {
			return true;
		}

		@Override
		public boolean isVariableTree() {
			return false;
		}
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	@Value
	class VariableTreeImpl implements ElementOrVariableTree {

		VariableTree variableTree;

		@Override
		public Element asElement() {
			throw new UnsupportedOperationException(ExceptionUtils.render(this));
		}

		@Override
		public VariableTree asVariableTree() {
			return variableTree;
		}

		@Override
		public boolean isElement() {
			return false;
		}

		@Override
		public boolean isVariableTree() {
			return true;
		}
	}
}
