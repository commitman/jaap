package tk.labyrinth.jaap.model.entity.selection;

import com.google.common.collect.Streams;
import com.sun.source.tree.IdentifierTree;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.util.MethodSignatureUtils;
import tk.labyrinth.jaap.util.TypeMirrorUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class EntitySelector {

	EntitySelectionContext context;

	String simpleName;

	public EntitySelectorChain asChain() {
		return EntitySelectorChain.build(context, Stream.of(simpleName));
	}

	public boolean canBeMethod() {
		return context.canBeMethod();
	}

	public boolean canBePackage() {
		return context.canBePackage();
	}

	public boolean canBeType() {
		return context.canBeType();
	}

	public boolean canBeVariable() {
		return context.canBeVariable();
	}

	public MethodSimpleSignature getMethodSignature(ProcessingEnvironment processingEnvironment) {
		if (!canBeMethod()) {
			throw new IllegalStateException("Not a method selector: " + this);
		}
		return MethodSignatureUtils.createSimple2(processingEnvironment, simpleName, context.getArgumentTypes());
	}

	public boolean matches(ProcessingEnvironment processingEnvironment, Element element) {
		boolean result;
		if (Objects.equals(simpleName, element.getSimpleName().toString())) {
			switch (element.getKind()) {
				case CLASS:
				case INTERFACE:
					result = canBeType();
					break;
				case FIELD:
					result = canBeVariable();
					break;
				case METHOD:
					// First condition is cheap and ensures we are looking for Method.
					// Second one does full signature matching.
					result = canBeMethod() && methodSignatureMatches(
							processingEnvironment,
							getMethodSignature(processingEnvironment),
							MethodSignatureUtils.createSimple(processingEnvironment, (ExecutableElement) element));
					break;
				case PACKAGE:
					result = canBePackage();
					break;
				default:
					result = false;
			}
		} else {
			result = false;
		}
		return result;
	}

	private static String extractSimpleName(IdentifierTree identifierTree) {
		return identifierTree.getName().toString();
	}

	// FIXME: find better name for method and its parameters
	private static boolean methodSignatureMatches(
			ProcessingEnvironment processingEnvironment,
			MethodSimpleSignature methodSignature,
			MethodSimpleSignature declaredSignature) {
		boolean result;
		if (Objects.equals(methodSignature.getName(), declaredSignature.getName())) {
			List<String> methodParameters = methodSignature.getParameters();
			List<String> declaredParameters = declaredSignature.getParameters();
			if (methodParameters.size() == declaredParameters.size()) {
				//noinspection UnstableApiUsage
				result = Streams
						.zip(methodParameters.stream(), declaredParameters.stream(), Pair::of)
						.allMatch(pair -> processingEnvironment.getTypeUtils().isAssignable(
								TypeMirrorUtils.resolve(processingEnvironment, pair.getKey()),
								TypeMirrorUtils.resolve(processingEnvironment, pair.getValue())));
			} else {
				result = false;
			}
		} else {
			result = false;
		}
		return result;
	}

	public static EntitySelector build(EntitySelectionContext selectionContext, IdentifierTree identifierTree) {
		return build(selectionContext, extractSimpleName(identifierTree));
	}

	public static EntitySelector build(EntitySelectionContext selectionContext, String simpleName) {
		return new EntitySelector(selectionContext, simpleName);
	}

	public static EntitySelector forMethod(String simpleName, Stream<TypeDescription> argumentTypes) {
		return build(EntitySelectionContext.forMethod(argumentTypes), simpleName);
	}

	public static EntitySelector forMethodInvocation(String simpleName, Stream<TypeDescription> argumentTypes) {
		return build(EntitySelectionContext.forMethodInvocation(argumentTypes), simpleName);
	}

	public static EntitySelector forTypeName(IdentifierTree identifierTree) {
		return forTypeName(extractSimpleName(identifierTree));
	}

	public static EntitySelector forTypeName(String simpleName) {
		return build(EntitySelectionContext.forTypeName(), simpleName);
	}

	public static EntitySelector forVariable(IdentifierTree identifierTree) {
		return forVariable(extractSimpleName(identifierTree));
	}

	public static EntitySelector forVariable(String simpleName) {
		return build(EntitySelectionContext.forVariable(), simpleName);
	}

	public static EntitySelector forVariableOrType(IdentifierTree identifierTree) {
		return forVariableOrType(extractSimpleName(identifierTree));
	}

	public static EntitySelector forVariableOrType(String name) {
		return build(EntitySelectionContext.forVariableOrType(), name);
	}

	public static EntitySelector forVariableType(IdentifierTree identifierTree) {
		return forVariableType(extractSimpleName(identifierTree));
	}

	public static EntitySelector forVariableType(String simpleName) {
		return build(EntitySelectionContext.forVariableType(), simpleName);
	}
}
