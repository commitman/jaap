package tk.labyrinth.jaap.model.common;

import java.util.Set;

public class JavaConstants {

	public static final Set<String> KEYWORDS = Set.of(
			boolean.class.getName(),
			byte.class.getName(),
			char.class.getName(),
			double.class.getName(),
			float.class.getName(),
			int.class.getName(),
			long.class.getName(),
			short.class.getName(),
			void.class.getName());
}
