package tk.labyrinth.jaap.model.entity.selection;

import lombok.Value;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.misc4j2.java.lang.EnumUtils;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Value
public class EntitySelectionContext {

	@Nullable
	List<TypeDescription> argumentTypes;

	EntitySelectionType type;

	private EntitySelectionContext(@Nullable List<TypeDescription> argumentTypes, EntitySelectionType type) {
		this.argumentTypes = argumentTypes;
		this.type = Objects.requireNonNull(type, "type");
		if (EnumUtils.in(type, EntitySelectionType.METHOD, EntitySelectionType.METHOD_INVOCATION)) {
			if (argumentTypes == null) {
				throw new IllegalArgumentException("argumentTypes must not be null: " + type);
			}
			if (argumentTypes.stream().anyMatch(Objects::isNull)) {
				throw new IllegalArgumentException("argumentTypes must not contain nulls: " + argumentTypes);
			}
		} else {
			if (argumentTypes != null) {
				throw new IllegalArgumentException("argumentTypes must be null: " + type);
			}
		}
	}

	public boolean canBeMethod() {
		return EnumUtils.in(type,
				EntitySelectionType.METHOD);
	}

	public boolean canBePackage() {
		return EnumUtils.in(type,
				EntitySelectionType.PACKAGE,
				EntitySelectionType.TYPE_NAME_OR_PACKAGE,
				EntitySelectionType.TYPE_OR_PACKAGE,
				EntitySelectionType.VARIABLE_OR_TYPE_OR_PACKAGE);
	}

	public boolean canBeType() {
		return EnumUtils.in(type,
				EntitySelectionType.METHOD_INVOCATION_TARGET,
				EntitySelectionType.STAR_OR_TYPE_NAME,
				EntitySelectionType.TYPE,
				EntitySelectionType.TYPE_NAME,
				EntitySelectionType.TYPE_NAME_OR_PACKAGE,
				EntitySelectionType.TYPE_OR_PACKAGE,
				EntitySelectionType.VARIABLE_OR_TYPE,
				EntitySelectionType.VARIABLE_OR_TYPE_OR_PACKAGE,
				EntitySelectionType.VARIABLE_TYPE);
	}

	public boolean canBeVariable() {
		return EnumUtils.in(type,
				EntitySelectionType.METHOD_INVOCATION_ARGUMENT,
				EntitySelectionType.METHOD_INVOCATION_TARGET,
				EntitySelectionType.VARIABLE,
				EntitySelectionType.VARIABLE_INITIALIZER,
				EntitySelectionType.VARIABLE_OR_TYPE,
				EntitySelectionType.VARIABLE_OR_TYPE_OR_PACKAGE);
	}

	public EntitySelectionContext getParent() {
		EntitySelectionContext result;
		{
			switch (type) {
				case IMPORT_DECLARATION:
					result = EntitySelectionContext.forStarOrTypeName();
					break;
				case METHOD:
				case TYPE:
				case VARIABLE:
					result = EntitySelectionContext.forVariableOrType();
					break;
				case METHOD_INVOCATION:
					result = EntitySelectionContext.forMethod(Objects.requireNonNull(argumentTypes).stream());
					break;
				case METHOD_INVOCATION_ARGUMENT:
				case VARIABLE_INITIALIZER:
					result = EntitySelectionContext.forVariable();
					break;
				case METHOD_INVOCATION_TARGET:
				case TYPE_OR_PACKAGE:
				case VARIABLE_OR_TYPE:
				case VARIABLE_OR_TYPE_OR_PACKAGE:
					result = EntitySelectionContext.forVariableOrTypeOrPackage();
					break;
				case PACKAGE:
				case PACKAGE_DECLARATION:
					result = EntitySelectionContext.forPackage();
					break;
				case STAR_OR_TYPE_NAME:
				case TYPE_NAME:
				case TYPE_NAME_OR_PACKAGE:
					result = EntitySelectionContext.forTypeNameOrPackage();
					break;
				case UNDEFINED:
					result = EntitySelectionContext.forUndefined();
					break;
				case VARIABLE_TYPE:
					result = EntitySelectionContext.forTypeName();
					break;
				default:
					throw new NotImplementedException(ExceptionUtils.render(this));
			}
		}
		return result;
	}

	public static EntitySelectionContext forImportDeclaration() {
		return new EntitySelectionContext(null, EntitySelectionType.IMPORT_DECLARATION);
	}

	public static EntitySelectionContext forMethod(Stream<TypeDescription> argumentTypes) {
		return new EntitySelectionContext(argumentTypes.collect(Collectors.toList()), EntitySelectionType.METHOD);
	}

	public static EntitySelectionContext forMethod(TypeDescription... argumentTypes) {
		return forMethod(Stream.of(argumentTypes));
	}

	public static EntitySelectionContext forMethodInvocation(Stream<TypeDescription> argumentTypes) {
		return new EntitySelectionContext(argumentTypes.collect(Collectors.toList()), EntitySelectionType.METHOD_INVOCATION);
	}

	public static EntitySelectionContext forMethodInvocation(TypeDescription... argumentTypes) {
		return forMethodInvocation(Stream.of(argumentTypes));
	}

	public static EntitySelectionContext forMethodInvocationArgument() {
		return new EntitySelectionContext(null, EntitySelectionType.METHOD_INVOCATION_ARGUMENT);
	}

	public static EntitySelectionContext forMethodInvocationTarget() {
		return new EntitySelectionContext(null, EntitySelectionType.METHOD_INVOCATION_TARGET);
	}

	public static EntitySelectionContext forPackage() {
		return new EntitySelectionContext(null, EntitySelectionType.PACKAGE);
	}

	public static EntitySelectionContext forPackageDeclaration() {
		return new EntitySelectionContext(null, EntitySelectionType.PACKAGE_DECLARATION);
	}

	public static EntitySelectionContext forStarOrTypeName() {
		return new EntitySelectionContext(null, EntitySelectionType.STAR_OR_TYPE_NAME);
	}

	public static EntitySelectionContext forType() {
		return new EntitySelectionContext(null, EntitySelectionType.TYPE);
	}

	public static EntitySelectionContext forTypeName() {
		return new EntitySelectionContext(null, EntitySelectionType.TYPE_NAME);
	}

	public static EntitySelectionContext forTypeNameOrPackage() {
		return new EntitySelectionContext(null, EntitySelectionType.TYPE_NAME_OR_PACKAGE);
	}

	public static EntitySelectionContext forTypeOrPackage() {
		return new EntitySelectionContext(null, EntitySelectionType.TYPE_OR_PACKAGE);
	}

	public static EntitySelectionContext forUndefined() {
		return new EntitySelectionContext(null, EntitySelectionType.UNDEFINED);
	}

	public static EntitySelectionContext forVariable() {
		return new EntitySelectionContext(null, EntitySelectionType.VARIABLE);
	}

	public static EntitySelectionContext forVariableInitializer() {
		return new EntitySelectionContext(null, EntitySelectionType.VARIABLE_INITIALIZER);
	}

	public static EntitySelectionContext forVariableOrType() {
		return new EntitySelectionContext(null, EntitySelectionType.VARIABLE_OR_TYPE);
	}

	public static EntitySelectionContext forVariableOrTypeOrPackage() {
		return new EntitySelectionContext(null, EntitySelectionType.VARIABLE_OR_TYPE_OR_PACKAGE);
	}

	public static EntitySelectionContext forVariableType() {
		return new EntitySelectionContext(null, EntitySelectionType.VARIABLE_TYPE);
	}
}
