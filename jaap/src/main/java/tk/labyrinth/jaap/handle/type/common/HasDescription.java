package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.model.declaration.TypeDescription;

public interface HasDescription {

	TypeDescription getDescription();
}
