package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;

public interface MayBeAnnotationTypeHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException when not an {@link AnnotationTypeHandle}
	 */
	AnnotationTypeHandle asAnnotationType();

	boolean isAnnotationType();
}
