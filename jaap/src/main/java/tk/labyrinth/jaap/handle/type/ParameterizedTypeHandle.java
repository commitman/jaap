package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.base.mixin.HasGenericContext;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.base.mixin.HasTypeMirror;
import tk.labyrinth.jaap.handle.element.common.ConvertibleToTypeElementTemplate;
import tk.labyrinth.jaap.handle.type.common.IsDeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.common.IsGenericTypeHandle;
import tk.labyrinth.jaap.handle.type.common.ConvertibleToRawTypeHandle;
import tk.labyrinth.jaap.handle.type.common.IsTypeHandle;

import java.util.stream.Stream;

public interface ParameterizedTypeHandle extends
		ConvertibleToRawTypeHandle,
		ConvertibleToTypeElementTemplate,
		HasGenericContext,
		HasProcessingContext,
		HasTypeMirror,
		IsDeclaredTypeHandle,
		IsGenericTypeHandle,
		IsTypeHandle {

	default TypeHandle getParameter(int index) {
		return getParameters().skip(index).findFirst().orElseThrow();
	}

	int getParameterCount();

	Stream<TypeHandle> getParameters();
}
