package tk.labyrinth.jaap.handle.element.common;

import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;

public interface ConvertibleToTypeParameterElementTemplate {

	TypeParameterElementTemplate toElement();
}
