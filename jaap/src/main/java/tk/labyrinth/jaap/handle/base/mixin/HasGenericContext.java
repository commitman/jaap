package tk.labyrinth.jaap.handle.base.mixin;

import tk.labyrinth.jaap.handle.base.GenericContext;

public interface HasGenericContext {

	GenericContext getGenericContext();
}
