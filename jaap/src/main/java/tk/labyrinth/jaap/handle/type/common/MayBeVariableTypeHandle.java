package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.VariableTypeHandle;

public interface MayBeVariableTypeHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException when not a {@link VariableTypeHandle}
	 */
	VariableTypeHandle asVariableType();

	boolean isVariableType();
}
