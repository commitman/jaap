package tk.labyrinth.jaap.handle.base.mixin;

public interface HasTemplate<T> {

	T getTemplate();
}
