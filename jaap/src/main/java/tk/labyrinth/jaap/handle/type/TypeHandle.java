package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.base.mixin.HasGenericContext;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.base.mixin.HasTypeMirror;
import tk.labyrinth.jaap.handle.type.common.HasDescription;
import tk.labyrinth.jaap.handle.type.common.MayBeAnnotationTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeDeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBePlainTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBePrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeRawTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeReferenceTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeVariableTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeWildcardTypeHandle;

/**
 * TypeHandle Hierarchy:<br>
 * * {@link TypeHandle} (abstract)<br>
 * - - {@link PrimitiveTypeHandle} (concrete)<br>
 * - - {@link ReferenceTypeHandle} (a)<br>
 * - - - {@link ArrayTypeHandle} (c)<br>
 * - - - {@link DeclaredTypeHandle} (a)<br>
 * - - - - {@link GenericTypeHandle} (a)<br>
 * - - - - - {@link ParameterizedTypeHandle} (c)<br>
 * - - - - - {@link RawTypeHandle} (c)<br>
 * - - - - {@link PlainTypeHandle} (c)<br>
 * - - - {@link VariableTypeHandle} (c)<br>
 * - - - {@link WildcardTypeHandle} (c)<br>
 * - - {@link VoidTypeHandle} (c)<br>
 */
public interface TypeHandle extends
		HasDescription,
		HasGenericContext,
		HasProcessingContext,
		HasTypeMirror,
		MayBeAnnotationTypeHandle,
		MayBeArrayTypeHandle,
		MayBeDeclaredTypeHandle,
		MayBeParameterizedTypeHandle,
		MayBePlainTypeHandle,
		MayBePrimitiveTypeHandle,
		MayBeRawTypeHandle,
		MayBeReferenceTypeHandle,
		MayBeVariableTypeHandle,
		MayBeWildcardTypeHandle {

	// FIXME: maybe create erasure interface
	// https://docs.oracle.com/javase/specs/jls/se8/html/jls-4.html#jls-4.6
	String getSignatureContributingString();

	/**
	 * @param subtype non-null
	 *
	 * @return whether <b>this</b> is assignable from <b>subtype</b>
	 */
	boolean isAssignableFrom(TypeHandle subtype);

	/**
	 * @param supertype non-null
	 *
	 * @return whether <b>this</b> is assignable to <b>supertype</b>
	 */
	boolean isAssignableTo(TypeHandle supertype);

	TypeHandle resolve(GenericContext genericContext);
}
