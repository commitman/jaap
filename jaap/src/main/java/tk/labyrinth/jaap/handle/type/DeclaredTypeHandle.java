package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.base.mixin.HasGenericContext;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.base.mixin.HasTypeMirror;
import tk.labyrinth.jaap.handle.element.common.ConvertibleToTypeElementTemplate;
import tk.labyrinth.jaap.handle.type.common.HasDescription;
import tk.labyrinth.jaap.handle.type.common.IsTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeAnnotationTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeGenericTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.common.MayBeRawTypeHandle;

public interface DeclaredTypeHandle extends
		ConvertibleToTypeElementTemplate,
		HasDescription,
		HasGenericContext,
		HasProcessingContext,
		HasTypeMirror,
		IsTypeHandle,
		MayBeAnnotationTypeHandle,
		MayBeGenericTypeHandle,
		MayBeParameterizedTypeHandle,
		MayBeRawTypeHandle {
	// empty
}
