package tk.labyrinth.jaap.handle.type.impl;

import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PlainTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.ReferenceTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.handle.type.WildcardTypeHandle;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import javax.lang.model.type.TypeMirror;

public class DecoratingTypeHandle implements TypeHandle {

	@Override
	public AnnotationTypeHandle asAnnotationType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ArrayTypeHandle asArrayType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public DeclaredTypeHandle asDeclaredType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ParameterizedTypeHandle asParameterizedType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public PlainTypeHandle asPlainType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public PrimitiveTypeHandle asPrimitiveType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public RawTypeHandle asRawType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ReferenceTypeHandle asReferenceType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public VariableTypeHandle asVariableType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public WildcardTypeHandle asWildcardType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeDescription getDescription() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public GenericContext getGenericContext() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ProcessingContext getProcessingContext() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String getSignatureContributingString() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeMirror getTypeMirror() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isAnnotationType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isArrayType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isAssignableFrom(TypeHandle subtype) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isAssignableTo(TypeHandle supertype) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isDeclaredType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isParameterizedType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isPlainType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isPrimitiveType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isRawType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isReferenceType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isVariableType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isWildcardType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeHandle resolve(GenericContext genericContext) {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
