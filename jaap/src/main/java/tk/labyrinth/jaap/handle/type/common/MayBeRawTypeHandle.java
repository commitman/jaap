package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.RawTypeHandle;

public interface MayBeRawTypeHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException when not a {@link RawTypeHandle}
	 */
	RawTypeHandle asRawType();

	boolean isRawType();
}
