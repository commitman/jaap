package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.RawTypeHandle;

public interface ConvertibleToRawTypeHandle {

	RawTypeHandle toRawType();
}
