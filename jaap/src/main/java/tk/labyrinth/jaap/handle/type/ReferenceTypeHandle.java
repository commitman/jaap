package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.base.mixin.HasTypeMirror;

public interface ReferenceTypeHandle extends HasProcessingContext, HasTypeMirror {

	TypeHandle asType();
}
