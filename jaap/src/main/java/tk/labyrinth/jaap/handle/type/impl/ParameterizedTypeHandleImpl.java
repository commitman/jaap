package tk.labyrinth.jaap.handle.type.impl;

import lombok.Getter;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.langmodel.type.util.DeclaredTypeUtils;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import java.util.Objects;
import java.util.stream.Stream;

public class ParameterizedTypeHandleImpl implements ParameterizedTypeHandle {

	@Getter
	private final DeclaredType declaredType;

	@Getter
	private final GenericContext genericContext;

	@Getter
	private final ProcessingContext processingContext;

	public ParameterizedTypeHandleImpl(
			DeclaredType declaredType,
			GenericContext genericContext,
			ProcessingContext processingContext) {
		this.declaredType = DeclaredTypeUtils.requireParameterized(declaredType);
		this.genericContext = Objects.requireNonNull(genericContext, "genericContext");
		this.processingContext = Objects.requireNonNull(processingContext, "processingContext");
	}

	@Override
	public DeclaredTypeHandle asDeclaredType() {
		return processingContext.getDeclaredTypeHandle(genericContext, declaredType);
	}

	@Override
	public GenericTypeHandle asGenericType() {
		return processingContext.getGenericTypeHandle(genericContext, declaredType);
	}

	@Override
	public TypeHandle asType() {
		return processingContext.getTypeHandle(genericContext, declaredType);
	}

	@Override
	public int getParameterCount() {
		return declaredType.getTypeArguments().size();
	}

	@Override
	public Stream<TypeHandle> getParameters() {
		return declaredType.getTypeArguments().stream()
				.map(typeArgument -> processingContext.getTypeHandle(genericContext, typeArgument));
	}

	@Override
	public TypeMirror getTypeMirror() {
		return declaredType;
	}

	@Override
	public TypeElementTemplate toElement() {
		return processingContext.getTypeElementTemplate(declaredType);
	}

	@Override
	public RawTypeHandle toRawType() {
		return processingContext.getRawTypeHandle(DeclaredTypeUtils.toRaw(
				processingContext.getProcessingEnvironment(),
				declaredType));
	}

	@Override
	public String toString() {
		return declaredType.toString();
	}
}
