package tk.labyrinth.jaap.handle.type.impl;

import lombok.Getter;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;

import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import java.util.Objects;

public class PrimitiveTypeHandleImpl implements PrimitiveTypeHandle {

	@Getter
	private final PrimitiveType primitiveType;

	@Getter
	private final ProcessingContext processingContext;

	public PrimitiveTypeHandleImpl(PrimitiveType primitiveType, ProcessingContext processingContext) {
		this.primitiveType = Objects.requireNonNull(primitiveType, "primitiveType");
		this.processingContext = Objects.requireNonNull(processingContext, "processingContext");
	}

	@Override
	public TypeHandle asType() {
		return getProcessingContext().getTypeHandle(GenericContext.empty(), primitiveType);
	}

	@Override
	public TypeMirror getTypeMirror() {
		return primitiveType;
	}

	@Override
	public String toString() {
		return primitiveType.toString();
	}
}
