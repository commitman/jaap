package tk.labyrinth.jaap.handle.element.common;

import tk.labyrinth.jaap.template.element.TypeElementTemplate;

public interface ConvertibleToTypeElementTemplate {

	TypeElementTemplate toElement();
}
