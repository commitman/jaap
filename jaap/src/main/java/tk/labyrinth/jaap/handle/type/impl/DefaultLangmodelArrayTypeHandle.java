package tk.labyrinth.jaap.handle.type.impl;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;

import javax.lang.model.type.ArrayType;

@RequiredArgsConstructor
public class DefaultLangmodelArrayTypeHandle implements ArrayTypeHandle {

	private final ArrayType arrayType;

	private final ProcessingContext processingContext;

	@Override
	public TypeHandle asType() {
		return processingContext.getTypeHandle(GenericContext.empty(), arrayType);
	}

	@Override
	public TypeHandle getComponentType() {
		return processingContext.getTypeHandle(GenericContext.empty(), arrayType.getComponentType());
	}
}
