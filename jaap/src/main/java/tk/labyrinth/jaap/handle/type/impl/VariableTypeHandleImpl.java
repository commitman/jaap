package tk.labyrinth.jaap.handle.type.impl;

import lombok.Getter;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;

import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.util.Objects;

public class VariableTypeHandleImpl implements VariableTypeHandle {

	@Getter
	private final GenericContext genericContext;

	@Getter
	private final ProcessingContext processingContext;

	@Getter
	private final TypeVariable typeVariable;

	public VariableTypeHandleImpl(
			GenericContext genericContext,
			ProcessingContext processingContext,
			TypeVariable typeVariable) {
		this.genericContext = Objects.requireNonNull(genericContext, "genericContext");
		this.processingContext = Objects.requireNonNull(processingContext, "processingContext");
		this.typeVariable = Objects.requireNonNull(typeVariable, "typeVariable");
	}

	@Override
	public TypeHandle asType() {
		return getProcessingContext().getTypeHandle(genericContext, typeVariable);
	}

	@Override
	public TypeMirror getTypeMirror() {
		return typeVariable;
	}

	@Override
	public TypeParameterElementTemplate toElement() {
		return processingContext.getTypeParameterElementTemplate((TypeParameterElement) typeVariable.asElement());
	}
}
