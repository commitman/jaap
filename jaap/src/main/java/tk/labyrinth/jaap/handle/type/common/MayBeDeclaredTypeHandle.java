package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;

public interface MayBeDeclaredTypeHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException when not a {@link DeclaredTypeHandle}
	 */
	DeclaredTypeHandle asDeclaredType();

	boolean isDeclaredType();
}
