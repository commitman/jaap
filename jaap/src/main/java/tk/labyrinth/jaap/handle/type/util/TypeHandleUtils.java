package tk.labyrinth.jaap.handle.type.util;

import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;

import javax.annotation.Nullable;

public class TypeHandleUtils {

	@Nullable
	public static ElementTemplate selectMember(TypeHandle typeHandle, EntitySelector entitySelector) {
		ElementTemplate result;
		if (typeHandle.isReferenceType()) {
			if (typeHandle.isDeclaredType()) {
				result = typeHandle.asDeclaredType().toElement().selectMember(entitySelector);
			} else {
				// TODO: Implement other type's members.
				throw new NotImplementedException();
			}
		} else {
			result = null;
		}
		return result;
	}
}
