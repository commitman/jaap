package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;

public interface IsDeclaredTypeHandle {

	DeclaredTypeHandle asDeclaredType();
}
