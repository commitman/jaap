package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import javax.annotation.Nullable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;

public interface TypeHandleFactory {

	@Nullable
	TypeHandle find(String typeDescriptionString);

	@Nullable
	TypeHandle find(TypeDescription typeDescription);

	@Nullable
	DeclaredTypeHandle findDeclared(String typeDescriptionString);

	@Nullable
	DeclaredTypeHandle findDeclared(TypeDescription typeDescription);

	default TypeHandle get(Class<?> type) {
		return get(GenericContext.empty(), type);
	}

	TypeHandle get(GenericContext genericContext, Class<?> type);

	TypeHandle get(GenericContext genericContext, TypeElement typeElement);

	TypeHandle get(GenericContext genericContext, TypeMirror typeMirror);

	ArrayTypeHandle getArray(GenericContext genericContext, TypeMirror typeMirror);

	DeclaredTypeHandle getDeclared(GenericContext genericContext, DeclaredType declaredType);

	DeclaredTypeHandle getDeclared(GenericContext genericContext, TypeElement typeElement);

	DeclaredTypeHandle getDeclared(GenericContext genericContext, TypeMirror typeMirror);

	GenericTypeHandle getGeneric(GenericContext genericContext, DeclaredType declaredType);

	GenericTypeHandle getGeneric(GenericContext genericContext, TypeMirror typeMirror);

	PrimitiveTypeHandle getPrimitive(PrimitiveType primitiveType);

	PrimitiveTypeHandle getPrimitive(TypeMirror typeMirror);

	RawTypeHandle getRaw(DeclaredType declaredType);

	RawTypeHandle getRaw(TypeMirror typeMirror);

	VariableTypeHandle getVariable(GenericContext genericContext, TypeMirror typeMirror);

	VariableTypeHandle getVariable(GenericContext genericContext, TypeVariable typeVariable);
}
