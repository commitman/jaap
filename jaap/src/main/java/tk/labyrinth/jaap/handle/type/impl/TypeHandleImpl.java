package tk.labyrinth.jaap.handle.type.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.PlainTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.ReferenceTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.handle.type.WildcardTypeHandle;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.util.TypeMirrorUtils;

import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;

@RequiredArgsConstructor
public class TypeHandleImpl implements TypeHandle {

	@Getter
	private final GenericContext genericContext;

	@Getter
	private final ProcessingContext processingContext;

	// TODO: Ensure only proper TypeMirrors comes here.
	@Getter
	private final TypeMirror typeMirror;

	@Override
	public AnnotationTypeHandle asAnnotationType() {
		return processingContext.getAnnotationTypeHandle(typeMirror);
	}

	@Override
	public ArrayTypeHandle asArrayType() {
		return processingContext.getArrayTypeHandle(genericContext, typeMirror);
	}

	@Override
	public DeclaredTypeHandle asDeclaredType() {
		return processingContext.getDeclaredTypeHandle(genericContext, typeMirror);
	}

	@Override
	public ParameterizedTypeHandle asParameterizedType() {
		return processingContext.getParameterizedTypeHandle(genericContext, typeMirror);
	}

	@Override
	public PlainTypeHandle asPlainType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public PrimitiveTypeHandle asPrimitiveType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public RawTypeHandle asRawType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ReferenceTypeHandle asReferenceType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public VariableTypeHandle asVariableType() {
		return processingContext.getVariableTypeHandle(genericContext, typeMirror);
	}

	@Override
	public WildcardTypeHandle asWildcardType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		TypeHandleImpl other = (TypeHandleImpl) obj;
		return TypeMirrorUtils.compareConsideringPrimitives(typeMirror, other.typeMirror) == 0;
	}

	@Override
	public TypeDescription getDescription() {
		return TypeMirrorUtils.toDescription(processingContext.getProcessingEnvironment(), typeMirror);
	}

	@Override
	public String getSignatureContributingString() {
		// FIXME: check resolving for <T extends List<String> & Serializable>
		return TypeMirrorUtils.getSignatureContributingString(TypeMirrorUtils.erasure(
				processingContext.getProcessingEnvironment(),
				typeMirror));
	}

	@Override
	public int hashCode() {
		return typeMirror.toString().hashCode();
	}

	@Override
	public boolean isAnnotationType() {
		return isDeclaredType() && asDeclaredType().isAnnotationType();
	}

	@Override
	public boolean isArrayType() {
		return TypeMirrorUtils.isArrayType(typeMirror);
	}

	@Override
	public boolean isAssignableFrom(TypeHandle subtype) {
		return subtype.isAssignableTo(this);
	}

	@Override
	public boolean isAssignableTo(TypeHandle supertype) {
		return getProcessingContext().getProcessingEnvironment().getTypeUtils()
				.isAssignable(typeMirror, supertype.getTypeMirror());
	}

	@Override
	public boolean isDeclaredType() {
		return TypeMirrorUtils.isDeclaredType(typeMirror);
	}

	@Override
	public boolean isParameterizedType() {
		boolean result;
		if (TypeMirrorUtils.isDeclaredType(typeMirror)) {
			result = TypeMirrorUtils.requireDeclaredType(typeMirror).getTypeArguments().size() > 0;
		} else {
			result = false;
		}
		return result;
	}

	@Override
	public boolean isPlainType() {
		boolean result;
		if (TypeMirrorUtils.isDeclaredType(typeMirror)) {
			result = processingContext.getTypeElementTemplate(TypeMirrorUtils.requireDeclaredType(typeMirror))
					.getTypeParameterCount() == 0;
		} else {
			result = false;
		}
		return result;
	}

	@Override
	public boolean isPrimitiveType() {
		return TypeMirrorUtils.isPrimitiveType(typeMirror);
	}

	@Override
	public boolean isRawType() {
		boolean result;
		if (TypeMirrorUtils.isDeclaredType(typeMirror)) {
			DeclaredType declaredType = TypeMirrorUtils.requireDeclaredType(typeMirror);
			result = declaredType.getTypeArguments().size() == 0 &&
					processingContext.getTypeElementTemplate(declaredType.asElement()).getTypeParameterCount() > 0;
		} else {
			result = false;
		}
		return result;
	}

	@Override
	public boolean isReferenceType() {
		return TypeMirrorUtils.isReferenceType(typeMirror);
	}

	@Override
	public boolean isVariableType() {
		return TypeMirrorUtils.isTypeVariable(typeMirror);
	}

	@Override
	public boolean isWildcardType() {
		return TypeMirrorUtils.isWildcardType(typeMirror);
	}

	@Override
	public TypeHandle resolve(GenericContext genericContext) {
		return processingContext.getTypeHandle(genericContext, typeMirror);
	}

	@Override
	public String toString() {
		// TODO: VariableType variant must write proper Variable name.
		return typeMirror.toString();
	}
}
