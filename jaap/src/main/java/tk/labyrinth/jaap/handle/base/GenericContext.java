package tk.labyrinth.jaap.handle.base;

import lombok.Value;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;

import java.util.Map;

@Value
public class GenericContext {

	Map<TypeParameterElementTemplate, TypeHandle> typeParameterMappings;

	public boolean isEmpty() {
		return typeParameterMappings.isEmpty();
	}

	public static GenericContext empty() {
		return new GenericContext(Map.of());
	}

	public static GenericContext withMappings(Map<TypeParameterElementTemplate, TypeHandle> typeParameterMappings) {
		return new GenericContext(Map.copyOf(typeParameterMappings));
	}
}
