package tk.labyrinth.jaap.handle.base.mixin;

import javax.lang.model.type.TypeMirror;

public interface HasTypeMirror {

	/**
	 * @return non-null
	 */
	TypeMirror getTypeMirror();
}
