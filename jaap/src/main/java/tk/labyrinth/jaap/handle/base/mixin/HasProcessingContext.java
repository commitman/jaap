package tk.labyrinth.jaap.handle.base.mixin;

import tk.labyrinth.jaap.context.ProcessingContext;

public interface HasProcessingContext {

	ProcessingContext getProcessingContext();
}
