package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.base.mixin.HasGenericContext;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.base.mixin.HasTypeMirror;
import tk.labyrinth.jaap.handle.element.common.ConvertibleToTypeParameterElementTemplate;
import tk.labyrinth.jaap.handle.type.common.IsTypeHandle;

public interface VariableTypeHandle extends
		ConvertibleToTypeParameterElementTemplate,
		HasGenericContext,
		HasProcessingContext,
		HasTypeMirror,
		IsTypeHandle {
	// empty
}
