package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;

public interface MayBeArrayTypeHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException when not an {@link ArrayTypeHandle}
	 */
	ArrayTypeHandle asArrayType();

	boolean isArrayType();
}
