package tk.labyrinth.jaap.handle.type.common;

import tk.labyrinth.jaap.handle.type.GenericTypeHandle;

public interface MayBeGenericTypeHandle {

	/**
	 * @return non-null
	 *
	 * @throws IllegalStateException when not a {@link GenericTypeHandle}
	 */
	GenericTypeHandle asGenericType();

	boolean isGenericType();
}
