package tk.labyrinth.jaap.handle.type.impl;

import lombok.Getter;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.langmodel.type.util.DeclaredTypeUtils;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import java.util.Objects;

public class DeclaredTypeHandleImpl implements DeclaredTypeHandle {

	@Getter
	private final GenericContext genericContext;

	@Getter
	private final ProcessingContext processingContext;

	@Getter
	private final DeclaredType declaredType;

	public DeclaredTypeHandleImpl(
			DeclaredType declaredType,
			GenericContext genericContext,
			ProcessingContext processingContext) {
		this.declaredType = Objects.requireNonNull(declaredType, "declaredType");
		this.genericContext = Objects.requireNonNull(genericContext, "genericContext");
		this.processingContext = Objects.requireNonNull(processingContext, "processingContext");
	}

	@Override
	public AnnotationTypeHandle asAnnotationType() {
		return processingContext.getAnnotationTypeHandle(declaredType);
	}

	@Override
	public GenericTypeHandle asGenericType() {
		return processingContext.getGenericTypeHandle(genericContext, declaredType);
	}

	@Override
	public ParameterizedTypeHandle asParameterizedType() {
		return processingContext.getParameterizedTypeHandle(genericContext, declaredType);
	}

	@Override
	public RawTypeHandle asRawType() {
		return processingContext.getRawTypeHandle(declaredType);
	}

	@Override
	public TypeHandle asType() {
		return processingContext.getTypeHandle(genericContext, declaredType);
	}

	@Override
	public TypeDescription getDescription() {
		return DeclaredTypeUtils.toDescription(processingContext.getProcessingEnvironment(), declaredType);
	}

	@Override
	public TypeMirror getTypeMirror() {
		return declaredType;
	}

	@Override
	public boolean isAnnotationType() {
		return DeclaredTypeUtils.isAnnotation(declaredType);
	}

	@Override
	public boolean isGenericType() {
		return DeclaredTypeUtils.isGeneric(declaredType);
	}

	@Override
	public boolean isParameterizedType() {
		return DeclaredTypeUtils.isParameterized(declaredType);
	}

	@Override
	public boolean isRawType() {
		return DeclaredTypeUtils.isRaw(declaredType);
	}

	@Override
	public TypeElementTemplate toElement() {
		return processingContext.getTypeElementTemplate(declaredType);
	}

	@Override
	public String toString() {
		return getDescription().toString();
	}
}
