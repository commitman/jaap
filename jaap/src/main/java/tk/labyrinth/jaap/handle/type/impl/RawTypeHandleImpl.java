package tk.labyrinth.jaap.handle.type.impl;

import lombok.Getter;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.langmodel.type.util.DeclaredTypeUtils;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import java.util.Objects;

public class RawTypeHandleImpl implements RawTypeHandle {

	@Getter
	private final DeclaredType declaredType;

	@Getter
	private final ProcessingContext processingContext;

	public RawTypeHandleImpl(
			DeclaredType declaredType,
			ProcessingContext processingContext) {
		this.declaredType = DeclaredTypeUtils.requireRaw(declaredType);
		this.processingContext = Objects.requireNonNull(processingContext, "processingContext");
	}

	@Override
	public GenericTypeHandle asGenericType() {
		return getProcessingContext().getGenericTypeHandle(GenericContext.empty(), declaredType);
	}

	@Override
	public TypeHandle asType() {
		return getProcessingContext().getTypeHandle(GenericContext.empty(), declaredType);
	}

	@Override
	public TypeMirror getTypeMirror() {
		return declaredType;
	}

	@Override
	public TypeElementTemplate toElement() {
		return processingContext.getTypeElementTemplate(declaredType);
	}

	@Override
	public String toString() {
		return declaredType.toString();
	}
}
