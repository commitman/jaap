package tk.labyrinth.jaap.handle.type;

import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.base.mixin.HasTypeMirror;
import tk.labyrinth.jaap.handle.element.common.ConvertibleToTypeElementTemplate;
import tk.labyrinth.jaap.handle.type.common.IsGenericTypeHandle;
import tk.labyrinth.jaap.handle.type.common.IsTypeHandle;

public interface RawTypeHandle extends
		ConvertibleToTypeElementTemplate,
		HasProcessingContext,
		HasTypeMirror,
		IsGenericTypeHandle,
		IsTypeHandle {
	// empty
}
