package tk.labyrinth.jaap.handle.type.impl;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.context.ProcessingContextAware;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ArrayTypeHandle;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.GenericTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.RawTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandleFactory;
import tk.labyrinth.jaap.handle.type.VariableTypeHandle;
import tk.labyrinth.jaap.langmodel.TypeMirrorFactory;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.util.TypeMirrorUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class AnnprocTypeHandleFactory implements ProcessingContextAware, TypeHandleFactory {

	private final TypeMirrorFactory typeMirrorFactory;

	private ProcessingContext processingContext;

	@Override
	public void acceptProcessingContext(ProcessingContext processingContext) {
		this.processingContext = processingContext;
	}

	@Nullable
	@Override
	public TypeHandle find(String typeDescriptionString) {
		return find(TypeDescription.of(typeDescriptionString));
	}

	@Nullable
	@Override
	public TypeHandle find(TypeDescription typeDescription) {
		return new TypeHandleImpl(GenericContext.empty(), processingContext, typeMirrorFactory.find(typeDescription));
	}

	@Nullable
	@Override
	public DeclaredTypeHandle findDeclared(String typeDescriptionString) {
		return findDeclared(TypeDescription.of(typeDescriptionString));
	}

	@Nullable
	@Override
	public DeclaredTypeHandle findDeclared(TypeDescription typeDescription) {
		DeclaredTypeHandle result;
		{
			TypeMirror typeMirror = typeMirrorFactory.find(typeDescription);
			if (typeMirror != null) {
				result = new DeclaredTypeHandleImpl(
						TypeMirrorUtils.requireDeclaredType(typeMirror),
						GenericContext.empty(),
						processingContext);
			} else {
				result = null;
			}
		}
		return result;
	}

	@Override
	public TypeHandle get(GenericContext genericContext, Class<?> type) {
		return new TypeHandleImpl(genericContext, processingContext, typeMirrorFactory.get(type));
	}

	@Override
	public TypeHandle get(GenericContext genericContext, TypeElement typeElement) {
		return get(genericContext, typeElement.asType());
	}

	@Override
	public TypeHandle get(GenericContext genericContext, TypeMirror typeMirror) {
		TypeMirror resolvedTypeMirror;
		if (!genericContext.isEmpty()) {
			resolvedTypeMirror = TypeMirrorUtils.resolve(
					processingContext.getProcessingEnvironment(),
					genericContext.getTypeParameterMappings().entrySet().stream()
							.collect(Collectors.toMap(
									entry -> entry.getKey().getTypeParameterElement(),
									entry -> entry.getValue().getTypeMirror())),
					typeMirror);
		} else {
			resolvedTypeMirror = typeMirror;
		}
		return new TypeHandleImpl(genericContext, processingContext, resolvedTypeMirror);
	}

	@Override
	public ArrayTypeHandle getArray(GenericContext genericContext, TypeMirror typeMirror) {
		return new DefaultLangmodelArrayTypeHandle(TypeMirrorUtils.requireArrayType(typeMirror), processingContext);
	}

	@Override
	public DeclaredTypeHandle getDeclared(GenericContext genericContext, DeclaredType declaredType) {
		return new DeclaredTypeHandleImpl(declaredType, genericContext, processingContext);
	}

	@Override
	public DeclaredTypeHandle getDeclared(GenericContext genericContext, TypeElement typeElement) {
		return getDeclared(genericContext, TypeMirrorUtils.requireDeclaredType(typeElement.asType()));
	}

	@Override
	public DeclaredTypeHandle getDeclared(GenericContext genericContext, TypeMirror typeMirror) {
		return getDeclared(genericContext, TypeMirrorUtils.requireDeclaredType(typeMirror));
	}

	@Override
	public GenericTypeHandle getGeneric(GenericContext genericContext, DeclaredType declaredType) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public GenericTypeHandle getGeneric(GenericContext genericContext, TypeMirror typeMirror) {
		return getGeneric(genericContext, TypeMirrorUtils.requireDeclaredType(typeMirror));
	}

	@Override
	public PrimitiveTypeHandle getPrimitive(PrimitiveType primitiveType) {
		return new PrimitiveTypeHandleImpl(primitiveType, processingContext);
	}

	@Override
	public PrimitiveTypeHandle getPrimitive(TypeMirror typeMirror) {
		return getPrimitive(TypeMirrorUtils.requirePrimitive(typeMirror));
	}

	@Override
	public RawTypeHandle getRaw(DeclaredType declaredType) {
		return new RawTypeHandleImpl(declaredType, processingContext);
	}

	@Override
	public RawTypeHandle getRaw(TypeMirror typeMirror) {
		return getRaw(TypeMirrorUtils.requireDeclaredType(typeMirror));
	}

	@Override
	public VariableTypeHandle getVariable(GenericContext genericContext, TypeMirror typeMirror) {
		return getVariable(genericContext, TypeMirrorUtils.requireTypeVariable(typeMirror));
	}

	@Override
	public VariableTypeHandle getVariable(GenericContext genericContext, TypeVariable typeVariable) {
		return new VariableTypeHandleImpl(genericContext, processingContext, typeVariable);
	}
}
