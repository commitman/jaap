package tk.labyrinth.jaap.typical.core;

import java.util.stream.Stream;

public interface TypeDescriptor<T, D extends TypeDescriptor<T, D>> {

	@Override
	boolean equals(Object obj);

	Stream<D> getDeclaredTypes();

	T getType();

	@Override
	int hashCode();
}
