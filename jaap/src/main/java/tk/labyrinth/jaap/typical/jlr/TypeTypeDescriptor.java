package tk.labyrinth.jaap.typical.jlr;

import lombok.Value;
import tk.labyrinth.jaap.typical.core.TypeDescriptor;

import java.lang.reflect.Type;
import java.util.stream.Stream;

@Value
public class TypeTypeDescriptor implements TypeDescriptor<Type, TypeTypeDescriptor> {

	Type type;

	@Override
	public Stream<TypeTypeDescriptor> getDeclaredTypes() {
		return InternalJlrUtils.getDeclaredTypes(type).stream()
				.map(TypeTypeDescriptor::new);
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public String toString() {
		return type.toString();
	}
}
