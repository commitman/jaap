package tk.labyrinth.jaap.typical.core;

import java.util.stream.Stream;

public class TypicalCoreUtils {

	public static <T, D extends TypeDescriptor<T, D>> Stream<D> getDeclaredTypes(D typeDescriptor) {
		return typeDescriptor.getDeclaredTypes();
	}
}
