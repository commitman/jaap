package tk.labyrinth.jaap.typical.jlm;

import tk.labyrinth.misc4j2.collectoin.StreamUtils;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.IntersectionType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InternalJlmUtils {

	private static Stream<TypeMirror> doGetDeclaredTypes(ProcessingEnvironment processingEnvironment, ArrayType arrayType) {
		// FIXME: Not sure if we need erasure for array.
		ArrayType erasure = (ArrayType) processingEnvironment.getTypeUtils().erasure(arrayType);
		return StreamUtils.concat(
				erasure,
				processingEnvironment.getTypeUtils().directSupertypes(erasure).stream()
						.flatMap(directSupertype -> doGetDeclaredTypes(processingEnvironment, directSupertype)));
	}

	private static Stream<TypeMirror> doGetDeclaredTypes(ProcessingEnvironment processingEnvironment, DeclaredType declaredType) {
		DeclaredType erasure = (DeclaredType) processingEnvironment.getTypeUtils().erasure(declaredType);
		return StreamUtils.concat(
				erasure,
				processingEnvironment.getTypeUtils().directSupertypes(erasure).stream()
						.flatMap(directSupertype -> doGetDeclaredTypes(processingEnvironment, directSupertype)));
	}

	private static Stream<TypeMirror> doGetDeclaredTypes(NoType noType) {
		return Stream.of(noType);
	}

	private static Stream<TypeMirror> doGetDeclaredTypes(PrimitiveType primitiveType) {
		return Stream.of(primitiveType);
	}

	private static Stream<TypeMirror> doGetDeclaredTypes(ProcessingEnvironment processingEnvironment, IntersectionType intersectionType) {
		return intersectionType.getBounds().stream()
				.flatMap(bound -> doGetDeclaredTypes(processingEnvironment, bound));
	}

	private static Stream<TypeMirror> doGetDeclaredTypes(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		processingEnvironment.getTypeUtils().directSupertypes(typeMirror);
		Stream<TypeMirror> result;
		if (typeMirror instanceof ArrayType) {
			result = doGetDeclaredTypes(processingEnvironment, (ArrayType) typeMirror);
		} else if (typeMirror instanceof IntersectionType) {
			// IntersectionType goes before DeclaredType because its implementation also implements DeclaredType.
			result = doGetDeclaredTypes(processingEnvironment, (IntersectionType) typeMirror);
		} else if (typeMirror instanceof DeclaredType) {
			result = doGetDeclaredTypes(processingEnvironment, (DeclaredType) typeMirror);
		} else if (typeMirror instanceof NoType) {
			result = doGetDeclaredTypes((NoType) typeMirror);
		} else if (typeMirror instanceof PrimitiveType) {
			result = doGetDeclaredTypes((PrimitiveType) typeMirror);
		} else if (typeMirror instanceof TypeVariable) {
			result = doGetDeclaredTypes(processingEnvironment, (TypeVariable) typeMirror);
		} else {
			throw new NotImplementedException(ExceptionUtils.render(typeMirror));
		}
		return result;
	}

	private static Stream<TypeMirror> doGetDeclaredTypes(ProcessingEnvironment processingEnvironment, TypeVariable typeVariable) {
		return doGetDeclaredTypes(processingEnvironment, typeVariable.getUpperBound());
	}

	public static Set<TypeMirror> getDeclaredTypes(ProcessingEnvironment processingEnvironment, TypeMirror typeMirror) {
		Set<TypeMirror> result = doGetDeclaredTypes(processingEnvironment, typeMirror).collect(Collectors.toSet());
//		result.add(processingEnvironment.getElementUtils().getTypeElement(Object.class.getCanonicalName()).asType());
		return result;
	}
}
