package tk.labyrinth.jaap.typical.jlr;

import tk.labyrinth.jaap.typical.core.TypicalCoreUtils;

import java.lang.reflect.Type;
import java.util.stream.Stream;

public class TypicalJlrUtils {

	public static Stream<Type> getDeclaredTypes(Type type) {
		return TypicalCoreUtils.getDeclaredTypes(new TypeTypeDescriptor(type))
				.map(TypeTypeDescriptor::getType);
	}
}
