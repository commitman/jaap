package tk.labyrinth.jaap.template.element.impl;

import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.template.element.InitializerElementTemplate;
import tk.labyrinth.jaap.template.element.util.ExecutableElementUtils;

import javax.lang.model.element.ExecutableElement;

public class InitializerElementTemplateImpl extends ExecutableElementTemplateImpl implements InitializerElementTemplate {

	public InitializerElementTemplateImpl(ProcessingContext processingContext, ExecutableElement executableElement) {
		super(processingContext, ExecutableElementUtils.requireInitializer(executableElement));
	}

	@Override
	public String getSignatureString() {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
