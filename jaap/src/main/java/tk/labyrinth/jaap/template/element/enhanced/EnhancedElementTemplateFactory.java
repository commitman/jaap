package tk.labyrinth.jaap.template.element.enhanced;

import tk.labyrinth.jaap.langmodel.ElementFactory;
import tk.labyrinth.jaap.langmodel.TypeMirrorFactory;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.template.element.AnnprocElementTemplateFactoryImpl;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.synthetic.SyntheticElementTemplateRegistry;

import javax.annotation.Nullable;
import javax.lang.model.element.TypeElement;

public class EnhancedElementTemplateFactory extends AnnprocElementTemplateFactoryImpl {

	private final SyntheticElementTemplateRegistry syntheticElementTemplateRegistry;

	public EnhancedElementTemplateFactory(
			ElementFactory elementFactory,
			TypeMirrorFactory typeMirrorFactory,
			SyntheticElementTemplateRegistry syntheticElementTemplateRegistry) {
		super(elementFactory, typeMirrorFactory);
		this.syntheticElementTemplateRegistry = syntheticElementTemplateRegistry;
	}

	@Nullable
	@Override
	public MethodElementTemplate findMethod(MethodFullSignature methodFullSignature) {
		MethodElementTemplate result;
		{
			MethodElementTemplate annprocTemplate = super.findMethod(methodFullSignature);
			if (annprocTemplate != null) {
				result = annprocTemplate;
			} else {
				result = syntheticElementTemplateRegistry.findMethod(methodFullSignature);
			}
		}
		return result;
	}

	@Override
	public TypeElementTemplate getType(TypeElement typeElement) {
		return new EnhancedTypeElementTemplate(getProcessingContext(), syntheticElementTemplateRegistry, typeElement);
	}
}
