package tk.labyrinth.jaap.template.element.util;

import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.jaap.util.TypeMirrorUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodSignatureUtils {

	public static MethodFullSignature createFull(
			ProcessingEnvironment processingEnvironment,
			Class<?> type,
			String methodSimpleSignatureString) {
		return createFull(
				processingEnvironment,
				// FIXME: Add method of(Class,String).
				MethodFullSignature.of(type.getCanonicalName() + "#" + methodSimpleSignatureString));
	}

	public static MethodFullSignature createFull(
			ProcessingEnvironment processingEnvironment,
			ExecutableElement executableElement) {
		Objects.requireNonNull(executableElement, "executableElement");
		//
		return createFull(
				processingEnvironment,
				(TypeElement) executableElement.getEnclosingElement(),
				executableElement.getSimpleName().toString(),
				executableElement.getParameters().stream()
						.map(VariableElement::asType)
						.collect(Collectors.toList()));
	}

	public static MethodFullSignature createFull(
			ProcessingEnvironment processingEnvironment,
			MethodFullSignature methodFullSignature) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(methodFullSignature, "methodFullSignature");
		//
		return createFull(
				processingEnvironment,
				TypeElementUtils.get(processingEnvironment, methodFullSignature.getTypeFullSignature()),
				methodFullSignature.getName(),
				methodFullSignature.getParameters().stream()
						.map(parameterTypeName -> TypeMirrorUtils.resolve(processingEnvironment, parameterTypeName))
						.collect(Collectors.toList()));
	}

	public static MethodFullSignature createFull(
			ProcessingEnvironment processingEnvironment,
			String methodFullSignature) {
		return createFull(processingEnvironment, MethodFullSignature.of(methodFullSignature));
	}

	public static MethodFullSignature createFull(
			ProcessingEnvironment processingEnvironment,
			TypeElement typeElement,
			MethodFullSignature methodFullSignature) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(typeElement, "typeElement");
		Objects.requireNonNull(methodFullSignature, "methodFullSignature");
		//
		return createFull(
				processingEnvironment,
				typeElement,
				methodFullSignature.getName(),
				methodFullSignature.getParameters().stream()
						.map(parameterTypeName -> TypeMirrorUtils.resolve(processingEnvironment, parameterTypeName))
						.collect(Collectors.toList()));
	}

	public static MethodFullSignature createFull(
			TypeElement typeElement,
			String methodSimpleSignature) {
		Objects.requireNonNull(typeElement, "typeElement");
		Objects.requireNonNull(methodSimpleSignature, "methodSimpleSignature");
		//
		return createFull(typeElement, MethodSimpleSignature.of(methodSimpleSignature));
	}

	public static MethodFullSignature createFull(
			ProcessingEnvironment processingEnvironment,
			TypeElement typeElement,
			String simpleName,
			List<TypeMirror> parameterTypes) {
		Objects.requireNonNull(typeElement, "typeElement");
		Objects.requireNonNull(simpleName, "simpleName");
		Objects.requireNonNull(parameterTypes, "parameterTypes");
		//
		return createFull(typeElement, MethodSignatureUtils.createSimple(processingEnvironment, simpleName,
				TypeMirrorUtils.erasures(processingEnvironment, parameterTypes)));
	}

	public static MethodFullSignature createFull(
			TypeElement typeElement,
			MethodSimpleSignature methodSimpleSignature) {
		Objects.requireNonNull(typeElement, "typeElement");
		Objects.requireNonNull(methodSimpleSignature, "methodSimpleSignature");
		//
		return new MethodFullSignature(
				methodSimpleSignature,
				typeElement.getQualifiedName().toString());
	}

	public static MethodSimpleSignature createSimple(
			ProcessingEnvironment processingEnvironment,
			ExecutableElement executableElement) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(executableElement, "executableElement");
		//
		return createSimple(
				processingEnvironment,
				executableElement.getSimpleName().toString(),
				executableElement.getParameters().stream()
						.map(VariableElement::asType)
						.collect(Collectors.toList()));
	}

	public static MethodSimpleSignature createSimple(
			ProcessingEnvironment processingEnvironment,
			MethodFullSignature methodFullSignature) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(methodFullSignature, "methodFullSignature");
		//
		return createSimple(
				processingEnvironment,
				methodFullSignature.getName(),
				methodFullSignature.getParameters().stream()
						.map(parameterTypeName -> TypeMirrorUtils.resolve(processingEnvironment, parameterTypeName))
						.collect(Collectors.toList()));
	}

	public static MethodSimpleSignature createSimple(
			ProcessingEnvironment processingEnvironment,
			String methodSimpleName,
			List<TypeMirror> parameterTypes) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(methodSimpleName, "methodSimpleName");
		Objects.requireNonNull(parameterTypes, "parameterTypes");
		//
		return createSimple(methodSimpleName, TypeMirrorUtils.erasures(processingEnvironment, parameterTypes));
	}

	public static MethodSimpleSignature createSimple(
			String methodSimpleName,
			List<TypeMirror> parameterTypes) {
		Objects.requireNonNull(methodSimpleName, "methodSimpleName");
		Objects.requireNonNull(parameterTypes, "parameterTypes");
		//
		return MethodSimpleSignature.of(
				methodSimpleName,
				parameterTypes.stream().map(TypeMirror::toString));
	}

	public static MethodSimpleSignature createSimple2(
			String methodSimpleName,
			List<TypeDescription> parameterTypes) {
		Objects.requireNonNull(methodSimpleName, "methodSimpleName");
		Objects.requireNonNull(parameterTypes, "parameterTypes");
		//
		return MethodSimpleSignature.of(
				methodSimpleName,
				parameterTypes.stream().map(TypeDescription::getErasureString));
	}

	public static MethodSimpleSignature createSimple2(
			ProcessingEnvironment processingEnvironment,
			String methodSimpleName,
			List<TypeDescription> parameterTypes) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(methodSimpleName, "methodSimpleName");
		Objects.requireNonNull(parameterTypes, "parameterTypes");
		//
		return createSimple2(methodSimpleName, parameterTypes);
	}

	public static MethodSimpleSignature createSimpleFromParameterful(
			ProcessingEnvironment processingEnvironment,
			String methodSimpleName,
			Class<?>... parameterTypes) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(methodSimpleName, "methodSimpleName");
		Objects.requireNonNull(parameterTypes, "parameterTypes");
		//
		return createSimpleFromParameterful(
				processingEnvironment,
				methodSimpleName,
				Stream.of(parameterTypes)
						.map(parameterType -> TypeMirrorUtils.get(processingEnvironment, parameterType)));
	}

	public static MethodSimpleSignature createSimpleFromParameterful(
			ProcessingEnvironment processingEnvironment,
			String methodSimpleName,
			Stream<TypeMirror> parameterTypes) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(methodSimpleName, "methodSimpleName");
		Objects.requireNonNull(parameterTypes, "parameterTypes");
		//
		return createSimple(
				methodSimpleName,
				TypeMirrorUtils.erasures(
						processingEnvironment,
						parameterTypes.collect(Collectors.toList())));
	}
}
