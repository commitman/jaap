package tk.labyrinth.jaap.template.element;

import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.util.ElementUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import java.lang.reflect.Type;

public interface ElementTemplateFactory {

	@Nullable
	ElementTemplate findElement(ElementSignature elementSignature);

	@Nullable
	ElementTemplate findElement(String elementSignatureString);

	FieldElementTemplate findField(Class<?> type, String fieldName);

	FieldElementTemplate findField(String fieldFullName);

	@Nullable
	MethodElementTemplate findMethod(Class<?> type, String methodSimpleSignatureString);

	@Nullable
	MethodElementTemplate findMethod(MethodFullSignature methodFullSignature);

	@Nullable
	MethodElementTemplate findMethod(String methodFullSignatureString);

	PackageElementTemplate findPackage(String packageSignatureString);

	@Nullable
	TypeElementTemplate findType(CanonicalTypeSignature canonicalTypeSignature);

	@Nullable
	TypeElementTemplate findType(String typeSignatureString);

	@Nullable
	TypeParameterElementTemplate findTypeParameter(Class<?> type, String typeParameterName);

	@Nullable
	TypeParameterElementTemplate findTypeParameter(String typeParameterSignature);

	ElementTemplate get(Class<?> type);

	// FIXME: No annproc model for base factories.
	@Deprecated
	ConstructorElementTemplate getConstructor(Element element);

	// FIXME: No annproc model for base factories.
	@Deprecated
	ConstructorElementTemplate getConstructor(ExecutableElement executableElement);

	// FIXME: No annproc model for base factories.
	@Deprecated
	ElementTemplate getElement(Element element);

	default ElementTemplate getElement(String elementSignatureString) {
		ElementTemplate result = findElement(elementSignatureString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: elementSignatureString = " + elementSignatureString);
		}
		return result;
	}

	ExecutableElementTemplate getExecutable(ExecutableElement executableElement);

	default FieldElementTemplate getField(Class<?> type, String fieldName) {
		FieldElementTemplate result = findField(type, fieldName);
		if (result == null) {
			throw new IllegalArgumentException("Not found: type = " + type + ", fieldName = " + fieldName);
		}
		return result;
	}

	default FieldElementTemplate getField(String fieldSignatureString) {
		FieldElementTemplate result = findField(fieldSignatureString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fieldSignatureString = " + fieldSignatureString);
		}
		return result;
	}

	// FIXME: No annproc model for base factories.
	@Deprecated
	FieldElementTemplate getField(Element element);

	// FIXME: No annproc model for base factories.
	@Deprecated
	FieldElementTemplate getField(VariableElement variableElement);

	// FIXME: No annproc model for base factories.
	@Deprecated
	FormalParameterElementTemplate getFormalParameter(VariableElement variableElement);

	default MethodElementTemplate getMethod(Class<?> type, String methodSimpleSignatureString) {
		MethodElementTemplate result = findMethod(type, methodSimpleSignatureString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"type = " + type + ", " +
					"methodSimpleSignatureString = " + methodSimpleSignatureString);
		}
		return result;
	}

	// FIXME: No annproc model for base factories.
	@Deprecated
	default MethodElementTemplate getMethod(Element element) {
		return getMethod(ElementUtils.requireMethod(element));
	}

	// FIXME: No annproc model for base factories.
	@Deprecated
	MethodElementTemplate getMethod(ExecutableElement executableElement);

	default MethodElementTemplate getMethod(String methodFullSignatureString) {
		MethodElementTemplate result = findMethod(methodFullSignatureString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: methodFullSignatureString = " + methodFullSignatureString);
		}
		return result;
	}

	PackageElementTemplate getPackage(Element element);

	PackageElementTemplate getPackage(PackageElement packageElement);

	PackageElementTemplate getPackage(String packageSignatureString);

	PackageElementTemplate getPackageOf(Class<?> childType);

	ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, DeclaredType declaredType);

	ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, Type type);

	ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, TypeMirror typeMirror);

	default TypeElementTemplate getType(CanonicalTypeSignature canonicalTypeSignature) {
		TypeElementTemplate result = findType(canonicalTypeSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: canonicalTypeSignature = " + canonicalTypeSignature);
		}
		return result;
	}

	TypeElementTemplate getType(Class<?> type);

	// FIXME: No annproc model for base factories.
	@Deprecated
	TypeElementTemplate getType(Element element);

	TypeElementTemplate getType(String typeSignatureString);

	// FIXME: No annproc model for base factories.
	@Deprecated
	TypeElementTemplate getType(TypeElement typeElement);

	// FIXME: No annproc model for base factories.
	@Deprecated
	TypeElementTemplate getType(TypeMirror typeMirror);

	TypeParameterElementTemplate getTypeParameter(Class<?> type, String typeParameterName);

	default TypeParameterElementTemplate getTypeParameter(String typeParameterSignature) {
		TypeParameterElementTemplate result = findTypeParameter(typeParameterSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeParameterSignature = " + typeParameterSignature);
		}
		return result;
	}

	// FIXME: No annproc model for base factories.
	@Deprecated
	TypeParameterElementTemplate getTypeParameter(TypeParameterElement typeParameterElement);

	// FIXME: No annproc model for base factories.
	@Deprecated
	VariableElementTemplate getVariable(VariableElement variableElement);
}
