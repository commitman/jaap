package tk.labyrinth.jaap.template.element.common;

public interface HasParent<P> {

	P getParent();
}
