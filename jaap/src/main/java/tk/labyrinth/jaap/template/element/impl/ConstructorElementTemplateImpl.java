package tk.labyrinth.jaap.template.element.impl;

import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.template.element.ConstructorElementTemplate;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.util.ExecutableElementUtils;

import javax.lang.model.element.ExecutableElement;
import java.util.stream.Stream;

public class ConstructorElementTemplateImpl extends ExecutableElementTemplateImpl implements ConstructorElementTemplate {

	public ConstructorElementTemplateImpl(ProcessingContext processingContext, ExecutableElement executableElement) {
		super(processingContext, ExecutableElementUtils.requireConstructor(executableElement));
	}

	@Override
	public int getFormalParameterCount() {
		return getExecutableElement().getParameters().size();
	}

	@Override
	public Stream<FormalParameterElementTemplate> getFormalParameters() {
		return getExecutableElement().getParameters().stream()
				.map(parameter -> getProcessingContext().getFormalParameterElementTemplate(parameter));
	}

	@Override
	public String getSignatureString() {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
