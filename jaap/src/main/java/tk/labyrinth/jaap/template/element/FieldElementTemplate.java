package tk.labyrinth.jaap.template.element;

import javax.annotation.Nonnull;

/**
 * ElementTemplate Hierarchy:<br>
 * - {@link ElementTemplate} (abstract)<br>
 * - - {@link ExecutableElementTemplate} (a)<br>
 * - - - {@link ConstructorElementTemplate} (concrete)<br>
 * - - - {@link InitializerElementTemplate} (c)<br>
 * - - - {@link MethodElementTemplate} (c)<br>
 * - - {@link PackageElementTemplate} (c)<br>
 * - - {@link TypeElementTemplate} (c)<br>
 * - - {@link TypeParameterElementTemplate} (c)<br>
 * - - {@link VariableElementTemplate} (a)<br>
 * * - - {@link FieldElementTemplate} (c)<br>
 * - - - {@link FormalParameterElementTemplate} (c)<br>
 */
public interface FieldElementTemplate extends VariableElementTemplate {

	@Nonnull
	@Override
	TypeElementTemplate getParent();

	@Override
	default TypeElementTemplate getTopLevelTypeElement() {
		return getParent().getTopLevelTypeElement();
	}

	@Override
	default boolean isFieldElement() {
		return true;
	}
}
