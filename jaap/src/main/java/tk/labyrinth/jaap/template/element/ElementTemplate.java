package tk.labyrinth.jaap.template.element;

import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.annotation.AnnotationTypeHandle;
import tk.labyrinth.jaap.annotation.common.HasAnnotations;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotation;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationContext;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.handle.base.mixin.HasProcessingContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.model.entity.mixin.HasSelectableMembers;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import java.lang.annotation.Annotation;
import java.util.List;

/**
 * ElementTemplate Hierarchy:<br>
 * * {@link ElementTemplate} (abstract)<br>
 * - - {@link ExecutableElementTemplate} (a)<br>
 * - - - {@link ConstructorElementTemplate} (concrete)<br>
 * - - - {@link InitializerElementTemplate} (c)<br>
 * - - - {@link MethodElementTemplate} (c)<br>
 * - - {@link PackageElementTemplate} (c)<br>
 * - - {@link TypeElementTemplate} (c)<br>
 * - - {@link TypeParameterElementTemplate} (c)<br>
 * - - {@link VariableElementTemplate} (a)<br>
 * - - - {@link FieldElementTemplate} (c)<br>
 * - - - {@link FormalParameterElementTemplate} (c)<br>
 */
public interface ElementTemplate extends
		HasAnnotations,
		HasProcessingContext,
		HasSelectableMembers {

	default MethodElementTemplate asMethodElement() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default PackageElementTemplate asPackageElement() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default TypeElementTemplate asTypeElement() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	@Nullable
	@Override
	default AnnotationHandle findDirectAnnotation(Class<? extends Annotation> annotationType) {
		return findDirectAnnotation(getProcessingContext().getAnnotationTypeHandle(annotationType));
	}

	@Nullable
	@Override
	default MergedAnnotation findMergedAnnotation(
			AnnotationTypeHandle annotationTypeHandle,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotationContext(annotationTypeHandle, mergedAnnotationSpecification).getMergedAnnotation();
	}

	@Nullable
	@Override
	default MergedAnnotation findMergedAnnotation(
			Class<? extends Annotation> annotationType,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return findMergedAnnotation(
				getProcessingContext().getAnnotationTypeHandle(annotationType),
				mergedAnnotationSpecification);
	}

	@Override
	default List<AnnotationHandle> getDirectAnnotations() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	Element getElement();

	@Override
	default MergedAnnotation getMergedAnnotation(
			String annotationTypeName,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotationContext(
				getProcessingContext().getAnnotationTypeHandle(annotationTypeName),
				mergedAnnotationSpecification).getMergedAnnotation();
	}

	@Override
	default MergedAnnotationContext getMergedAnnotationContext(
			AnnotationTypeHandle annotationTypeHandle,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return new MergedAnnotationContext(annotationTypeHandle, this, mergedAnnotationSpecification);
	}

	@Override
	default MergedAnnotationContext getMergedAnnotationContext(
			Class<? extends Annotation> annotationType,
			MergedAnnotationSpecification mergedAnnotationSpecification) {
		return getMergedAnnotationContext(
				getProcessingContext().getAnnotationTypeHandle(annotationType),
				mergedAnnotationSpecification);
	}

	/**
	 * Can only be null for root PackageElement.
	 *
	 * @return nullable
	 */
	@Nullable
	ElementTemplate getParent();

	String getSignatureString();

	// FIXME: Do not use Name in common model.
	@Deprecated
	Name getSimpleName();

	// FIXME: Do not use common method for element naming, instead use simpleName for types, just name for fields/methods?
	@Deprecated
	default String getSimpleNameAsString() {
		return getSimpleName().toString();
	}

	default boolean isFieldElement() {
		return false;
	}

	default boolean isMethodElement() {
		return false;
	}

	default boolean isPackageElement() {
		return false;
	}

	default boolean isTypeElement() {
		return false;
	}

	/**
	 * Executable - returns {@link ExecutableElementTemplate#getReturnType()};<br>
	 * Package - throws {@link UnsupportedOperationException};<br>
	 * Type - returns {@link TypeElementTemplate#toType()};<br>
	 * Variable - returns {@link VariableElementTemplate#getType()};<br>
	 *
	 * @return non-null
	 *
	 * @throws UnsupportedOperationException if package
	 */
	TypeHandle resolveType();
}
