package tk.labyrinth.jaap.template.element.common;

import tk.labyrinth.jaap.template.element.TypeElementTemplate;

public interface HasTopLevelTypeElement {

	/**
	 * All elements except package has top-level type element.
	 *
	 * @return non-null
	 */
	TypeElementTemplate getTopLevelTypeElement();
}
