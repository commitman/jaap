package tk.labyrinth.jaap.template.element.impl;

import com.google.common.collect.Streams;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.base.TypeElementAwareBase;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.UnreachableStateException;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.template.element.ConstructorElementTemplate;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.FieldElementTemplate;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.jaap.util.TypeMirrorUtils;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TypeElementTemplateImpl extends TypeElementAwareBase implements TypeElementTemplate {

	public TypeElementTemplateImpl(ProcessingContext processingContext, TypeElement typeElement) {
		super(processingContext, typeElement);
	}

	@Nullable
	@Override
	public TypeElementTemplate findSuperclass() {
		TypeMirror superclass = getTypeElement().getSuperclass();
		return superclass != null
				? getProcessingContext().getTypeElementTemplate(superclass)
				: null;
	}

	@Override
	public Stream<FieldElementTemplate> getAllFields() {
		return TypeElementUtils.getAllFields(getProcessingContext().getProcessingEnvironment(), getTypeElement())
				.map(declaredField -> getProcessingContext().getFieldElementTemplate(declaredField));
	}

	@Override
	public String getBinaryName() {
		return getProcessingContext().getProcessingEnvironment().getElementUtils().getBinaryName(getTypeElement()).toString();
	}

	@Override
	public Stream<ConstructorElementTemplate> getConstructors() {
		return getTypeElement().getEnclosedElements().stream()
				.filter(ElementUtils::isConstructor)
				.map(enclosedElement -> getProcessingContext().getConstructorElementTemplate(enclosedElement));
	}

	@Override
	public Stream<FieldElementTemplate> getDeclaredFields() {
		return TypeElementUtils.getDeclaredFields(getTypeElement())
				.map(declaredField -> getProcessingContext().getFieldElementTemplate(declaredField));
	}

	@Override
	public Stream<MethodElementTemplate> getDeclaredMethods() {
		return getTypeElement().getEnclosedElements().stream()
				.filter(element -> element.getKind() == ElementKind.METHOD)
				.map(ExecutableElement.class::cast)
				.map(element -> new MethodElementTemplateImpl(getProcessingContext(), element));
	}

	@Override
	public List<AnnotationHandle> getDirectAnnotations() {
		return getTypeElement().getAnnotationMirrors().stream()
				.map(annotationMirror -> getProcessingContext().getAnnotationHandle(
						getTypeElement(),
						annotationMirror))
				.collect(Collectors.toList());
	}

	@Override
	public Stream<? extends TypeElementTemplate> getDirectSupertypes() {
		return getProcessingContext().getProcessingEnvironment().getTypeUtils()
				.directSupertypes(getTypeElement().asType()).stream()
				.map(directSupertype -> getProcessingContext().getTypeElementTemplate(directSupertype));
	}

	@Override
	public FieldElementTemplate getField(String fieldName) {
		return getAllFields()
				.filter(field -> Objects.equals(field.getSimpleNameAsString(), fieldName))
				.collect(CollectorUtils.findOnly());
	}

	@Override
	public Stream<TypeElementTemplate> getNestedTypeStream() {
		return TypeElementUtils.getNestedTypeElementStream(getTypeElement())
				.map(nestedTypeElement -> getProcessingContext().getTypeElementTemplate(nestedTypeElement));
	}

	@Override
	public PackageElementTemplate getPackage() {
		return getProcessingContext().getPackageElementTemplate(
				TypeElementUtils.getPackageQualifiedName(getTypeElement()));
	}

	@Override
	public String getPackageQualifiedName() {
		return TypeElementUtils.getPackageQualifiedName(getTypeElement());
	}

	@Override
	public ElementTemplate getParent() {
		return getProcessingContext().getElementTemplate(getTypeElement().getEnclosingElement());
	}

	@Override
	public String getQualifiedName() {
		return getTypeElement().getQualifiedName().toString();
	}

	@Override
	public CanonicalTypeSignature getSignature() {
		return CanonicalTypeSignature.ofValid(TypeElementUtils.getSignature(getTypeElement()));
	}

	@Override
	public String getSignatureString() {
		return getSignature().toString();
	}

	@Override
	public Name getSimpleName() {
		return getTypeElement().getSimpleName();
	}

	@Override
	public TypeElementTemplate getTopLevelTypeElement() {
		TypeElementTemplate result;
		{
			ElementTemplate parent = getParent();
			if (parent.isPackageElement()) {
				result = this;
			} else if (parent.isTypeElement()) {
				result = parent.asTypeElement().getTopLevelTypeElement();
			} else {
				throw new UnreachableStateException(parent.toString());
			}
		}
		return result;
	}

	@Override
	public TypeMirror getTypeMirror() {
		return TypeMirrorUtils.erasure(getProcessingContext().getProcessingEnvironment(), getTypeElement());
	}

	@Override
	public int getTypeParameterCount() {
		return getTypeElement().getTypeParameters().size();
	}

	@Override
	public Stream<TypeParameterElementTemplate> getTypeParameters() {
		return getTypeElement().getTypeParameters().stream()
				.map(typeParameter -> getProcessingContext().getTypeParameterElementTemplate(typeParameter));
	}

	@Override
	public boolean isAssignableTo(TypeElementTemplate other) {
		return getProcessingContext().getProcessingEnvironment().getTypeUtils().isAssignable(
				getTypeMirror(), other.getTypeMirror());
	}

	@Override
	public boolean isInterface() {
		return getTypeElement().getKind() == ElementKind.INTERFACE;
	}

	@Override
	public TypeHandle resolveType() {
		return getProcessingContext().getTypeHandle(GenericContext.empty(), getTypeElement());
	}

	@Nullable
	@Override
	public ElementTemplate selectMember(EntitySelector selector) {
		Element element = TypeElementUtils.selectMember(getProcessingContext().getProcessingEnvironment(), getTypeElement(), selector);
		return element != null ? getProcessingContext().getElementTemplate(element) : null;
	}

	@Nullable
	@Override
	public MethodElementTemplate selectMethodElement(String methodSimpleName, List<TypeHandle> argumentTypes) {
		// FIXME: Support overrides (via reduction).
		//noinspection UnstableApiUsage
		return getAllMethods()
				.filter(method -> Objects.equals(method.getSimpleNameAsString(), methodSimpleName))
				.filter(method -> {
					boolean result;
					{
						List<FormalParameterElementTemplate> formalParameters = method.getFormalParameters().collect(Collectors.toList());
						if (argumentTypes.size() == formalParameters.size()) {
							//noinspection UnstableApiUsage
							result = Streams.zip(argumentTypes.stream(), formalParameters.stream(), Pair::of)
									.allMatch(pair -> getProcessingContext().getProcessingEnvironment().getTypeUtils().isAssignable(
											pair.getKey().getTypeMirror(), pair.getValue().getType().getTypeMirror()));
						} else {
							result = false;
						}
					}
					return result;
				}).min(Comparator.comparing(Function.identity(), TypeElementTemplateImpl::compareMethods)
						.thenComparing(comparatorOfMethodParameters())).orElse(null);
	}

	@Override
	public String toString() {
		return getSignature().toString();
	}

	@Override
	public DeclaredTypeHandle toType() {
		return getProcessingContext().getDeclaredTypeHandle(GenericContext.empty(), getTypeElement());
	}

	private static Comparator<MethodElementTemplate> comparatorOfMethodParameters() {
		return Comparator.comparing(
				method -> method.getFormalParameters().map(FormalParameterElementTemplate::getVariableElement)
						.map(VariableElement::asType)
						.findFirst().orElseThrow(() -> new IllegalArgumentException("Methods with no parameters must not be compared here")),
				TypeMirrorUtils::compareConsideringPrimitives);
	}

	private static int compareMethods(MethodElementTemplate first, MethodElementTemplate second) {
		int result;
		{
			TypeElementTemplate firstParent = first.getParent();
			TypeElementTemplate secondParent = second.getParent();
			if (!Objects.equals(firstParent, secondParent)) {
				if (firstParent.isAssignableTo(secondParent)) {
					result = -1;
				} else if (firstParent.isAssignableFrom(secondParent)) {
					result = 1;
				} else {
					result = 0;
				}
			} else {
				result = 0;
			}
		}
		return result;
//		Comparator.<MethodElementTemplate>comparing(method -> 0).<MethodElementTemplate>thenComparing(
//				method -> method.getParameters()
//						.map(ParameterElementTemplate::getVariableElement)
//						.map(VariableElement::asType)
//						.findFirst().orElse(null),
//				TypeMirrorUtils::compareConsideringPrimitives
//		)
	}
}
