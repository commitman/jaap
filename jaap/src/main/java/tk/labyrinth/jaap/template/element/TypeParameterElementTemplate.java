package tk.labyrinth.jaap.template.element;

import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.element.common.HasParent;
import tk.labyrinth.jaap.template.element.common.HasSignature;
import tk.labyrinth.jaap.template.element.common.HasTopLevelTypeElement;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeParameterElement;
import java.util.List;

/**
 * ElementTemplate Hierarchy:<br>
 * - {@link ElementTemplate} (abstract)<br>
 * - - {@link ExecutableElementTemplate} (a)<br>
 * - - - {@link ConstructorElementTemplate} (concrete)<br>
 * - - - {@link InitializerElementTemplate} (c)<br>
 * - - - {@link MethodElementTemplate} (c)<br>
 * - - {@link PackageElementTemplate} (c)<br>
 * - - {@link TypeElementTemplate} (c)<br>
 * * - {@link TypeParameterElementTemplate} (c)<br>
 * - - {@link VariableElementTemplate} (a)<br>
 * - - - {@link FieldElementTemplate} (c)<br>
 * - - - {@link FormalParameterElementTemplate} (c)<br>
 */
public interface TypeParameterElementTemplate extends
		ElementTemplate,
		HasSignature<String>,
		HasParent<ElementTemplate>,
		HasTopLevelTypeElement {

	List<TypeHandle> getBounds();

	@Override
	default Element getElement() {
		return getTypeParameterElement();
	}

	TypeParameterElement getTypeParameterElement();
}
