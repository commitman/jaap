package tk.labyrinth.jaap.template.element;

import javax.lang.model.element.Element;
import javax.lang.model.element.PackageElement;

/**
 * ElementTemplate Hierarchy:<br>
 * - {@link ElementTemplate} (abstract)<br>
 * - - {@link ExecutableElementTemplate} (a)<br>
 * - - - {@link ConstructorElementTemplate} (concrete)<br>
 * - - - {@link InitializerElementTemplate} (c)<br>
 * - - - {@link MethodElementTemplate} (c)<br>
 * * - {@link PackageElementTemplate} (c)<br>
 * - - {@link TypeElementTemplate} (c)<br>
 * - - {@link TypeParameterElementTemplate} (c)<br>
 * - - {@link VariableElementTemplate} (a)<br>
 * - - - {@link FieldElementTemplate} (c)<br>
 * - - - {@link FormalParameterElementTemplate} (c)<br>
 */
public interface PackageElementTemplate extends ElementTemplate {

	@Override
	default PackageElementTemplate asPackageElement() {
		return this;
	}

	@Override
	default Element getElement() {
		return getPackageElement();
	}

	PackageElement getPackageElement();

	/**
	 * @return non-null
	 */
	String getQualifiedName();

	@Override
	default boolean isPackageElement() {
		return true;
	}

	boolean isSynthetic();
}
