package tk.labyrinth.jaap.template.element;

import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.template.element.common.HasTopLevelTypeElement;

import javax.annotation.Nonnull;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;

/**
 * ElementTemplate Hierarchy:<br>
 * - {@link ElementTemplate} (abstract)<br>
 * * - {@link ExecutableElementTemplate} (a)<br>
 * - - - {@link ConstructorElementTemplate} (concrete)<br>
 * - - - {@link InitializerElementTemplate} (c)<br>
 * - - - {@link MethodElementTemplate} (c)<br>
 * - - {@link PackageElementTemplate} (c)<br>
 * - - {@link TypeElementTemplate} (c)<br>
 * - - {@link TypeParameterElementTemplate} (c)<br>
 * - - {@link VariableElementTemplate} (a)<br>
 * - - - {@link FieldElementTemplate} (c)<br>
 * - - - {@link FormalParameterElementTemplate} (c)<br>
 */
public interface ExecutableElementTemplate extends
		ElementTemplate,
		HasTopLevelTypeElement {

	/**
	 * @return never
	 *
	 * @deprecated see {@link #getExecutableElement()}
	 */
	@Deprecated
	@Override
	default Element getElement() {
		return getExecutableElement();
	}

	/**
	 * @return never
	 *
	 * @deprecated We have synthetic methods which do not yield ExecutableElement, should not rely on this method.
	 */
	@Deprecated
	default ExecutableElement getExecutableElement() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	@Nonnull
	@Override
	TypeElementTemplate getParent();

	TypeHandle getReturnType();

	@Override
	default TypeElementTemplate getTopLevelTypeElement() {
		return getParent().getTopLevelTypeElement();
	}

	@Override
	default TypeHandle resolveType() {
		return getReturnType();
	}
}
