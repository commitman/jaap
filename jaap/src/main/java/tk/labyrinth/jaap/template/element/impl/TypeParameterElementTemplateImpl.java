package tk.labyrinth.jaap.template.element.impl;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;
import tk.labyrinth.jaap.template.element.util.TypeParameterElementUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeParameterElement;
import java.util.List;
import java.util.stream.Collectors;

@EqualsAndHashCode
@RequiredArgsConstructor
public class TypeParameterElementTemplateImpl implements TypeParameterElementTemplate {

	@EqualsAndHashCode.Exclude
	@Getter
	private final ProcessingContext processingContext;

	@Getter
	private final TypeParameterElement typeParameterElement;

	@Override
	public List<TypeHandle> getBounds() {
		return typeParameterElement.getBounds().stream()
				.map(bound -> processingContext.getTypeHandle(GenericContext.empty(), bound))
				.collect(Collectors.toList());
	}

	@Override
	public ElementTemplate getParent() {
		return processingContext.getElementTemplate(typeParameterElement.getEnclosingElement());
	}

	@Override
	public String getSignature() {
		return TypeParameterElementUtils.getSignature(
				processingContext.getProcessingEnvironment(),
				typeParameterElement);
	}

	@Override
	public String getSignatureString() {
		return getSignature();
	}

	@Override
	public Name getSimpleName() {
		return typeParameterElement.getSimpleName();
	}

	@Override
	public TypeElementTemplate getTopLevelTypeElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeHandle resolveType() {
		return processingContext.getTypeHandle(
				GenericContext.empty(),
				TypeParameterElementUtils.getTypeVariable(typeParameterElement));
	}

	@Nullable
	@Override
	public ElementTemplate selectMember(EntitySelector selector) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String toString() {
		return getSignature();
	}
}
