package tk.labyrinth.jaap.template.element.util;

import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodElementTemplateUtils {

	public static Stream<MethodElementTemplate> filterNonOverriden(Stream<MethodElementTemplate> methodElementTemplates) {
		Map<MethodSimpleSignature, List<MethodElementTemplate>> groupedMethodElementTemplates = methodElementTemplates
				.collect(Collectors.groupingBy(MethodElementTemplate::getSimpleSignature));
		return groupedMethodElementTemplates.values().stream()
				.map(templates -> templates.stream().reduce((first, second) ->
						first.getParent().isAssignableTo(second.getParent()) ? first : second))
				.map(Optional::orElseThrow);
	}
}
