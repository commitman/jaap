package tk.labyrinth.jaap.template.element.synthetic;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.MethodModifier;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.lang.model.element.Name;
import java.util.stream.Stream;

@EqualsAndHashCode
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SyntheticMethodElementTemplate implements MethodElementTemplate {

	private final MethodDeclaration declaration;

	private final CanonicalTypeSignature parentSignature;

	@EqualsAndHashCode.Exclude
	private final ProcessingContext processingContext;

	@Override
	public int getFormalParameterCount() {
		return declaration.getFormalParameters().size();
	}

	@Override
	public Stream<FormalParameterElementTemplate> getFormalParameters() {
		return declaration.getFormalParameters().stream().map(parameter ->
				SyntheticFormalParameterElementTemplate.from(this, parameter));
	}

	@Override
	public Stream<MethodModifier> getModifiers() {
		return declaration.getModifiers().stream();
	}

	@Nonnull
	@Override
	public TypeElementTemplate getParent() {
		return processingContext.getTypeElementTemplate(parentSignature);
	}

	@Override
	public ProcessingContext getProcessingContext() {
		return processingContext;
	}

	@Override
	public TypeHandle getReturnType() {
		return processingContext.getTypeHandle(declaration.getReturnType());
	}

	@Override
	public String getSignature() {
		return parentSignature + SignatureSeparators.EXECUTABLE + declaration.getSignature().toString();
	}

	@Override
	public String getSignatureString() {
		return getSignature();
	}

	@Override
	public Name getSimpleName() {
		return getProcessingContext().getName(declaration.getName());
	}

	@Override
	public int getTypeParameterCount() {
		return declaration.getTypeParameters().size();
	}

	@Override
	public Stream<TypeParameterElementTemplate> getTypeParameters() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasExplicitPublicModifier() {
		return declaration.getModifiers().contains(JavaMethodModifier.PUBLIC);
	}

	@Override
	public boolean hasExplicitStaticModifier() {
		return declaration.getModifiers().contains(JavaMethodModifier.STATIC);
	}

	@Override
	public boolean isEffectivelyNonPublic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyNonStatic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyPublic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyStatic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public ElementTemplate selectMember(EntitySelector selector) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String toString() {
		// FIXME: Varargs
		return getFullSignature().toString();
	}

	public static SyntheticMethodElementTemplate from(ProcessingContext processingContext, CanonicalTypeSignature parentSignature, MethodDeclaration declaration) {
		return new SyntheticMethodElementTemplate(declaration, parentSignature, processingContext);
	}
}
