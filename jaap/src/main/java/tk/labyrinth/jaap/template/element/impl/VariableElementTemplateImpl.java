package tk.labyrinth.jaap.template.element.impl;

import tk.labyrinth.jaap.base.VariableElementAwareBase;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.util.TypeHandleUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.VariableElementTemplate;

import javax.annotation.Nullable;
import javax.lang.model.element.Name;
import javax.lang.model.element.VariableElement;

public abstract class VariableElementTemplateImpl extends VariableElementAwareBase implements VariableElementTemplate {

	public VariableElementTemplateImpl(ProcessingContext processingContext, VariableElement variableElement) {
		super(processingContext, variableElement);
	}

	@Override
	public Name getSimpleName() {
		return getVariableElement().getSimpleName();
	}

	@Override
	public TypeHandle getType() {
		return getProcessingContext().getTypeHandle(GenericContext.empty(), getVariableElement().asType());
	}

	@Nullable
	@Override
	public ElementTemplate selectMember(EntitySelector selector) {
		return TypeHandleUtils.selectMember(resolveType(), selector);
	}
}
