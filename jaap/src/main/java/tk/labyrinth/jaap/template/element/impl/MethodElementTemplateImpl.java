package tk.labyrinth.jaap.template.element.impl;

import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;
import tk.labyrinth.jaap.model.declaration.MethodModifier;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;
import tk.labyrinth.jaap.template.element.util.ExecutableElementUtils;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodElementTemplateImpl extends ExecutableElementTemplateImpl implements MethodElementTemplate {

	public MethodElementTemplateImpl(ProcessingContext processingContext, ExecutableElement executableElement) {
		super(processingContext, ExecutableElementUtils.requireMethod(executableElement));
	}

	@Override
	public List<AnnotationHandle> getDirectAnnotations() {
		return getExecutableElement().getAnnotationMirrors().stream()
				.map(annotationMirror -> getProcessingContext().getAnnotationHandle(
						getExecutableElement(),
						annotationMirror))
				.collect(Collectors.toList());
	}

	@Override
	public int getFormalParameterCount() {
		return getExecutableElement().getParameters().size();
	}

	@Override
	public Stream<FormalParameterElementTemplate> getFormalParameters() {
		return getExecutableElement().getParameters().stream()
				.map(parameter -> getProcessingContext().getFormalParameterElementTemplate(parameter));
	}

	@Override
	public Stream<MethodModifier> getModifiers() {
		return getExecutableElement().getModifiers().stream().map(modifier -> JavaMethodModifier.valueOf(modifier.name()));
	}

	@Override
	public String getSignature() {
		return ExecutableElementUtils.getSignatureString(
				getProcessingContext().getProcessingEnvironment(),
				getExecutableElement());
	}

	@Override
	public String getSignatureString() {
		return getSignature();
	}

	@Override
	public int getTypeParameterCount() {
		return getExecutableElement().getTypeParameters().size();
	}

	@Override
	public Stream<TypeParameterElementTemplate> getTypeParameters() {
		return getExecutableElement().getTypeParameters().stream()
				.map(typeParameter -> getProcessingContext().getTypeParameterElementTemplate(typeParameter));
	}

	@Override
	public boolean hasExplicitPublicModifier() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasExplicitStaticModifier() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyNonPublic() {
		return !isEffectivelyPublic();
	}

	@Override
	public boolean isEffectivelyNonStatic() {
		return !isEffectivelyStatic();
	}

	@Override
	public boolean isEffectivelyPublic() {
		return getExecutableElement().getModifiers().contains(Modifier.PUBLIC);
	}

	@Override
	public boolean isEffectivelyStatic() {
		return getExecutableElement().getModifiers().contains(Modifier.STATIC);
	}

	@Override
	public String toString() {
		return getSignature();
	}
}
