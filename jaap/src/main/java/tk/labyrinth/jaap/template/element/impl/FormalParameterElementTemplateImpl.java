package tk.labyrinth.jaap.template.element.impl;

import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.util.VariableElementUtils;

import javax.lang.model.element.VariableElement;

public class FormalParameterElementTemplateImpl extends VariableElementTemplateImpl implements
		FormalParameterElementTemplate {

	public FormalParameterElementTemplateImpl(ProcessingContext processingContext, VariableElement variableElement) {
		super(processingContext, VariableElementUtils.requireFormalParameter(variableElement));
	}

	@Override
	public MethodElementTemplate getParent() {
		return getProcessingContext().getMethodElementTemplate(getVariableElement().getEnclosingElement());
	}

	@Override
	public String toString() {
		return getType() + " " + getSimpleName();
	}
}
