package tk.labyrinth.jaap.template.element.handle;

import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.common.HasParent;
import tk.labyrinth.jaap.template.element.common.HasTypeParameters;

import javax.lang.model.element.Parameterizable;

/**
 * @see Parameterizable
 */
// TODO: CanBeTypeElementTemplate, CanBeExecutableElementTemplate
public interface ParameterizableElementHandle extends
		HasParent<ElementTemplate>,
		HasTypeParameters {
	// empty
}
