package tk.labyrinth.jaap.template.element;

import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.MethodModifier;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.common.HasFormalParameters;
import tk.labyrinth.jaap.template.element.common.HasPublicModifier;
import tk.labyrinth.jaap.template.element.common.HasSignature;
import tk.labyrinth.jaap.template.element.common.HasStaticModifier;
import tk.labyrinth.jaap.template.element.common.HasTypeParameters;
import tk.labyrinth.jaap.template.element.handle.ParameterizableElementHandle;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ElementTemplate Hierarchy:<br>
 * - {@link ElementTemplate} (abstract)<br>
 * - - {@link ExecutableElementTemplate} (a)<br>
 * - - - {@link ConstructorElementTemplate} (concrete)<br>
 * - - - {@link InitializerElementTemplate} (c)<br>
 * * - - {@link MethodElementTemplate} (c)<br>
 * - - {@link PackageElementTemplate} (c)<br>
 * - - {@link TypeElementTemplate} (c)<br>
 * - - {@link TypeParameterElementTemplate} (c)<br>
 * - - {@link VariableElementTemplate} (a)<br>
 * - - - {@link FieldElementTemplate} (c)<br>
 * - - - {@link FormalParameterElementTemplate} (c)<br>
 */
public interface MethodElementTemplate extends
		ExecutableElementTemplate,
		HasFormalParameters,
		HasPublicModifier,
		HasSignature<String>,
		HasStaticModifier,
		HasTypeParameters,
		ParameterizableElementHandle {

	@Override
	default MethodElementTemplate asMethodElement() {
		return this;
	}

	default MethodDeclaration getDeclaration() {
		return MethodDeclaration.builder()
				.formalParameters(getFormalParameters()
						.map(FormalParameterElementTemplate::getDeclaration)
						.collect(Collectors.toList()))
				.modifiers(getModifiers().collect(Collectors.toList()))
				.name(getSimpleNameAsString())
				.returnType(getReturnType().getDescription())
				.build();
	}

	default MethodFullSignature getFullSignature() {
		return getSimpleSignature().toFull(getParent().getSignatureString());
	}

	Stream<MethodModifier> getModifiers();

	/**
	 * Useful links:<br>
	 * - <a href="https://www.baeldung.com/java-method-signature-return-type#vararg-parameters">https://www.baeldung.com/java-method-signature-return-type#vararg-parameters</a><br>
	 *
	 * @return non-null
	 */
	default MethodSimpleSignature getSimpleSignature() {
		return MethodSimpleSignature.of(
				getSimpleNameAsString(),
				getFormalParameters().map(formalParameter ->
						formalParameter.getType().getSignatureContributingString()));
	}

	@Override
	default boolean isMethodElement() {
		return true;
	}
}
