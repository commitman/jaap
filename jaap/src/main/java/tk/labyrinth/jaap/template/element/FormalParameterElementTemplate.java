package tk.labyrinth.jaap.template.element;

import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Method parameter.<br>
 * <br>
 * ElementTemplate Hierarchy:<br>
 * - {@link ElementTemplate} (abstract)<br>
 * - - {@link ExecutableElementTemplate} (a)<br>
 * - - - {@link ConstructorElementTemplate} (concrete)<br>
 * - - - {@link InitializerElementTemplate} (c)<br>
 * - - - {@link MethodElementTemplate} (c)<br>
 * - - {@link PackageElementTemplate} (c)<br>
 * - - {@link TypeElementTemplate} (c)<br>
 * - - {@link TypeParameterElementTemplate} (c)<br>
 * - - {@link VariableElementTemplate} (a)<br>
 * - - - {@link FieldElementTemplate} (c)<br>
 * * - - {@link FormalParameterElementTemplate} (c)<br>
 */
public interface FormalParameterElementTemplate extends
		VariableElementTemplate {

	default FormalParameterDeclaration getDeclaration() {
		return FormalParameterDeclaration.builder()
				.name(getSimpleNameAsString())
				.type(getType().getDescription())
				.build();
	}

	default int getIndex() {
		List<FormalParameterElementTemplate> formalParameters = getParent().getFormalParameters().collect(Collectors.toList());
		return formalParameters.indexOf(this);
	}

	@Override
	MethodElementTemplate getParent();

	@Override
	default String getSignatureString() {
		return getParent().getFullSignature() + "#" + getIndex();
	}

	@Override
	default TypeElementTemplate getTopLevelTypeElement() {
		return getParent().getTopLevelTypeElement();
	}
}
