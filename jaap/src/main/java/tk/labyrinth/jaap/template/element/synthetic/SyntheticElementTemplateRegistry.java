package tk.labyrinth.jaap.template.element.synthetic;

import lombok.Setter;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

public class SyntheticElementTemplateRegistry {

	// TODO: Key should be TypeReference for better reliability.
	private final Map<String, Set<MethodDeclaration>> methodDeclarations = new HashMap<>();

	@Setter
	private ProcessingContext processingContext;

	@Nullable
	public MethodElementTemplate findMethod(MethodFullSignature methodFullSignature) {
		return methodDeclarations.getOrDefault(
				methodFullSignature.getCanonicalTypeSignature().toQualifiedName(),
				Collections.emptySet())
				.stream()
				.filter(methodDeclaration -> Objects.equals(
						methodDeclaration.getSignature(),
						methodFullSignature.getSimpleSignature()))
				.map(methodDeclaration -> SyntheticMethodElementTemplate.from(
						processingContext,
						methodFullSignature.getCanonicalTypeSignature(),
						methodDeclaration))
				.collect(CollectorUtils.findOnly(true));
	}

	@Nullable
	public MethodElementTemplate findMethod(String methodFullSignature) {
		return findMethod(MethodFullSignature.of(methodFullSignature));
	}

	public MethodElementTemplate getMethod(String methodFullSignature) {
		MethodElementTemplate result = findMethod(methodFullSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: methodFullSignature = " + methodFullSignature);
		}
		return result;
	}

	public Stream<MethodElementTemplate> getMethodsOfType(CanonicalTypeSignature fullTypeSignature) {
		return methodDeclarations.getOrDefault(fullTypeSignature.toQualifiedName(), Collections.emptySet())
				.stream().map(methodDeclaration -> SyntheticMethodElementTemplate.from(
						processingContext, fullTypeSignature, methodDeclaration));
	}

	public void registerMethodDeclaration(CanonicalTypeSignature parentSignature, MethodDeclaration declaration) {
		methodDeclarations.computeIfAbsent(parentSignature.toQualifiedName(), key -> new HashSet<>()).add(declaration);
	}
}
