package tk.labyrinth.jaap.template.element;

import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.element.common.HasTopLevelTypeElement;

import javax.lang.model.element.Element;
import javax.lang.model.element.VariableElement;

/**
 * ElementTemplate Hierarchy:<br>
 * - {@link ElementTemplate} (abstract)<br>
 * - - {@link ExecutableElementTemplate} (a)<br>
 * - - - {@link ConstructorElementTemplate} (concrete)<br>
 * - - - {@link InitializerElementTemplate} (c)<br>
 * - - - {@link MethodElementTemplate} (c)<br>
 * - - {@link PackageElementTemplate} (c)<br>
 * - - {@link TypeElementTemplate} (c)<br>
 * - - {@link TypeParameterElementTemplate} (c)<br>
 * * - {@link VariableElementTemplate} (a)<br>
 * - - - {@link FieldElementTemplate} (c)<br>
 * - - - {@link FormalParameterElementTemplate} (c)<br>
 */
public interface VariableElementTemplate extends
		ElementTemplate,
		HasTopLevelTypeElement {

	@Override
	default Element getElement() {
		return getVariableElement();
	}

	TypeHandle getType();

	VariableElement getVariableElement();

	@Override
	default TypeHandle resolveType() {
		return getType();
	}
}
