package tk.labyrinth.jaap.template.element;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.context.ProcessingContextAware;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.impl.ParameterizedTypeHandleImpl;
import tk.labyrinth.jaap.langmodel.ElementFactory;
import tk.labyrinth.jaap.langmodel.TypeMirrorFactory;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.jaap.model.element.template.lang.LangPackageElementTemplate;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.template.element.impl.ConstructorElementTemplateImpl;
import tk.labyrinth.jaap.template.element.impl.FieldElementTemplateImpl;
import tk.labyrinth.jaap.template.element.impl.FormalParameterElementTemplateImpl;
import tk.labyrinth.jaap.template.element.impl.MethodElementTemplateImpl;
import tk.labyrinth.jaap.template.element.impl.TypeElementTemplateImpl;
import tk.labyrinth.jaap.template.element.impl.TypeParameterElementTemplateImpl;
import tk.labyrinth.jaap.template.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.template.element.util.VariableElementUtils;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.jaap.util.TypeMirrorUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import java.lang.reflect.Type;
import java.util.Objects;

@RequiredArgsConstructor
public class AnnprocElementTemplateFactoryImpl implements ElementTemplateFactory, ProcessingContextAware {

	private final ElementFactory elementFactory;

	private final TypeMirrorFactory typeMirrorFactory;

	@Getter(AccessLevel.PROTECTED)
	private ProcessingContext processingContext;

	@Override
	public void acceptProcessingContext(ProcessingContext processingContext) {
		this.processingContext = processingContext;
	}

	@Nullable
	@Override
	public ElementTemplate findElement(ElementSignature elementSignature) {
		Objects.requireNonNull(elementSignature, "elementSignature");
		//
		Element element = elementFactory.findElement(elementSignature);
		return element != null ? getElement(element) : null;
	}

	@Nullable
	@Override
	public ElementTemplate findElement(String elementSignatureString) {
		return findElement(ElementSignature.of(elementSignatureString));
	}

	@Override
	public FieldElementTemplate findField(Class<?> type, String fieldName) {
		// FIXME: Make it not fail on null.
		TypeElementTemplate typeElementTemplate = getType(type);
		return getField(VariableElementUtils.resolveField(
				processingContext.getProcessingEnvironment(), type, fieldName));
	}

	@Override
	public FieldElementTemplate findField(String fieldFullName) {
		// FIXME: Make it not fail on null.
		return getField(VariableElementUtils.resolveField(
				processingContext.getProcessingEnvironment(), fieldFullName));
	}

	@Nullable
	@Override
	public MethodElementTemplate findMethod(Class<?> type, String methodSimpleSignatureString) {
		ExecutableElement executableElement = elementFactory.findMethod(type, methodSimpleSignatureString);
		return executableElement != null ? getMethod(executableElement) : null;
	}

	@Nullable
	@Override
	public MethodElementTemplate findMethod(MethodFullSignature methodFullSignature) {
		ExecutableElement executableElement = elementFactory.findMethod(methodFullSignature);
		return executableElement != null ? getMethod(executableElement) : null;
	}

	@Nullable
	@Override
	public MethodElementTemplate findMethod(String methodFullSignatureString) {
		return findMethod(MethodFullSignature.of(methodFullSignatureString));
	}

	@Override
	public PackageElementTemplate findPackage(String packageSignatureString) {
		PackageElement packageElement = elementFactory.findPackage(packageSignatureString);
		return packageElement != null ? getPackage(packageElement) : null;
	}

	@Nullable
	@Override
	public TypeElementTemplate findType(CanonicalTypeSignature canonicalTypeSignature) {
		Objects.requireNonNull(canonicalTypeSignature, "canonicalTypeSignature");
		//
		TypeElement typeElement = elementFactory.findType(canonicalTypeSignature);
		return typeElement != null ? getType(typeElement) : null;
	}

	@Nullable
	@Override
	public TypeElementTemplate findType(String typeSignatureString) {
		return findType(CanonicalTypeSignature.ofValid(typeSignatureString));
	}

	@Nullable
	@Override
	public TypeParameterElementTemplate findTypeParameter(Class<?> type, String typeParameterName) {
		return getType(type).findTypeParameter(typeParameterName);
	}

	@Nullable
	@Override
	public TypeParameterElementTemplate findTypeParameter(String typeParameterSignature) {
		TypeParameterElementTemplate result;
		{
			int lastIndexOfSharp = typeParameterSignature.lastIndexOf(SignatureSeparators.TYPE_PARAMETER);
			if (lastIndexOfSharp != -1) {
				TypeElementTemplate typeElementTemplate = findType(typeParameterSignature.substring(0, lastIndexOfSharp));
				if (typeElementTemplate != null) {
					result = typeElementTemplate.findTypeParameter(typeParameterSignature.substring(lastIndexOfSharp + 1));
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Override
	public ElementTemplate get(Class<?> type) {
		return getType(type);
	}

	@Override
	public ConstructorElementTemplate getConstructor(Element element) {
		return getConstructor(ElementUtils.requireConstructor(element));
	}

	@Override
	public ConstructorElementTemplate getConstructor(ExecutableElement executableElement) {
		return new ConstructorElementTemplateImpl(processingContext, ExecutableElementUtils.requireConstructor(executableElement));
	}

	@Override
	public ElementTemplate getElement(Element element) {
		Objects.requireNonNull(element, "element");
		//
		ElementTemplate result;
		if (ElementUtils.isExecutable(element)) {
			result = getExecutable(ElementUtils.requireExecutable(element));
		} else if (ElementUtils.isVariable(element)) {
			result = getVariable(ElementUtils.requireVariable(element));
		} else if (ElementUtils.isPackage(element)) {
			result = getPackage(ElementUtils.requirePackage(element));
		} else if (ElementUtils.isType(element)) {
			result = getType((TypeElement) element);
		} else {
			throw new UnsupportedOperationException(ExceptionUtils.render(element));
		}
		return result;
	}

	@Override
	public ExecutableElementTemplate getExecutable(ExecutableElement executableElement) {
		ExecutableElementTemplate result;
		if (ExecutableElementUtils.isConstructor(executableElement)) {
			result = getConstructor(executableElement);
		} else if (ExecutableElementUtils.isInitializer(executableElement)) {
			throw new NotImplementedException();
		} else if (ExecutableElementUtils.isMethod(executableElement)) {
			result = getMethod(executableElement);
		} else {
			throw new UnsupportedOperationException(ExceptionUtils.render(executableElement));
		}
		return result;
	}

	@Override
	public FieldElementTemplate getField(Element element) {
		return getField(ElementUtils.requireVariable(element));
	}

	@Override
	public FieldElementTemplate getField(VariableElement variableElement) {
		Objects.requireNonNull(variableElement, "variableElement");
		//
		return new FieldElementTemplateImpl(processingContext, variableElement);
	}

	@Override
	public FormalParameterElementTemplate getFormalParameter(VariableElement variableElement) {
		Objects.requireNonNull(variableElement, "variableElement");
		//
		return new FormalParameterElementTemplateImpl(processingContext, variableElement);
	}

	@Override
	public MethodElementTemplate getMethod(ExecutableElement executableElement) {
		Objects.requireNonNull(executableElement, "executableElement");
		//
		return new MethodElementTemplateImpl(processingContext, executableElement);
	}

	@Override
	public PackageElementTemplate getPackage(Element element) {
		return getPackage(elementFactory.getPackage(element));
	}

	@Override
	public PackageElementTemplate getPackage(PackageElement packageElement) {
		return new LangPackageElementTemplate(processingContext, packageElement);
	}

	@Override
	public PackageElementTemplate getPackage(String packageSignatureString) {
		PackageElementTemplate result = findPackage(packageSignatureString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: packageSignatureString = " + packageSignatureString);
		}
		return result;
	}

	@Override
	public PackageElementTemplate getPackageOf(Class<?> childType) {
		return getPackage(elementFactory.getPackageOf(childType));
	}

	@Override
	public ParameterizedTypeHandle getParameterizedTypeHandle(
			GenericContext genericContext,
			DeclaredType declaredType) {
		return new ParameterizedTypeHandleImpl(declaredType, genericContext, processingContext);
	}

	@Override
	public ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, Type type) {
		return getParameterizedTypeHandle(
				genericContext,
				typeMirrorFactory.getDeclared(genericContext, type));
	}

	@Override
	public ParameterizedTypeHandle getParameterizedTypeHandle(GenericContext genericContext, TypeMirror typeMirror) {
		return getParameterizedTypeHandle(genericContext, TypeMirrorUtils.requireDeclaredType(typeMirror));
	}

	@Override
	public TypeElementTemplate getType(Class<?> type) {
		return getType(elementFactory.getType(type));
	}

	@Override
	public TypeElementTemplate getType(Element element) {
		return getType(ElementUtils.requireType(element));
	}

	@Override
	public TypeElementTemplate getType(String typeSignatureString) {
		return getType(elementFactory.getType(typeSignatureString));
	}

	@Override
	public TypeElementTemplate getType(TypeElement typeElement) {
		return new TypeElementTemplateImpl(processingContext, typeElement);
	}

	@Override
	public TypeElementTemplate getType(TypeMirror typeMirror) {
		return getType(TypeElementUtils.get(processingContext.getProcessingEnvironment(), typeMirror));
	}

	@Override
	public TypeParameterElementTemplate getTypeParameter(Class<?> type, String typeParameterName) {
		TypeParameterElementTemplate result = findTypeParameter(type, typeParameterName);
		if (result == null) {
			throw new IllegalArgumentException("Not found: type = " + type + ", typeParameterSimpleName = " + typeParameterName);
		}
		return result;
	}

	@Override
	public TypeParameterElementTemplate getTypeParameter(TypeParameterElement typeParameterElement) {
		Objects.requireNonNull(typeParameterElement, "typeParameterElement");
		//
		return new TypeParameterElementTemplateImpl(processingContext, typeParameterElement);
	}

	@Override
	public VariableElementTemplate getVariable(VariableElement variableElement) {
		VariableElementTemplate result;
		if (VariableElementUtils.isField(variableElement)) {
			result = getField(variableElement);
		} else if (VariableElementUtils.isFormalParameter(variableElement)) {
			result = getFormalParameter(variableElement);
		} else {
			throw new NotImplementedException(ExceptionUtils.render(variableElement));
		}
		return result;
	}
}
