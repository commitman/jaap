package tk.labyrinth.jaap.template.element.enhanced;

import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.impl.TypeElementTemplateImpl;
import tk.labyrinth.jaap.template.element.synthetic.SyntheticElementTemplateRegistry;

import javax.lang.model.element.TypeElement;
import java.util.stream.Stream;

public class EnhancedTypeElementTemplate extends TypeElementTemplateImpl implements TypeElementTemplate {

	private final SyntheticElementTemplateRegistry elementTemplateRegistry;

	public EnhancedTypeElementTemplate(
			ProcessingContext processingContext,
			SyntheticElementTemplateRegistry elementTemplateRegistry,
			TypeElement typeElement) {
		super(processingContext, typeElement);
		this.elementTemplateRegistry = elementTemplateRegistry;
	}

	@Override
	public Stream<MethodElementTemplate> getDeclaredMethods() {
		return Stream.concat(super.getDeclaredMethods(), elementTemplateRegistry.getMethodsOfType(getSignature()));
	}
}
