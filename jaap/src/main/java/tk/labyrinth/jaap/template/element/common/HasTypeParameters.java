package tk.labyrinth.jaap.template.element.common;

import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.annotation.Nullable;
import java.util.Objects;
import java.util.stream.Stream;

public interface HasTypeParameters {

	@Nullable
	default TypeParameterElementTemplate findTypeParameter(int index) {
		return getTypeParameters().skip(index).findFirst().orElse(null);
	}

	@Nullable
	default TypeParameterElementTemplate findTypeParameter(String name) {
		return getTypeParameters()
				.filter(typeParameter -> Objects.equals(typeParameter.getSimpleNameAsString(), name))
				.collect(CollectorUtils.findOnly(true));
	}

	default TypeParameterElementTemplate getTypeParameter(int index) {
		TypeParameterElementTemplate result = findTypeParameter(index);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"index = " + index + ", " +
					"this = " + this);
		}
		return result;
	}

	default TypeParameterElementTemplate getTypeParameter(String name) {
		TypeParameterElementTemplate result = findTypeParameter(name);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"name = " + name + ", " +
					"this = " + this);
		}
		return result;
	}

	int getTypeParameterCount();

	Stream<TypeParameterElementTemplate> getTypeParameters();
}
