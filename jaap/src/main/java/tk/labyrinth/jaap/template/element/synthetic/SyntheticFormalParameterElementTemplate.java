package tk.labyrinth.jaap.template.element.synthetic;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;

import javax.annotation.Nullable;
import javax.lang.model.element.Name;
import javax.lang.model.element.VariableElement;

@EqualsAndHashCode
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SyntheticFormalParameterElementTemplate implements FormalParameterElementTemplate {

	private final FormalParameterDeclaration declaration;

	private final MethodElementTemplate parent;

	@Override
	public MethodElementTemplate getParent() {
		return parent;
	}

	@Override
	public ProcessingContext getProcessingContext() {
		return parent.getProcessingContext();
	}

	@Override
	public Name getSimpleName() {
		return getProcessingContext().getName(declaration.getName());
	}

	@Override
	public TypeHandle getType() {
		return getProcessingContext().getTypeHandle(declaration.getType());
	}

	@Override
	public VariableElement getVariableElement() {
		throw new UnsupportedOperationException();
	}

	@Nullable
	@Override
	public ElementTemplate selectMember(EntitySelector selector) {
		// TODO: Yield annotations?
		return null;
	}

	@Override
	public String toString() {
		return getSignatureString();
	}

	public static SyntheticFormalParameterElementTemplate from(SyntheticMethodElementTemplate parent, FormalParameterDeclaration declaration) {
		return new SyntheticFormalParameterElementTemplate(declaration, parent);
	}
}
