package tk.labyrinth.jaap.template.element.impl;

import tk.labyrinth.jaap.base.ExecutableElementAwareBase;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.handle.type.util.TypeHandleUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.ExecutableElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;

public abstract class ExecutableElementTemplateImpl extends ExecutableElementAwareBase implements ExecutableElementTemplate {

	public ExecutableElementTemplateImpl(ProcessingContext processingContext, ExecutableElement executableElement) {
		super(processingContext, executableElement);
	}

	@Nonnull
	@Override
	public TypeElementTemplate getParent() {
		return getProcessingContext().getTypeElementTemplate(getExecutableElement().getEnclosingElement());
	}

	@Override
	public TypeHandle getReturnType() {
		return getProcessingContext().getTypeHandle(GenericContext.empty(), getExecutableElement().getReturnType());
	}

	@Override
	public Name getSimpleName() {
		return getExecutableElement().getSimpleName();
	}

	@Nullable
	@Override
	public ElementTemplate selectMember(EntitySelector selector) {
		return TypeHandleUtils.selectMember(resolveType(), selector);
	}
}
