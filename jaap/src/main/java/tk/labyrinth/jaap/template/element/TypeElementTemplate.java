package tk.labyrinth.jaap.template.element;

import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.common.HasSignature;
import tk.labyrinth.jaap.template.element.common.HasTopLevelTypeElement;
import tk.labyrinth.jaap.template.element.common.HasTypeParameters;
import tk.labyrinth.jaap.template.element.util.MethodElementTemplateUtils;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ElementTemplate Hierarchy:<br>
 * - {@link ElementTemplate} (abstract)<br>
 * - - {@link ExecutableElementTemplate} (a)<br>
 * - - - {@link ConstructorElementTemplate} (concrete)<br>
 * - - - {@link InitializerElementTemplate} (c)<br>
 * - - - {@link MethodElementTemplate} (c)<br>
 * - - {@link PackageElementTemplate} (c)<br>
 * * - {@link TypeElementTemplate} (c)<br>
 * - - {@link TypeParameterElementTemplate} (c)<br>
 * - - {@link VariableElementTemplate} (a)<br>
 * - - - {@link FieldElementTemplate} (c)<br>
 * - - - {@link FormalParameterElementTemplate} (c)<br>
 */
public interface TypeElementTemplate extends
		ElementTemplate,
		HasSignature<CanonicalTypeSignature>,
		HasTopLevelTypeElement,
		HasTypeParameters {

	@Override
	default TypeElementTemplate asTypeElement() {
		return this;
	}

	@Nullable
	default MethodElementTemplate findDeclaredMethod(String simpleName) {
		List<MethodElementTemplate> result = getDeclaredMethods(simpleName).collect(Collectors.toList());
		if (result.size() > 1) {
			throw new IllegalArgumentException("Multiple methods found with simpleName: " + simpleName);
		}
		return !result.isEmpty() ? result.get(0) : null;
	}

	@Nullable
	default MethodElementTemplate findMethodByName(String methodName) {
		return getAllMethods()
				.filter(method -> Objects.equals(method.getSimpleNameAsString(), methodName))
				.collect(CollectorUtils.findOnly(true));
	}

	@Nullable
	TypeElementTemplate findSuperclass();

	/**
	 * @return non-null
	 *
	 * @since 0.2.8
	 */
	Stream<FieldElementTemplate> getAllFields();

	default Stream<MethodElementTemplate> getAllMethods() {
		return getTypeHierarchy().flatMap(TypeElementTemplate::getDeclaredMethods);
	}

	String getBinaryName();

	Stream<ConstructorElementTemplate> getConstructors();

	Stream<FieldElementTemplate> getDeclaredFields();

	default MethodElementTemplate getDeclaredMethod(String simpleName) {
		return Objects.requireNonNull(findDeclaredMethod(simpleName), "Require method exists: this = " + this + ", simpleName = " + simpleName);
	}

	Stream<MethodElementTemplate> getDeclaredMethods();

	default Stream<MethodElementTemplate> getDeclaredMethods(String simpleName) {
		Objects.requireNonNull(simpleName, "simpleName");
		//
		return getDeclaredMethods().filter(declaredMethod -> Objects.equals(declaredMethod.getSimpleNameAsString(), simpleName));
	}

	Stream<? extends TypeElementTemplate> getDirectSupertypes();

	@Override
	default Element getElement() {
		return getTypeElement();
	}

	FieldElementTemplate getField(String fieldName);

	default MethodElementTemplate getMethodByName(String methodName) {
		MethodElementTemplate result = findMethodByName(methodName);
		if (result == null) {
			throw new IllegalArgumentException("Not found: " +
					"methodName = " + methodName + ", " +
					"this = " + this);
		}
		return result;
	}

	Stream<TypeElementTemplate> getNestedTypeStream();

	default Stream<MethodElementTemplate> getNonOverridenMethods() {
		return MethodElementTemplateUtils.filterNonOverriden(getAllMethods());
	}

	default Stream<MethodElementTemplate> getNonOverridenMethods(String name) {
		return MethodElementTemplateUtils.filterNonOverriden(
				getAllMethods().filter(method -> Objects.equals(method.getSimpleNameAsString(), name)));
	}

	/**
	 * @return non-null
	 */
	PackageElementTemplate getPackage();

	/**
	 * @return non-null
	 */
	String getPackageQualifiedName();

	@Override
	ElementTemplate getParent();

	String getQualifiedName();

	default TypeElementTemplate getSuperclass() {
		TypeElementTemplate result = findSuperclass();
		if (result == null) {
			throw new IllegalArgumentException("Not found: this = " + this);
		}
		return result;
	}

	default Stream<TypeElementTemplate> getTypeChain() {
		ElementTemplate parent = getParent();
		return parent.isTypeElement()
				? StreamUtils.concat(this, parent.asTypeElement().getTypeChain())
				: Stream.of(this);
	}

	TypeElement getTypeElement();

	default Stream<? extends TypeElementTemplate> getTypeHierarchy() {
		Queue<TypeElementTemplate> unprocessedTypeElements = new LinkedList<>();
		unprocessedTypeElements.add(this);
		Set<TypeElementTemplate> processedTypeElements = new HashSet<>();
		//
		List<TypeElementTemplate> result = new ArrayList<>();
		while (!unprocessedTypeElements.isEmpty()) {
			TypeElementTemplate typeElement = unprocessedTypeElements.remove();
			if (processedTypeElements.add(typeElement)) {
				result.add(typeElement);
				typeElement.getDirectSupertypes().forEach(unprocessedTypeElements::add);
			}
		}
		if (isInterface()) {
			TypeElementTemplate objectTypeElement = getProcessingContext().getTypeElementTemplate(Object.class);
			result.add(objectTypeElement);
		}
		return result.stream();
	}

	TypeMirror getTypeMirror();

	/**
	 * {@link Object} is assignable from {@link String};<br>
	 * {@link String} is <b>not</b> assignable from {@link Object};<br>
	 * <br>
	 * {@link String} is assignable to {@link Object};<br>
	 * {@link Object} is <b>not</b> assignable to {@link String};<br>
	 *
	 * @param other non-null
	 *
	 * @return whether <b>this</b> is parent of <b>other</b>
	 *
	 * @see #isAssignableTo(TypeElementTemplate)
	 */
	default boolean isAssignableFrom(TypeElementTemplate other) {
		return other.isAssignableTo(this);
	}

	/**
	 * {@link String} is assignable to {@link Object};<br>
	 * {@link Object} is <b>not</b> assignable to {@link String};<br>
	 * <br>
	 * {@link Object} is assignable from {@link String};<br>
	 * {@link String} is <b>not</b> assignable from {@link Object};<br>
	 *
	 * @param other non-null
	 *
	 * @return whether <b>this</b> is child of <b>other</b>
	 *
	 * @see #isAssignableFrom(TypeElementTemplate)
	 */
	boolean isAssignableTo(TypeElementTemplate other);

	boolean isInterface();

	@Override
	default boolean isTypeElement() {
		return true;
	}

	@Nullable
	default MethodElementTemplate selectMethodElement(MethodSimpleSignature methodSimpleSignature) {
		return selectMethodElement(
				methodSimpleSignature.getName(),
				methodSimpleSignature.getParameters().stream()
						.map(parameter -> getProcessingContext().getTypeHandle(parameter)));
	}

	@Nullable
	MethodElementTemplate selectMethodElement(String methodSimpleName, List<TypeHandle> argumentTypes);

	@Nullable
	default MethodElementTemplate selectMethodElement(String methodSimpleName, Stream<TypeHandle> argumentTypes) {
		return selectMethodElement(methodSimpleName, argumentTypes.collect(Collectors.toList()));
	}

	@Nullable
	default MethodElementTemplate selectMethodElement(String methodSimpleName, TypeHandle... argumentTypes) {
		return selectMethodElement(methodSimpleName, List.of(argumentTypes));
	}

	default MethodElementTemplate selectMethodElementOrFail(MethodSimpleSignature methodSimpleSignature) {
		return Objects.requireNonNull(selectMethodElement(methodSimpleSignature));
	}

	default MethodElementTemplate selectMethodElementOrFail(String methodSimpleName, List<TypeHandle> argumentTypes) {
		return Objects.requireNonNull(selectMethodElement(methodSimpleName, argumentTypes));
	}

	default MethodElementTemplate selectMethodElementOrFail(String methodSimpleName, Stream<TypeHandle> argumentTypes) {
		return Objects.requireNonNull(selectMethodElement(methodSimpleName, argumentTypes));
	}

	default MethodElementTemplate selectMethodElementOrFail(String methodSimpleName, TypeHandle... argumentTypes) {
		return Objects.requireNonNull(selectMethodElement(methodSimpleName, argumentTypes));
	}

	DeclaredTypeHandle toType();
}
