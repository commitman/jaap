package tk.labyrinth.jaap.template.element.impl;

import tk.labyrinth.jaap.annotation.AnnotationHandle;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.template.element.FieldElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.util.VariableElementUtils;

import javax.annotation.Nonnull;
import javax.lang.model.element.VariableElement;
import java.util.List;
import java.util.stream.Collectors;

public class FieldElementTemplateImpl extends VariableElementTemplateImpl implements FieldElementTemplate {

	public FieldElementTemplateImpl(ProcessingContext processingContext, VariableElement variableElement) {
		super(processingContext, VariableElementUtils.requireField(variableElement));
	}

	@Override
	public List<AnnotationHandle> getDirectAnnotations() {
		return getVariableElement().getAnnotationMirrors().stream()
				.map(annotationMirror -> getProcessingContext().getAnnotationHandle(
						getVariableElement(),
						annotationMirror))
				.collect(Collectors.toList());
	}

	@Nonnull
	@Override
	public TypeElementTemplate getParent() {
		return getProcessingContext().getTypeElementTemplate(getVariableElement().getEnclosingElement());
	}

	@Override
	public String getSignatureString() {
		return getParent().getSignatureString() + SignatureSeparators.VARIABLE + getSimpleNameAsString();
	}

	@Override
	public String toString() {
		return getType() + " " + getSimpleName();
	}
}
