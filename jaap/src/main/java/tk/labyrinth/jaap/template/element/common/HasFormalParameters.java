package tk.labyrinth.jaap.template.element.common;

import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;

import java.util.stream.Stream;

public interface HasFormalParameters {

	default FormalParameterElementTemplate getFormalParameter(int index) {
		return getFormalParameters().skip(index).findFirst().orElseThrow();
	}

	int getFormalParameterCount();

	Stream<FormalParameterElementTemplate> getFormalParameters();
}
