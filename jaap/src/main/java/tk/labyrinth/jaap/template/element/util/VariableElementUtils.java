package tk.labyrinth.jaap.template.element.util;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.jaap.util.TypeMirrorUtils;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.Objects;

public class VariableElementUtils {

	public static TypeElement getType(ProcessingEnvironment processingEnvironment, VariableElement variableElement) {
		return TypeElementUtils.get(processingEnvironment, variableElement.asType());
	}

	public static TypeMirror getTypeMirrorErasure(ProcessingEnvironment processingEnvironment, VariableElement variableElement) {
		return TypeMirrorUtils.erasure(processingEnvironment, variableElement);
	}

	public static boolean isField(@Nullable VariableElement value) {
		return value != null && value.getKind() == ElementKind.FIELD;
	}

	public static boolean isFormalParameter(@Nullable VariableElement value) {
		return value != null && value.getKind() == ElementKind.PARAMETER;
	}

	public static Pair<String, String> parseFieldFullName(String fieldFullName) {
		Pair<String, String> result;
		{
			int indexOfSharp = fieldFullName.indexOf('#');
			if (indexOfSharp != -1) {
				result = Pair.of(
						fieldFullName.substring(0, indexOfSharp),
						fieldFullName.substring(indexOfSharp + 1));
			} else {
				throw new IllegalArgumentException("Require '#' as field indicator: " + fieldFullName);
			}
		}
		return result;
	}

	public static Triple<String, String, Integer> parseParameterFullName(String parameterFullName) {
		Triple<String, String, Integer> result;
		{
			int indexOfSharp = parameterFullName.indexOf('#');
			if (indexOfSharp != -1) {
				int indexOfDollar = parameterFullName.indexOf('$', indexOfSharp);
				if (indexOfDollar != -1) {
					result = Triple.of(
							parameterFullName.substring(0, indexOfSharp),
							parameterFullName.substring(indexOfSharp + 1, indexOfDollar),
							Integer.parseInt(parameterFullName.substring(indexOfDollar + 1)));
				} else {
					throw new IllegalArgumentException("Require '$' after '#' as method indicator: " + parameterFullName);
				}
			} else {
				throw new IllegalArgumentException("Require '#' as method indicator: " + parameterFullName);
			}
		}
		return result;
	}

	public static VariableElement requireField(VariableElement value) {
		if (!isField(value)) {
			throw new IllegalArgumentException("Require field: " + value);
		}
		return value;
	}

	public static VariableElement requireFormalParameter(VariableElement value) {
		if (!isFormalParameter(value)) {
			throw new IllegalArgumentException("Require Parameter: " + value);
		}
		return value;
	}

	public static VariableElement resolveField(ProcessingEnvironment processingEnvironment, Class<?> type, String fieldSimpleName) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(type, "type");
		Objects.requireNonNull(fieldSimpleName, "fieldSimpleName");
		//
		return resolveField(processingEnvironment, TypeElementUtils.get(processingEnvironment, type), fieldSimpleName);
	}

	public static VariableElement resolveField(ProcessingEnvironment processingEnvironment, String fieldFullName) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(fieldFullName, "fieldFullName");
		//
		Pair<String, String> parsedName = parseFieldFullName(fieldFullName);
		TypeElement typeElement = TypeElementUtils.get(processingEnvironment, parsedName.getLeft());
		return resolveField(processingEnvironment, typeElement, parsedName.getRight());
	}

	public static VariableElement resolveField(ProcessingEnvironment processingEnvironment, TypeElement typeElement, String fieldSimpleName) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(typeElement, "typeElement");
		Objects.requireNonNull(fieldSimpleName, "fieldSimpleName");
		//
		// FIXME: Support inheritance.
		return TypeElementUtils.getDeclaredField(typeElement, fieldSimpleName);
	}

	public static VariableElement resolveParameter(ExecutableElement executableElement, int parameterIndex) {
		Objects.requireNonNull(executableElement, "executableElement");
		// TODO: Require non-negative parameterIndex.
		//
		return executableElement.getParameters().stream().skip(parameterIndex).findFirst().orElseThrow();
	}

	public static VariableElement resolveParameter(ProcessingEnvironment processingEnvironment, Class<?> type, String methodSimpleSignature, int parameterIndex) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(methodSimpleSignature, "methodSimpleSignature");
		// TODO: Require non-negative parameterIndex.
		//
		return resolveParameter(ExecutableElementUtils.resolve(processingEnvironment, type, methodSimpleSignature), parameterIndex);
	}

	public static VariableElement resolveParameter(ProcessingEnvironment processingEnvironment, String parameterFullName) {
		Objects.requireNonNull(processingEnvironment, "processingEnvironment");
		Objects.requireNonNull(parameterFullName, "parameterFullName");
		//
		Triple<String, String, Integer> parsedName = parseParameterFullName(parameterFullName);
		ExecutableElement executableElement = ExecutableElementUtils.resolve(processingEnvironment,
				parsedName.getLeft(), parsedName.getMiddle());
		return executableElement.getParameters().stream().skip(parsedName.getRight()).findFirst().orElseThrow();
	}

	@Nullable
	public static Element selectMember(ProcessingEnvironment processingEnvironment, VariableElement variableElement, EntitySelector selector) {
		return TypeElementUtils.selectMember(processingEnvironment, getType(processingEnvironment, variableElement), selector);
	}
}
