package tk.labyrinth.jaap.template.element.common;

public interface HasStaticModifier {

	/**
	 * - Top-level type elements are implicitly static;<br>
	 * - Type elements nested in interfaces are implicitly static;<br>
	 * - Other nested type elements are implicitly non-static;<br>
	 * <br>
	 * - Methods are implicitly non-static;<br>
	 *
	 * @return <b>true</b> if this element has no explicit static modifier, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitStaticModifier()
	 */
	// FIXME: Looks like we can't get accurate value in annproc. Should think if we need this method at all.
	boolean hasExplicitStaticModifier();

	/**
	 * @return <b>true</b> if this element has no static modifier and not implicitly static, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitStaticModifier()
	 * @see #isEffectivelyStatic()
	 */
	boolean isEffectivelyNonStatic();

	/**
	 * @return <b>true</b> if this element has static modifier or implicitly static, <b>false</b> otherwise.
	 *
	 * @see #hasExplicitStaticModifier()
	 * @see #isEffectivelyNonStatic()
	 */
	boolean isEffectivelyStatic();
}
