package tk.labyrinth.jaap.template.element.enhanced;

import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.context.ProcessingContextImpl;
import tk.labyrinth.jaap.handle.type.impl.AnnprocTypeHandleFactory;
import tk.labyrinth.jaap.langmodel.AnnprocElementFactory;
import tk.labyrinth.jaap.langmodel.AnnprocTypeMirrorFactory;
import tk.labyrinth.jaap.langmodel.ElementFactory;
import tk.labyrinth.jaap.langmodel.TypeMirrorFactory;
import tk.labyrinth.jaap.template.element.synthetic.SyntheticElementTemplateRegistry;

import javax.annotation.processing.ProcessingEnvironment;

public class EnhancedProcessing {

	public static ProcessingContext createContext(
			ProcessingEnvironment processingEnvironment,
			SyntheticElementTemplateRegistry syntheticElementTemplateRegistry) {
		ElementFactory elementFactory = new AnnprocElementFactory(processingEnvironment);
		TypeMirrorFactory typeMirrorFactory = new AnnprocTypeMirrorFactory(elementFactory, processingEnvironment);
		//
		EnhancedElementTemplateFactory elementTemplateFactory = new EnhancedElementTemplateFactory(
				elementFactory,
				typeMirrorFactory,
				syntheticElementTemplateRegistry);
		AnnprocTypeHandleFactory typeHandleFactory = new AnnprocTypeHandleFactory(typeMirrorFactory);
		//
		ProcessingContext processingContext = new ProcessingContextImpl(
				elementTemplateFactory,
				processingEnvironment,
				typeHandleFactory,
				typeMirrorFactory);
		//
		elementTemplateFactory.acceptProcessingContext(processingContext);
		syntheticElementTemplateRegistry.setProcessingContext(processingContext);
		typeHandleFactory.acceptProcessingContext(processingContext);
		//
		return processingContext;
	}
}
