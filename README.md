# Java Advanced Annotation Processing

[![MAVEN-CENTRAL](https://img.shields.io/maven-central/v/tk.labyrinth/jaap.svg?label=Maven+Central)](https://mvnrepository.com/artifact/tk.labyrinth/jaap)
[![LICENSE](https://img.shields.io/badge/license-MIT-green.svg?label=License)](LICENSE)
[![PIPELINE](https://gitlab.com/commitman/jaap/badges/master/pipeline.svg)](https://gitlab.com/commitman/jaap/-/pipelines)

Utility library to work with <i>javax.annotation.processing</i> & <i>javax.lang.model</i> Classes.

<b>Note: This API is highly experimental and most probably will contain breaking changes with almost each release.</b>

## Maven Dependency

```xml
<dependency>
	<groupId>tk.labyrinth</groupId>
	<artifactId>jaap</artifactId>
	<version>0.4.2</version>
</dependency>
```

## Quick Guide

### Key Concept

FIXME: Rewrite.
JAAP introduces two main type hierarchies:
- [Element]Template - Generic description of an Element with no context;
- [Element]Handle - Description of an Element in a specific context.

TODO: Describe all hierarchy.

### Entry Classes

FIXME: Rewrite.
```java
// Annotation Processing Classes (provided by Compiler).
javax.annotation.processing.ProcessingEnvironment processingEnvironment = ...
javax.annotation.processing.RoundEnvironment roundEnvironment = ...
//
// Context Classes (provided by JAAP).
tk.labyrinth.jaap.context.ProcessingContext processingContext = ProcessingContext.of(processingEnvironment);
tk.labyrinth.jaap.context.RoundContext roundContext = RoundContext.of(round);
//
// Utility factory Classes (statically available).
tk.labyrinth.jaap.Templates
tk.labyrinth.jaap.Handles
```

### Working with javax.lang.model utils

- If you need to obtain instance of some type (like TypeMirror):
  - Look for class with name TypeMirrorUtils;
  - If you have some instances in current scope, look for their respective *Utils classes;
