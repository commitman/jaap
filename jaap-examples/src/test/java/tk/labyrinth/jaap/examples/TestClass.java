package tk.labyrinth.jaap.examples;

/**
 * This one is required for {@link ExampleAnnotationProcessor} to be triggered
 * during this module's test sources compilation.
 */
public class TestClass {
	// empty
}
