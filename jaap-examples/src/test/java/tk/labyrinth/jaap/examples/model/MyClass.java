package tk.labyrinth.jaap.examples.model;

import lombok.Value;

import java.util.List;
import java.util.UUID;

@Value
public class MyClass {

	int myInt;

	Integer myInteger;

	String myString;

	List<UUID> myUuidList;
}
