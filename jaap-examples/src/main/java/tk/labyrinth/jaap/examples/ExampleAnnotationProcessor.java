package tk.labyrinth.jaap.examples;

import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;

//@AutoService(Processor.class)
public class ExampleAnnotationProcessor extends CallbackAnnotationProcessor {

	{
		onFirstRound(round -> {
			// Annotation Processing Classes (provided by Compiler).
			javax.annotation.processing.ProcessingEnvironment processingEnvironment = round.getProcessingEnvironment();
			javax.annotation.processing.RoundEnvironment roundEnvironment = round.getRoundEnvironment();
			//
			// Context Classes (provided by JAAP).
			tk.labyrinth.jaap.context.ProcessingContext processingContext = ProcessingContext.of(processingEnvironment);
			tk.labyrinth.jaap.context.RoundContext roundContext = RoundContext.of(round);
			//
			// Usage of Factory classes (statically available).
			// TODO
		});
		onEachRound(round -> {
			// no-op
		});
	}
}
