package tk.labyrinth.misc4j2.java.lang;

import javax.annotation.Nullable;
import java.util.Objects;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

/**
 * @author Commitman
 * @version 1.0.3
 */
public class ObjectUtils {

	/**
	 * @param value   nullable
	 * @param values  non-null
	 * @param <T>Type
	 *
	 * @return whether <b>value</b> is contained in <b>values</b>
	 */
	@SafeVarargs
	public static <T> boolean in(@Nullable T value, T... values) {
		boolean result = false;
		for (T innerValue : values) {
			if (Objects.equals(innerValue, value)) {
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * @param first                nullable
	 * @param second               nullable
	 * @param mergeOperator        non-null with non-null result
	 * @param defaultValueSupplier non-null with non-null value
	 * @param <T>                  Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <T> T mergeNullable(@Nullable T first, @Nullable T second, BinaryOperator<T> mergeOperator, Supplier<T> defaultValueSupplier) {
		Objects.requireNonNull(mergeOperator, "mergeOperator");
		Objects.requireNonNull(defaultValueSupplier, "defaultValueSupplier");
		//
		T result;
		if (first != null) {
			if (second != null) {
				result = Objects.requireNonNull(mergeOperator.apply(first, second), "mergeResult");
			} else {
				result = first;
			}
		} else {
			if (second != null) {
				result = second;
			} else {
				result = Objects.requireNonNull(defaultValueSupplier.get(), "defaultValue");
			}
		}
		return result;
	}

	/**
	 * Recursively applies provided <b>operator</b> to <b>initialValue</b> and subsequent results
	 * until <b>predicate</b> returns false, preconditionally (while equivalent).
	 *
	 * @param initialValue non-null
	 * @param predicate    non-null
	 * @param operator     non-null
	 * @param <T>          Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static <T> T reduce(T initialValue, Predicate<T> predicate, UnaryOperator<T> operator) {
		return Objects.requireNonNull(reduceNullable(initialValue, predicate, operator));
	}

	/**
	 * Recursively applies provided <b>operator</b> to <b>initialValue</b> and subsequent results
	 * until <b>predicate</b> returns false, preconditionally (while equivalent).
	 *
	 * @param initialValue nullable
	 * @param predicate    non-null
	 * @param operator     non-null
	 * @param <T>          Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.3
	 */
	@Nullable
	public static <T> T reduceNullable(@Nullable T initialValue, Predicate<T> predicate, UnaryOperator<T> operator) {
		T value = initialValue;
		while (predicate.test(value)) {
			value = operator.apply(value);
		}
		return value;
	}

	/**
	 * Checks <b>value</b> for being null.
	 *
	 * @param value null
	 * @param name  nullable
	 * @param <T>   Type
	 *
	 * @return null
	 *
	 * @throws IllegalArgumentException if value is not null
	 * @since 1.0.2
	 */
	@Nullable
	public static <T> T requireNull(@Nullable T value, @Nullable String name) {
		if (value != null) {
			throw new IllegalArgumentException("Require null: "
					+ (name != null ? name + ": " : "")
					+ value);
		}
		return null;
	}
}
