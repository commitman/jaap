package tk.labyrinth.misc4j2.java.util.function;

import tk.labyrinth.jaap.misc4j.exception.UnreachableStateException;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class FunctionUtils {

	public static <T, R> R throwUnreachableStateException(T first, R second) {
		throw new UnreachableStateException();
	}
}
