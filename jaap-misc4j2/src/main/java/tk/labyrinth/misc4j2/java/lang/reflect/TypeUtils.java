package tk.labyrinth.misc4j2.java.lang.reflect;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class TypeUtils {

	/**
	 * @param value non-null
	 * @param <T>   Type
	 *
	 * @return non-null
	 */
	public static <T> T cast(Object value) {
		return castNullable(Objects.requireNonNull(value, "value"));
	}

	/**
	 * @param value nullable
	 * @param <T>   Type
	 *
	 * @return nullable
	 */
	@Nullable
	@SuppressWarnings("unchecked")
	public static <T> T castNullable(@Nullable Object value) {
		return (T) value;
	}

	/**
	 * @param type non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String getSimpleName(Type type) {
		String result;
		if (type instanceof Class) {
			result = ((Class<?>) type).getSimpleName();
		} else {
			result = type.toString();
		}
		return result;
	}
}
