package tk.labyrinth.misc4j2.lang;

public interface Accessor<T> {

	T get();

	T set(T value);
}
