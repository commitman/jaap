package tk.labyrinth.misc4j2.collectoin;

import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.misc4j2.lang.Accessor;
import tk.labyrinth.misc4j2.lang.SimpleAccessor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author Commitman
 * @version 1.1.2
 */
public class StreamUtils {

	/**
	 * <b>Element nullability considerations:</b><br>
	 * This method does not check elements in <b>second</b> argument as this would break streaming so they may be null.
	 * In such case there is not reason in restricting <b>first</b> argument, so any element is permitted be null.
	 *
	 * @param first  nullable
	 * @param second non-null
	 * @param <E>    Element
	 *
	 * @return non-empty
	 *
	 * @since 1.0.5
	 */
	public static <E> Stream<E> concat(@Nullable E first, Stream<E> second) {
		Objects.requireNonNull(second, "second");
		//
		return Stream.concat(Stream.of(first), second);
	}

	/**
	 * <b>Element nullability considerations:</b><br>
	 * This method does not check elements in <b>second</b> argument as this would break streaming so they may be null.
	 * In such case there is not reason in restricting <b>first</b> argument, so any element is permitted be null.
	 *
	 * @param first  non-null
	 * @param second nullable
	 * @param <E>    Element
	 *
	 * @return non-empty
	 *
	 * @since 1.0.7
	 */
	public static <E> Stream<E> concat(Stream<E> first, @Nullable E second) {
		Objects.requireNonNull(first, "first");
		//
		return Stream.concat(first, Stream.of(second));
	}

	/**
	 * Returns a product of sequential concatenation of provided streams.
	 * This is a vararg alternative of {@link Stream#concat}.<br>
	 * <br>
	 * Note that this method does additional null-check before invoking {@link Stream#flatMap} for each element.
	 * This may result in a NPE thrown later when returned stream is terminated.<br>
	 *
	 * @param streams non-null
	 * @param <T>     Type
	 *
	 * @return non-null
	 *
	 * @throws NullPointerException for null vararg or null element
	 */
	@SafeVarargs
	public static <T> Stream<T> concat(Stream<? extends T>... streams) {
		return Stream.of(streams).flatMap(stream -> {
			Objects.requireNonNull(stream);
			return stream;
		});
	}

	/**
	 * @param first  nullable
	 * @param second non-null
	 * @param <T>    Type
	 *
	 * @return non-empty
	 *
	 * @since 1.0.8
	 */
	public static <T> Stream<T> concat(@Nullable T first, T[] second) {
		return Stream.concat(Stream.of(first), Stream.of(second));
	}

	@SuppressWarnings("unchecked")
	public static <T> Stream<T> concatNullable(Stream<? extends T> first, Stream<? extends T> second) {
		Stream<T> result;
		if (first != null) {
			if (second != null) {
				result = Stream.concat(first, second);
			} else {
				result = (Stream<T>) first;
			}
		} else {
			if (second != null) {
				result = (Stream<T>) second;
			} else {
				result = Stream.empty();
			}
		}
		return result;
	}

	@SafeVarargs
	public static <T> Stream<T> concatNullable(Stream<? extends T>... streams) {
		return Stream.of(streams)
				.filter(Objects::nonNull)
				.flatMap(stream -> stream);
	}

	/**
	 * Returns a stream consisting of <i>element</i> and result of applying <i>flatMapFunction</i> to <i>element</i>.
	 *
	 * @param element         non-null
	 * @param flatMapFunction non-null
	 * @param <E>             Element
	 *
	 * @return non-empty
	 */
	public static <E> Stream<E> expand(E element, Function<E, Stream<E>> flatMapFunction) {
		return Stream.concat(Stream.of(element), flatMapFunction.apply(element));
	}

	/**
	 * Returns a stream of elements resulted by applying {@link #expand} to <i>element</i>
	 * and each element yielded after on.
	 *
	 * @param element         non-null
	 * @param flatMapFunction non-null
	 * @param <E>             Element
	 *
	 * @return non-empty
	 */
	public static <E> Stream<E> expandRecursively(E element, Function<E, Stream<E>> flatMapFunction) {
		return Stream.concat(Stream.of(element), flatMapFunction.apply(element));
	}

	public static <T> Stream<T> from(Enumeration<T> enumeration) {
		Objects.requireNonNull(enumeration, "enumeration");
		//
		return from(IteratorUtils.from(enumeration));
	}

	/**
	 * @param iterable non-null
	 * @param <T>      Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.4
	 */
	public static <T> Stream<T> from(Iterable<T> iterable) {
		Objects.requireNonNull(iterable, "iterable");
		//
		return from(iterable.iterator());
	}

	public static <T> Stream<T> from(Iterator<T> iterator) {
		Objects.requireNonNull(iterator, "iterator");
		//
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED), false);
	}

	@Nullable
	public static <T> Stream<T> fromNullable(@Nullable Enumeration<T> enumeration) {
		return enumeration != null ? from(enumeration) : null;
	}

	/**
	 * @param iterable nullable
	 * @param <T>      Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.4
	 */
	@Nullable
	public static <T> Stream<T> fromNullable(@Nullable Iterable<T> iterable) {
		return iterable != null ? from(iterable) : null;
	}

	@Nullable
	public static <T> Stream<T> fromNullable(@Nullable Iterator<T> iterator) {
		return iterator != null ? from(iterator) : null;
	}

	@Nullable
	public static <T> Stream<T> fromOrDefault(@Nullable Collection<T> collection, @Nullable Stream<T> defaultValue) {
		return collection != null ? collection.stream() : defaultValue;
	}

	@Nonnull
	public static <T> Stream<T> fromOrEmpty(@Nullable Collection<T> collection) {
		return collection != null ? collection.stream() : Stream.empty();
	}

	@Nullable
	public static <T> Stream<T> fromOrNull(@Nullable Collection<T> collection) {
		return fromOrDefault(collection, null);
	}

	/**
	 * Note: Not thread-safe.
	 *
	 * @param stream non-null
	 * @param <T>    Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.9
	 */
	public static <T> Stream<Pair<Integer, T>> indexed(Stream<T> stream) {
		int[] index = {0};
		return stream.map(element -> Pair.of(index[0]++, element));
	}

	/**
	 * @param stream         non-null
	 * @param function       non-null
	 * @param initialContext nullable
	 * @param <T>            Input Type
	 * @param <R>            Output Type
	 * @param <C>            Context Type
	 *
	 * @return non-null
	 *
	 * @since 1.1.0
	 */
	public static <T, R, C> Stream<R> mapWithContext(
			Stream<T> stream,
			BiFunction<Accessor<C>, T, R> function,
			@Nullable C initialContext) {
		Accessor<C> contextAccessor = new SimpleAccessor<>(initialContext);
		return stream.map(input -> function.apply(contextAccessor, input));
	}

	/**
	 * @param stream   non-null
	 * @param function non-null
	 * @param <T>      Input Type
	 * @param <R>      Output Type
	 *
	 * @return non-null
	 *
	 * @since 1.1.0
	 */
	public static <T, R> Stream<R> mapWithPreviousInput(Stream<T> stream, BiFunction<T, T, R> function) {
		return mapWithContext(stream, (contextAccessor, input) -> {
			R result = function.apply(contextAccessor.get(), input);
			contextAccessor.set(input);
			return result;
		}, (T) null);
	}

	/**
	 * Note: in Java types are inferred from their first appearance, which means type for R would be inferred
	 * from its appearance as argument and not from return type.<br>
	 * This means lambda argument types should be declared explicitly.<br>
	 *
	 * @param stream   non-null
	 * @param function non-null
	 * @param <T>      Input Type
	 * @param <R>      Output Type
	 *
	 * @return non-null
	 *
	 * @since 1.1.0
	 */
	public static <T, R> Stream<R> mapWithPreviousOutput(Stream<T> stream, BiFunction<R, T, R> function) {
		return mapWithContext(stream, (contextAccessor, input) -> {
			R result = function.apply(contextAccessor.get(), input);
			contextAccessor.set(result);
			return result;
		}, (R) null);
	}

	/**
	 * @param firstStream  non-null
	 * @param secondStream non-null
	 * @param consumer     non-null
	 * @param <F>          First
	 * @param <S>Second
	 *
	 * @since 1.1.1
	 */
	public static <F, S> void permute(Stream<F> firstStream, Stream<S> secondStream, BiConsumer<F, S> consumer) {
		firstStream.forEach(firstElement ->
				secondStream.forEach(secondElement ->
						consumer.accept(firstElement, secondElement)));
	}
}
