package tk.labyrinth.jaap.misc4j.java.lang.reflect;

import java.lang.reflect.Constructor;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class ReflectionUtils {

	/**
	 * @param type non-null
	 * @param <T>  Type
	 *
	 * @return non-null
	 *
	 * @throws RuntimeException over {@link ReflectiveOperationException}
	 * @since 1.0.0
	 */
	public static <T> T createNewInstance(Class<T> type) {
		try {
			return type.getConstructor().newInstance();
		} catch (ReflectiveOperationException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * @param constructor non-null
	 * @param arguments   non-null
	 * @param <T>         Type
	 *
	 * @return non-null
	 *
	 * @throws RuntimeException over {@link ReflectiveOperationException}
	 * @since 1.0.1
	 */
	public static <T> T createNewInstance(Constructor<T> constructor, Stream<?> arguments) {
		try {
			return constructor.newInstance(arguments.toArray());
		} catch (ReflectiveOperationException ex) {
			throw new RuntimeException(ex);
		}
	}
}
