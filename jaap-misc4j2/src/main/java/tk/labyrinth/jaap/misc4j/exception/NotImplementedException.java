package tk.labyrinth.jaap.misc4j.exception;

/**
 * @author Commitman
 * @version 1.0.1
 */
public final class NotImplementedException extends RuntimeException {

	public NotImplementedException() {
		super(detectMethod());
	}

	public NotImplementedException(String message) {
		super(message);
	}

	/**
	 * @return non-null
	 */
	// TODO: Make smarter: Detect line before current class name.
	private static String detectMethod() {
		return Thread.currentThread().getStackTrace()[3].toString();
	}
}
