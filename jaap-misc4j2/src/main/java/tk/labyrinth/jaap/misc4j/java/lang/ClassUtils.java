package tk.labyrinth.jaap.misc4j.java.lang;

import javax.annotation.Nullable;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ClassUtils {

	/**
	 * @param cl non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String getLongName(Class<?> cl) {
		return cl.getEnclosingClass() != null
				? getLongName(cl.getEnclosingClass()) + "." + cl.getSimpleName()
				: cl.getSimpleName();
	}

	/**
	 * @param cl non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String getSignature(Class<?> cl) {
		String result;
		if (isArray(cl)) {
			result = getSignature(cl.getComponentType()) + "[]";
		} else {
			if (!isKeyword(cl)) {
				String packageName = cl.getPackageName();
				//
				result = !packageName.isEmpty()
						? packageName + ":" + getLongName(cl)
						: getLongName(cl);
			} else {
				// Keyword types have no packages or enclosing types.
				result = cl.getSimpleName();
			}
		}
		return result;
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if array, false otherwise
	 *
	 * @since 1.0.0
	 */
	public static boolean isArray(@Nullable Class<?> cl) {
		return cl != null && cl.isArray();
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if keyword, false otherwise
	 *
	 * @since 1.0.0
	 */
	public static boolean isKeyword(@Nullable Class<?> cl) {
		return isPrimitive(cl) || isVoid(cl);
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if primitive, false otherwise
	 *
	 * @since 1.0.0
	 */
	public static boolean isPrimitive(@Nullable Class<?> cl) {
		return cl != null && cl.isPrimitive();
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if void, false otherwise
	 *
	 * @since 1.0.0
	 */
	public static boolean isVoid(@Nullable Class<?> cl) {
		return cl == void.class;
	}
}
