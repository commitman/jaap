package tk.labyrinth.jaap.testing.junit5;

import org.junit.jupiter.api.extension.ExtensionContext;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExpectCompilationFailure;
import tk.labyrinth.jaap.testing.junit5.parameter.JaapVariableResolver;
import tk.labyrinth.misc4j2.java.lang.ObjectUtils;
import tk.labyrinth.misc4j2.java.lang.reflect.DynamicProxyHandler;
import tk.labyrinth.misc4j2.java.lang.reflect.ProxyMarker;

import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class JaapExtensionUtils {

	private static final String allRounds = "all-rounds";

	private static final String firstRound = "first-round";

	private static final String lastRound = "last-round";

	@SuppressWarnings("unchecked")
	private static <T> void enrichMethodArgument(Object argument, AnnotationProcessingRound round, Map<Class<?>, JaapVariableResolver<?>> variableResolvers) {
		if (argument instanceof ProxyMarker) {
			DynamicProxyHandler<T> proxyHandler = (DynamicProxyHandler<T>) Proxy.getInvocationHandler(argument);
			JaapVariableResolver<T> variableResolver = (JaapVariableResolver<T>) variableResolvers.get(proxyHandler.getType());
			proxyHandler.setTarget(variableResolver.resolveVariable(round));
		}
	}

	private static CompilationTarget mergeCompilationTargets(@Nullable CompilationTarget first, @Nullable CompilationTarget second) {
		return ObjectUtils.mergeNullable(first, second,
				CompilationTarget::merge,
				CompilationTarget::ofObject);
	}

	public static void cleanInstanceFields(Object target, Map<Class<?>, JaapVariableResolver<?>> variableResolvers) {
		Stream.of(target.getClass().getDeclaredFields()).forEach(declaredField -> {
			try {
				declaredField.setAccessible(true);
				if (declaredField.get(target) != null && variableResolvers.containsKey(declaredField.getType())) {
					declaredField.set(target, null);
				}
			} catch (IllegalAccessException ex) {
				throw new RuntimeException(ex);
			}
		});
	}

	public static CompilationTarget composeCompilationTarget(Method method) {
		CompilationTarget result;
		{
			CompilationTarget methodDeclaredTarget;
			CompilationTarget typeDeclaredTarget;
			{
				boolean searchForTypeAnnotation;
				{
					tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget methodAnnotation =
							method.getAnnotation(tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget.class);
					if (methodAnnotation != null) {
						methodDeclaredTarget = CompilationTarget.of(
								List.of(methodAnnotation.compiledNames()),
								List.of(methodAnnotation.compiledTypes()),
								List.of(methodAnnotation.sourceFiles()),
								List.of(methodAnnotation.sourceNames()),
								List.of(methodAnnotation.sourceResources()),
								List.of(methodAnnotation.sourceTypes()));
						searchForTypeAnnotation = methodAnnotation.append();
					} else {
						methodDeclaredTarget = null;
						searchForTypeAnnotation = true;
					}
				}
				if (searchForTypeAnnotation) {
					tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget typeAnnotation =
							method.getDeclaringClass().getAnnotation(tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget.class);
					if (typeAnnotation != null) {
						typeDeclaredTarget = CompilationTarget.of(
								List.of(typeAnnotation.compiledNames()),
								List.of(typeAnnotation.compiledTypes()),
								List.of(typeAnnotation.sourceFiles()),
								List.of(typeAnnotation.sourceNames()),
								List.of(typeAnnotation.sourceResources()),
								List.of(typeAnnotation.sourceTypes()));
					} else {
						typeDeclaredTarget = null;
					}
				} else {
					typeDeclaredTarget = null;
				}
			}
			result = mergeCompilationTargets(methodDeclaredTarget, typeDeclaredTarget);
		}
		return result;
	}

	public static void enrichInstanceFields(Object target, AnnotationProcessingRound round, Map<Class<?>, JaapVariableResolver<?>> variableResolvers) {
		Stream.of(target.getClass().getDeclaredFields()).forEach(declaredField -> {
			try {
				declaredField.setAccessible(true);
				if (declaredField.get(target) == null) {
					JaapVariableResolver<?> variableResolver = variableResolvers.get(declaredField.getType());
					if (variableResolver != null) {
						declaredField.set(target, variableResolver.resolveVariable(round));
					}
				}
			} catch (IllegalAccessException ex) {
				throw new RuntimeException(ex);
			}
		});
	}

	public static void enrichMethodArguments(Stream<Object> arguments, AnnotationProcessingRound round, Map<Class<?>, JaapVariableResolver<?>> variableResolvers) {
		arguments.forEach(argument -> enrichMethodArgument(argument, round, variableResolvers));
	}

	public static void ensureInternal(ExtensionContext extensionContext) {
		if (extensionContext.getTags().contains(allRounds) || extensionContext.getTags().contains(lastRound)) {
			throw new IllegalStateException("wat");
		}
		extensionContext.getTags().add(firstRound);
	}

	public static boolean expectCompilationFailure(Method method) {
		return method.getAnnotation(ExpectCompilationFailure.class) != null ||
				method.getDeclaringClass().getAnnotation(ExpectCompilationFailure.class) != null;
	}
}
