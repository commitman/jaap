package tk.labyrinth.jaap.testing.model.core;

/**
 * Inheritable non-generic test class that has a good varietyof methods and siblings (primitive wrappers).
 */
public abstract class TestNumber extends Number {

	@Override
	public double doubleValue() {
		throw new UnsupportedOperationException();
	}

	@Override
	public float floatValue() {
		throw new UnsupportedOperationException();
	}

	@Override
	public int intValue() {
		throw new UnsupportedOperationException();
	}

	@Override
	public long longValue() {
		throw new UnsupportedOperationException();
	}
}
