package tk.labyrinth.jaap.testing.junit5.annotation;

import tk.labyrinth.jaap.testing.junit5.JaapExtension;

import java.io.File;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to specify compilation target for {@link JaapExtension} tests.<br>
 * May be placed on <b>Types</b> and <b>Methods</b>.<br>
 *
 * @author Commitman
 * @see tk.labyrinth.jaap.core.CompilationTarget
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface CompilationTarget {

	/**
	 * Set to true if you want <b>Method</b> annotation to append <b>Type</b> one instead of override.
	 *
	 * @return whether append or override
	 */
	boolean append() default false;

	String[] compiledNames() default {};

	Class<?>[] compiledTypes() default {};

	/**
	 * Discovered via {@link File#File(String)} mechanics.
	 *
	 * @return names of source files located in root folder
	 */
	String[] sourceFiles() default {};

	String[] sourceNames() default {};

	/**
	 * Discovered via {@link ClassLoader#getResources(String)}  mechanics.
	 *
	 * @return names of source resources located in classpath
	 */
	String[] sourceResources() default {};

	Class<?>[] sourceTypes() default {};
}
