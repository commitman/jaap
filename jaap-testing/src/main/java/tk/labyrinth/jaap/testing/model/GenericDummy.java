package tk.labyrinth.jaap.testing.model;

import java.util.List;

public class GenericDummy<T> {

	private List<? extends Number> extendsNumberListField;

	private Integer integerField;

	private List<Integer> integerListField;

	private int intField;

	private List rawtypeListField;

	private T tField;

	private List<T> tListField;

	private List<?> wildcardListField;
}
