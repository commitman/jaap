package tk.labyrinth.jaap.testing.junit5.parameter;

import com.google.auto.service.AutoService;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;

@AutoService(JaapVariableResolver.class)
public class AnnotationProcessingRoundVariableResolver implements JaapVariableResolver<AnnotationProcessingRound> {

	@Override
	public Class<AnnotationProcessingRound> getParameterType() {
		return AnnotationProcessingRound.class;
	}

	@Override
	public boolean isInternal() {
		return true;
	}

	@Override
	public AnnotationProcessingRound resolveVariable(AnnotationProcessingRound round) {
		return round;
	}
}
