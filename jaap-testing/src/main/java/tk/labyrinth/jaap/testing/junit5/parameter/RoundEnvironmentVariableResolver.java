package tk.labyrinth.jaap.testing.junit5.parameter;

import com.google.auto.service.AutoService;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;

import javax.annotation.processing.RoundEnvironment;

@AutoService(JaapVariableResolver.class)
public class RoundEnvironmentVariableResolver implements JaapVariableResolver<RoundEnvironment> {

	@Override
	public Class<RoundEnvironment> getParameterType() {
		return RoundEnvironment.class;
	}

	@Override
	public boolean isInternal() {
		return true;
	}

	@Override
	public RoundEnvironment resolveVariable(AnnotationProcessingRound round) {
		return round.getRoundEnvironment();
	}
}
