package tk.labyrinth.jaap.testing.junit5;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.InvocationInterceptor;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.jupiter.api.extension.ReflectiveInvocationContext;
import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;
import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.jaap.misc4j.exception.wrapping.ExceptionWrapper;
import tk.labyrinth.jaap.testing.CompilationException;
import tk.labyrinth.jaap.testing.JaapTestCompiler;
import tk.labyrinth.jaap.testing.junit5.parameter.JaapVariableResolver;
import tk.labyrinth.misc4j2.java.lang.reflect.DynamicProxyHandler;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * TODO: Make no AP wrapping if no AP parameters are specified.
 * TODO: Add an annotation to indicate AP wrapping is required even with no AP parameters.
 */
public class JaapExtension implements InvocationInterceptor, ParameterResolver {

	private final JaapTestCompiler compiler = new JaapTestCompiler();

	private final Map<Class<?>, JaapVariableResolver<?>> variableResolvers;

	{
		variableResolvers = ServiceLoader.load(JaapVariableResolver.class).stream()
				.map(ServiceLoader.Provider::get)
				.map(variableResolver -> (JaapVariableResolver<?>) variableResolver)
				.collect(Collectors.toMap(JaapVariableResolver::getParameterType, Function.identity()));
	}

	@Override
	public void interceptTestMethod(
			Invocation<Void> invocation,
			ReflectiveInvocationContext<Method> invocationContext,
			ExtensionContext extensionContext) throws Throwable {
		Optional<Object> targetOptional = invocationContext.getTarget();
		boolean expectCompilationFailure = JaapExtensionUtils.expectCompilationFailure(invocationContext.getExecutable());
		boolean[] invocationProceeded = {false};
		boolean compilationFailed = false;
		try {
			compiler.run(JaapExtensionUtils.composeCompilationTarget(invocationContext.getExecutable()),
					new CallbackAnnotationProcessor().onFirstRound(round -> {
						targetOptional.ifPresent(target -> JaapExtensionUtils.enrichInstanceFields(target, round, variableResolvers));
						try {
							JaapExtensionUtils.enrichMethodArguments(invocationContext.getArguments().stream(), round, variableResolvers);
							invocation.proceed();
							invocationProceeded[0] = true;
						} catch (Throwable t) {
							// TODO: Catch and unwrap around task invocation
							ExceptionUtils.throwUnchecked(t);
						} finally {
							targetOptional.ifPresent(target -> JaapExtensionUtils.cleanInstanceFields(target, variableResolvers));
						}
					}));
		} catch (RuntimeException ex) {
			if (expectCompilationFailure && ex instanceof CompilationException) {
				compilationFailed = true;
			} else {
				// Must be from: com.sun.tools.javac.api.JavacTaskImpl.handleExceptions
				Throwable cause = ex.getCause();
				if (cause instanceof ExceptionWrapper) {
					Throwable rootCause = ((ExceptionWrapper) cause).unwrap();
					rootCause.addSuppressed(ex);
					throw rootCause;
				} else {
					throw new RuntimeException(ex);
				}
			}
		}
		if (expectCompilationFailure && (!invocationProceeded[0] || !compilationFailed)) {
			String faultMessage;
			if (!invocationProceeded[0]) {
				faultMessage = "CompilationFailure happened before test method was invoked";
			} else {
				faultMessage = "CompilationFailure was expected but not happened";
			}
			throw new RuntimeException(faultMessage);
		}
	}

	@Override
	public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) {
		Object result;
		{
			Class<?> type = parameterContext.getParameter().getType();
			JaapVariableResolver<?> variableResolver = variableResolvers.get(type);
			result = DynamicProxyHandler.ofType(variableResolver.getParameterType()).getProxy();
		}
		return result;
	}

	@Override
	public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) {
		Class<?> type = parameterContext.getParameter().getType();
		return variableResolvers.containsKey(type);
	}
}
