package tk.labyrinth.jaap.testing;

import lombok.Getter;

import java.util.List;

public class CompilationException extends RuntimeException {

	@Getter
	private final List<String> output;

	public CompilationException(List<String> output) {
		super();
		this.output = output;
	}

	@Override
	public String getMessage() {
		return String.join("\n", output);
	}
}
