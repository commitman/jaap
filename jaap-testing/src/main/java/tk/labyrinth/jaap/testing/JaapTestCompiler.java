package tk.labyrinth.jaap.testing;

import lombok.Getter;
import org.apache.commons.io.output.StringBuilderWriter;
import org.apache.commons.io.output.TeeWriter;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;
import tk.labyrinth.misc4j2.jaap.misc4j.java.util.regex.Regexes;

import javax.annotation.processing.Processor;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Based on spring-context-indexer <a href="https://github.com/spring-projects/spring-framework/blob/master/spring-context-indexer/src/test/java/org/springframework/context/index/test/TestCompiler.java">TestCompiler</a>.<br>
 *
 * @author Commitman
 * @version 1.0.0
 */
public class JaapTestCompiler {

	public static final String SOURCE_FOLDER = "src/test/java";

	@Getter
	private final JavaCompiler compiler;

	@Getter
	private final StandardJavaFileManager fileManager;

	@Getter
	private final Path outputDirectory;

	public JaapTestCompiler() {
		compiler = ToolProvider.getSystemJavaCompiler();
		{
			fileManager = compiler.getStandardFileManager(null, null, null);
			try {
				outputDirectory = Files.createTempDirectory(null);
				List<File> tempDir = List.of(outputDirectory.toFile());
				fileManager.setLocation(StandardLocation.CLASS_OUTPUT, tempDir);
				fileManager.setLocation(StandardLocation.SOURCE_OUTPUT, tempDir);
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			}
		}
	}

	private Stream<String> getClasses(CompilationTarget target) {
		return Stream.concat(target.getCompiledNames().stream(),
				target.getCompiledTypes().stream().map(Class::getCanonicalName));
	}

	private Stream<? extends JavaFileObject> getCompilationUnits(CompilationTarget target) {
		// TODO: Create a mechanics to add marked source files to artifact
		//  and make them available for projects that depends on this one.
		return StreamUtils.from(fileManager.getJavaFileObjects(StreamUtils.concat(
				JaapTestCompilerUtils.sourceFilesToPaths(target.getSourceFiles().stream()),
				JaapTestCompilerUtils.sourceNamesToPaths(SOURCE_FOLDER, target.getSourceNames().stream()),
				JaapTestCompilerUtils.sourceResourcesToPaths(target.getSourceResources().stream()),
				JaapTestCompilerUtils.sourceTypesToPaths(SOURCE_FOLDER, target.getSourceTypes().stream()))
				.toArray(Path[]::new)));
	}

	/**
	 * @param target     non-null
	 * @param processors non-null
	 *
	 * @return compiler output split in lines
	 */
	public List<String> run(CompilationTarget target, Processor... processors) {
		StringBuilderWriter outputWriter = new StringBuilderWriter();
		//
		JavaCompiler.CompilationTask task = compiler.getTask(
				new TeeWriter(JaapTestCompilerUtils.getDefaultJavacWriter(), outputWriter),
				fileManager,
				null,
				null,
				getClasses(target).collect(Collectors.toList()),
				getCompilationUnits(target).collect(Collectors.toList()));
		task.setProcessors(List.of(processors));
		//
		Boolean succeed = task.call();
		List<String> output = Stream.of(outputWriter.toString().split(Regexes.newline()))
				.collect(Collectors.toList());
		//
		if (!succeed) {
			throw new CompilationException(output);
		}
		return output;
	}
}
