package tk.labyrinth.jaap.testing.junit5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.ProcessingEnvironment;

@ExtendWithJaap
public class JaapExtensionFieldInjectionTest {

	private ProcessingEnvironment processingEnvironment;

	@Test
	void test(AnnotationProcessingRound round) {
		Assertions.assertEquals(round.getProcessingEnvironment(), processingEnvironment);
	}
}
