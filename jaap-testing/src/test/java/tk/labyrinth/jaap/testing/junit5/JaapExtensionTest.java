package tk.labyrinth.jaap.testing.junit5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExpectCompilationFailure;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.testing.test.model.Dummy;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import java.util.List;
import java.util.stream.Collectors;

@ExtendWithJaap
class JaapExtensionTest {

	@Test
	void testAnnotationProcessingRound(AnnotationProcessingRound round) {
		Assertions.assertNotNull(round);
	}

	@CompilationTarget(sourceResources = "InvalidJavaHelloWorld.java")
	@ExpectCompilationFailure
	@Test
	void testExpectCompilationFailure() {
		// We do not have sources for java.lang.Object presented so the compilation must fail.
	}

	@CompilationTarget(
			sourceNames = {
					"tk.labyrinth.jaap.testing.test.model.package-info",
					"tk.labyrinth.jaap.testing.test.model.pkg.package-info"},
			sourceTypes = Dummy.class)
	@Test
	void testMethodDeclaredCompilationTargetWithSourceNamesAndTypes(RoundEnvironment environment) {
		Assertions.assertEquals(List.of(
				"tk.labyrinth.jaap.testing.test.model",
				"tk.labyrinth.jaap.testing.test.model.Dummy",
				"tk.labyrinth.jaap.testing.test.model.pkg"
		), environment.getRootElements().stream()
				.map(Object::toString)
				.sorted()
				.collect(Collectors.toList()));
	}

	@CompilationTarget(sourceResources = "JavaHelloWorld.java")
	@Test
	void testMethodDeclaredCompilationTargetWithSourceResources(RoundEnvironment environment) {
		Assertions.assertEquals(List.of(
				"tk.labyrinth.jaap.testing.resource.JavaHelloWorld"
		), environment.getRootElements().stream()
				.map(Object::toString)
				.sorted()
				.collect(Collectors.toList()));
	}

	@CompilationTarget(sourceTypes = Dummy.class)
	@Test
	void testMethodDeclaredCompilationTargetWithSourceTypes(RoundEnvironment environment) {
		Assertions.assertEquals(List.of(
				"tk.labyrinth.jaap.testing.test.model.Dummy"
		), environment.getRootElements().stream()
				.map(Object::toString)
				.sorted()
				.collect(Collectors.toList()));
	}

	@Test
	void testProcessingEnvironment(ProcessingEnvironment environment) {
		Assertions.assertNotNull(environment);
	}

	@Test
	void testProcessingEnvironmentAndRoundEnvironment(ProcessingEnvironment processingEnvironment, RoundEnvironment roundEnvironment) {
		Assertions.assertNotNull(processingEnvironment);
		Assertions.assertNotNull(roundEnvironment);
	}

	@Test
	void testRoundEnvironment(RoundEnvironment environment) {
		Assertions.assertNotNull(environment);
	}
}
