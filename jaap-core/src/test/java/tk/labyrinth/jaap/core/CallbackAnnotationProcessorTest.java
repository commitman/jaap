package tk.labyrinth.jaap.core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.misc4j.exception.wrapping.WrappingError;
import tk.labyrinth.jaap.misc4j.lib.junit5.ContribAssertions;

class CallbackAnnotationProcessorTest {

	@Test
	void testOnInitCallbackCanThrowThrowable() {
		ContribAssertions.assertThrows(
				() -> new CallbackAnnotationProcessor().onInit(processingEnvironment -> {
					throw new Throwable("foo");
				}).init(null),
				fault -> {
					Assertions.assertEquals(WrappingError.class, fault.getClass());
					Assertions.assertEquals("java.lang.Throwable: foo", fault.getMessage());
				});
	}
}
