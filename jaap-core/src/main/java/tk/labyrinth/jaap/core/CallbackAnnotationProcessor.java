package tk.labyrinth.jaap.core;

import tk.labyrinth.jaap.misc4j.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.java.util.function.CheckedConsumer;

import javax.annotation.processing.Completion;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CallbackAnnotationProcessor implements Processor {

	private final List<CheckedConsumer<ProcessingEnvironment, ? extends Throwable>> initCallbacks = new ArrayList<>();

	private final List<CheckedConsumer<AnnotationProcessingRound, ? extends Throwable>> roundCallbacks = new ArrayList<>();

	private int index = 0;

	private ProcessingEnvironment processingEnvironment = null;

	@Override
	public Iterable<? extends Completion> getCompletions(Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
		return null;
	}

	@Override
	public Set<String> getSupportedAnnotationTypes() {
		return Set.of("*");
	}

	@Override
	public Set<String> getSupportedOptions() {
		return Set.of();
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latestSupported();
	}

	@Override
	public void init(ProcessingEnvironment processingEnvironment) {
		this.processingEnvironment = processingEnvironment;
		initCallbacks.forEach(callback -> {
			try {
				callback.accept(processingEnvironment);
			} catch (Throwable t) {
				ExceptionUtils.throwUnchecked(t);
			}
		});
	}

	public final CallbackAnnotationProcessor onEachRound(CheckedConsumer<AnnotationProcessingRound, ? extends Throwable> callback) {
		roundCallbacks.add(callback);
		return this;
	}

	public final CallbackAnnotationProcessor onFirstRound(CheckedConsumer<AnnotationProcessingRound, ? extends Throwable> callback) {
		roundCallbacks.add(round -> {
			if (round.isFirst()) {
				callback.accept(round);
			}
		});
		return this;
	}

	public final CallbackAnnotationProcessor onInit(CheckedConsumer<ProcessingEnvironment, ? extends Throwable> callback) {
		initCallbacks.add(callback);
		return this;
	}

	public final CallbackAnnotationProcessor onLastRound(CheckedConsumer<AnnotationProcessingRound, ? extends Throwable> callback) {
		roundCallbacks.add(round -> {
			if (round.isLast()) {
				callback.accept(round);
			}
		});
		return this;
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment) {
		AnnotationProcessingRound round = new AnnotationProcessingRoundImpl(annotations, index, processingEnvironment, roundEnvironment);
		roundCallbacks.forEach(callback -> {
			try {
				callback.accept(round);
			} catch (Throwable t) {
				ExceptionUtils.throwUnchecked(t);
			}
		});
		index++;
		return false;
	}
}
