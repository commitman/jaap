package tk.labyrinth.jaap.core;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Builder
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class CompilationTarget {

	@Builder.Default
	List<String> compiledNames = List.of();

	@Builder.Default
	List<Class<?>> compiledTypes = List.of();

	/**
	 * Discovered via {@link File#File(String)} mechanics.
	 */
	@Builder.Default
	List<String> sourceFiles = List.of();

	@Builder.Default
	List<String> sourceNames = List.of();

	/**
	 * Discovered via {@link ClassLoader#getResources(String)}  mechanics.
	 */
	@Builder.Default
	List<String> sourceResources = List.of();

	@Builder.Default
	List<Class<?>> sourceTypes = List.of();

	public static CompilationTarget merge(CompilationTarget first, CompilationTarget second) {
		return CompilationTarget.builder()
				.compiledNames(Stream.concat(first.compiledNames.stream(), second.compiledNames.stream()).collect(Collectors.toList()))
				.compiledTypes(Stream.concat(first.compiledTypes.stream(), second.compiledTypes.stream()).collect(Collectors.toList()))
				.sourceFiles(Stream.concat(first.sourceFiles.stream(), second.sourceFiles.stream()).collect(Collectors.toList()))
				.sourceNames(Stream.concat(first.sourceNames.stream(), second.sourceNames.stream()).collect(Collectors.toList()))
				.sourceResources(Stream.concat(first.sourceResources.stream(), second.sourceResources.stream()).collect(Collectors.toList()))
				.sourceTypes(Stream.concat(first.sourceTypes.stream(), second.sourceTypes.stream()))
				.build();
	}

	public static CompilationTarget of(
			List<String> compiledNames,
			List<Class<?>> compiledTypes,
			List<String> sourceFiles,
			List<String> sourceNames,
			List<String> sourceResources,
			List<Class<?>> sourceTypes) {
		return CompilationTarget.builder()
				.compiledNames(compiledNames)
				.compiledTypes(compiledTypes)
				.sourceFiles(sourceFiles)
				.sourceNames(sourceNames)
				.sourceResources(sourceResources)
				.sourceTypes(sourceTypes)
				.build();
	}

	/**
	 * Simplest CompilationTarget consisting of compiled Object class. May be used when Processor is aimed on existing model.
	 *
	 * @return non-null
	 */
	public static CompilationTarget ofObject() {
		return of(
				List.of(),
				List.of(Object.class),
				List.of(),
				List.of(),
				List.of(),
				List.of());
	}

	public static CompilationTarget ofSourceResources(Stream<String> sourceResources) {
		return of(
				List.of(),
				List.of(),
				List.of(),
				List.of(),
				sourceResources.collect(Collectors.toList()),
				List.of());
	}

	public static CompilationTarget ofSourceResources(String... sourceResources) {
		return ofSourceResources(Stream.of(sourceResources));
	}

	public static CompilationTarget ofSourceTypes(Class<?>... sourceTypes) {
		return ofSourceTypes(Stream.of(sourceTypes));
	}

	public static CompilationTarget ofSourceTypes(Stream<Class<?>> sourceTypes) {
		return of(
				List.of(),
				List.of(),
				List.of(),
				List.of(),
				List.of(),
				sourceTypes.collect(Collectors.toList()));
	}

	public static class CompilationTargetBuilder {

		public CompilationTargetBuilder sourceTypes(Class<?>... sourceTypes) {
			return sourceTypes(ListUtils.fromNullable(sourceTypes));
		}

		public CompilationTargetBuilder sourceTypes(List<Class<?>> sourceTypes) {
			sourceTypes$set = true;
			sourceTypes$value = sourceTypes;
			return this;
		}

		public CompilationTargetBuilder sourceTypes(Stream<Class<?>> sourceTypes) {
			return sourceTypes(ListUtils.fromNullable(sourceTypes));
		}
	}
}
